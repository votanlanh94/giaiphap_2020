<?php echo $_doctype; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $_title;?></title>
    
    <!-- Bootstrap-->
    <!-- Latest compiled and minified CSS -->
    <link rel="icon" type="image/png" sizes="16x16" href="public/site/clickfunel/images/icon-120x120.png">
    <link rel="icon" type="image/png" sizes="32x32" href="public/site/clickfunel/images/icon-120x120.png">

    <link href="public/site/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/site/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="public/site/clickfunel/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/site/clickfunel/css/timeline.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="public/site/clickfunel/css/youtube-embed.css">
    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick-theme.css"/>
    <link rel="stylesheet" href="public/site/clickfunel/css/ionicons.min.css">
    
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Serum Sắc Ngọc Khang - dưỡng trắng diệu kỳ" />
    <meta property="og:description" content="Serum Sắc Ngọc Khang - hiệu quả gấp 10 lần so với kem trắng da thông thường." />
    <meta property="og:image" content="<?php echo base_url().'/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-1.jpg'; ?>" />

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '299203756911918');
        fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=299203756911918&ev=PageView&noscript=1"
      /></noscript>
      <!-- End Facebook Pixel Code -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-28114834-7', 'auto');
      ga('send', 'pageview');

    </script>
    <!-- Global site tag (gtag.js) - AdWords: 1005741814 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1005741814"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-1005741814'); </script> 
  </head>
  <body class="">
    <?php $homepage = "http://sacngockhang.com"; ?>
  	<?php echo $header; ?>
  	<?php echo $content; ?>
  	<?php echo $footer; ?>  

    <input type="hidden" id="baseurl" value="<?php echo base_url() ?>" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->

    <script src="public/site/clickfunel/js/jquery.js"></script>
    <script src="public/site/clickfunel/js/lazyload.min.js"></script>
    <script>
      $("img").lazyload({
          effect : "fadeIn"
      });
    </script>
    <script src="public/site/clickfunel/js/youtube-embed.js"></script>
    <!-- Latest compiled JavaScript-->
    <script src="public/site/clickfunel/js/tether.min.js" type="text/javascript"></script>
    <script src="public/site/clickfunel/js/bootstrap.min.js"></script>
    <script src="public/site/clickfunel/js/slick.min.js"></script>
    
    <?php echo $_scripts; ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;amp;subset=vietnamese" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&amp;subset=vietnamese" rel="stylesheet">
    <script>      
      $('#myCarousel').carousel({interval:5000});
    </script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId            : '218440352049462',
          autoLogAppEvents : true,
          xfbml            : true,
          version          : 'v2.12'
        });
      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/vi_VN/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    
  </body>
</html>
