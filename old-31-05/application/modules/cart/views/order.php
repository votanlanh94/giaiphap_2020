<div role="document" class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title"> <span>Thông tin đơn hàng <?php echo $data->MaDH ?></span></h4>
        </div>
        <div class="modal-body">
            <div class="row" style="margin-bottom:10px;">
                <div class="col-xs-6 col-sm-3"><b>Tên người nhận</b></div>
                <div class="col-xs-6 col-sm-9">: <?php echo $data->Name ?></div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-xs-6 col-sm-3"><b>Số điện thoại</b></div>
                <div class="col-xs-6 col-sm-9">: <?php echo $data->Phone ?></div>
            </div>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-xs-6 col-sm-3"><b>Địa chỉ giao hàng</b></div>
                <div class="col-xs-6 col-sm-9">: <?php echo $data->AddressOrder ?></div>
            </div>
            <?php 
            $arr_status = $this->db->query("select * from ttp_define where `group`='status' and `type`='order'")->result();
            $array_status = array();
            foreach($arr_status as $key=>$ite){
                $code = (int)$ite->code;
                $array_status[$code] = $ite->name;
            }
            $current_status = isset($array_status[$data->Status]) ? $array_status[$data->Status] : 'Đang xác định...' ;
            if($data->Status==7){
                if($data->TransportStatus!=''){
                    $current_status = $current_status.' > '.$data->TransportStatus;
                }
            }
            $color = $data->Status == 1 ? 'danger' : 'primary' ;
            $color = $data->Status == 0 ? 'success' : $color ;
            ?>
            <div class="row" style="margin-bottom:10px;">
                <div class="col-xs-6 col-sm-3"><b>Trạng thái đơn</b></div>
                <div class="col-xs-6 col-sm-9"><label class="label label-<?php echo $color ?>"><?php echo $current_status ?></label></div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table">
                        <tr>
                            <th>STT</th>
                            <th>Sản phẩm</th>
                            <th>Đơn giá</th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                        </tr>
                        <?php 
                        if(count($details)>0){
                            foreach($details as $key=>$row){
                                $stt = $key+1;
                                echo "<tr>";
                                echo "<td>$stt</td>";
                                echo "<td>$row->Title</td>";
                                echo "<td>".number_format($row->Price,0,'','.')."</td>";
                                echo "<td>$row->Amount</td>";
                                echo "<td>".number_format($row->Total,0,'','.')."</td>";
                                echo "</tr>";
                            }
                        }
                        ?>
                        <tr>
                            <th colspan="4" class="text-right">PHÍ GIAO HÀNG : </th>
                            <th><?php echo number_format($data->Chiphi,0,'','.') ?></th>
                        </tr>
                        <tr>
                            <th colspan="4" class="text-right">GIÁ TRỊ ĐƠN HÀNG : </th>
                            <th><?php echo number_format($data->Total+$data->Chiphi,0,'','.') ?>đ</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
