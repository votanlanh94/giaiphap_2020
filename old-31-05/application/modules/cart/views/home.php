<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Sắc Ngọc Khang</title>
    
      <!-- Bootstrap-->
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="public/site/clickfunel/css/base.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;amp;subset=vietnamese" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&amp;subset=vietnamese" rel="stylesheet">
    
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Chương trình khuyến mãi Sắc Ngọc Khang tháng 08/2017" />
    <meta property="og:description" content="Tiết kiệm lên đến 647.000đ và còn rất nhiều phần quà hấp dẫn đang chờ bạn." />
    <meta property="og:image" content="<?php echo base_url().'public/site/clickfunel/images/banner1T1-02.jpg' ?>" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
    script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
    -->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '299203756911918'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=299203756911918&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '299203756911918', {
    em: 'online@hoathienphu.com'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=299203756911918&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
      
      <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80360224-1', 'auto');
  ga('send', 'pageview');

</script>
  </head>
  <body class="sale-page">
    <?php 
        $this->db->query("update ttp_funels set View=View+1 where ID=1");  
    ?>
    <header class="header">
      <!-- End top-header-->
      <div class="main-head">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 text-center">
                <img src="public/site/clickfunel/images/logo.svg" style="max-width:300px;margin:20px auto" class="img-responsive" />
            </div>
          </div>
        </div>
      </div>
      <!-- End main-head-->
    </header>
    <!-- End header-->
    <div class="content VTL-content">
      <div class="site-control hidden-md-down">
        <div class="top-widget hidden">
          <div class="alert">
            <p><strong>A beautiful widgets </strong><span>to your website  </span><a href="" class="btn btn-warning btn-sm">Tham gia ngay</a>
              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </p>
          </div>
        </div>
        <div class="bottom-widget hidden">
          <div class="alert">
            <p><strong>A beautiful widgets </strong><span>to your website in bottom  </span><a href="" class="btn btn-warning btn-sm">Tham gia ngay</a>
              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </p>
          </div>
        </div>
        <div class="flyer-right hidden">
          <div class="alert">
            <p><strong>Beautiful and simple widgets </strong><span>Grab your visitors’ attention and lead them to the most important content. </span><a href="" class="btn btn-warning btn-sm">Tham gia ngay</a>
              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            </p>
          </div>
        </div>
        <div class="social"><a href="#" class="btn"><i class="snk snk-thumbs-up-alt"></i><span><b>Thích </b>100K</span></a><a href="https://www.facebook.com/sharer.php?u=<?php echo current_url() ?>" class="btn"><i class="snk snk-facebook-official"></i><span> <b>Chia sẻ </b>25K</span></a><a href="#comment-box" class="btn"><i class="snk snk-facebook-messenger"></i><span><b>Bình luận </b><?php echo count($Comments) ?></span></a></div>
      </div>
      <div class="content-box content-style-1">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-lg-10 offset-lg-1">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/r5HHapANIyo" allowfullscreen="" class="embed-responsive-item"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="content-box content-style-2" style="background: none;padding: 0px;margin-top: -30px;">
          <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-xs-center group-cta"><a onclick="click_position(this,'click_position',1,1,'')" href="#cta-plan" title="Đặt ngay trọn bộ" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Đặt ngay trọn bộ giải pháp</b><span>Tiết kiệm lên đến 647.000Đ </span></a></div>
                    </div>
                </div>  
          </div>
        </div>
      <div class="content-box content-style-2">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
              Theo khảo sát của <span><b>Kadence International </b>tại việt nam</span>
            </h3>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="item"> <img src="public/site/clickfunel/images/asset/phu-nu-25-55.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid" style="margin:auto">
                <h6>PHỤ NỮ TUỔI TỪ 25 - 55</h6>
                <p>Bị các vấn đề về nám da, sạm da & tàn nhang.</p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="item"> <img src="public/site/clickfunel/images/asset/phu-nu-tren-35.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid" style="margin:auto">
                <h6>PHỤ NỮ TUỔI TỪ 35+</h6>
                <p>Gặp các vấn đề về da.</p>
              </div>
            </div>
            <div class="col-md-6">
              <p>Điều này có nghĩa là cứ 10 phụ nữ  từ 25 tuổi thì có tới khoảng 4 người bị nám, sạm, tàn nhang. Tình trạng này khiến chị em thiếu tự tin trong giao tiếp, ảnh hưởng tới tâm lý cũng như chất lượng cuộc sống. </p>
              <p>Có rất nhiều nguyên nhân dẫn tới nám da, sạm da, tàn nhang nhưng nguyên nhân chủ yếu là do rối loạn nội tiết tố nữ thường xảy ra vào giai đoạn dậy thì, mang thai hoặc tiền mãn kinh. </p>
              <p>Ngoài ra, lạm dụng thuốc tránh thai, lão hóa, ánh nắng, khói bụi,… cũng là những yếu tố có thể gây xuất hiện nám, sạm, tàn nhang.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="content-box content-style-3">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
              Tại sao điều trị<span><b>Nám, sạm & tàn nhang </b>thường thất bại</span>
            </h3>
          </div>
          <div class="row">
            <div class="col-xs-12 col-lg-10 offset-lg-1">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/m1AGYRKJKyo" allowfullscreen="" class="embed-responsive-item"></iframe>
              </div>
              <p class="quote-item col-lg-10 offset-lg-1"><i class="snk snk-quote"></i><span>Đa phần là do chị em quá chủ quan, nóng vội và không hiểu đúng giải pháp khắc phục nám đúng cách.</span></p>
            </div>
            <div class="col-md-12">
              <div class="comment-content">
                <ul class="media-list">
                  <li class="media">
                    <div class="media-left"><img src="public/site/clickfunel/images/asset/gs-hien.jpg" class="media-object img-circle"><span class="snk-stack snk-lg"><i class="snk snk-circle snk-stack-2x"></i><i class="snk snk-quote snk-stack-1x fa-inverse"></i></span></div>
                    <div class="media-body">
                      <h6 class="media-heading"> <b>GS Phạm Văn Hiển </b><span>Nguyên Viện trưởng Viện Da Liễu Quốc Gia</span></h6>
                      <p>Tôi đã chữa cho độ khoảng trên 10 người, người ta bị cái gọi là chữa nám tức thời vì mặt họ bị rộp hết lên và thực sự khắc phục rất khó. Tôi phải mất gần 2 tháng để chữa cho bệnh nhân đó thì tình trạng mới dịu dần dần. </p>
                      <p>Cho nên, những ai dùng những sản phẩm lột da, tẩy da, làm mỏng da thì phải hết sức cẩn thận bởi đất nước ta là một xứ có rất nhiều ánh nắng mặt trời lại bụi bẩn vô cùng. Nếu chúng ta làm cho da thật mỏng đi coi chừng nó lại quay lại khi nó tái phát chắc chắn sẽ nặng hơn trước rất nhiều...</p>
                    </div>
                  </li>
                  <li class="media">
                    <div class="media-left"><img src="public/site/clickfunel/images/asset/gs-hoang.jpg" class="media-object img-circle"><span class="snk-stack snk-lg"><i class="snk snk-circle snk-stack-2x"></i><i class="snk snk-quote snk-stack-1x fa-inverse"></i></span></div>
                    <div class="media-body">
                      <h6 class="media-heading"> <b>BS. Huỳnh Huy Hoàng </b><span>Trưởng khoa điều trị số 2 Bệnh Vện Da Liễu TP HCM</span></h6>
                      <p> Thứ nhất, ngăn chặn các nguyên nhân ngoại cảnh gây nám như ánh nắng mặt trời, môi trường sống, căng thẳng…</p>
                      <p>Thứ hai, xóa đi hoặc làm giảm đi các hắc tố melanin đã gây nám, sạm, tàn nhang ở trên da.</p>
                      <p>Thứ ba, điều trị nám từ gốc, ngăn cản các yếu tố dẫn tới nám da như nội tiết, lão hóa,…</p>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="text-xs-center group-cta"><a onclick="click_position(this,'click_position',3,1,'')" href="#cta-plan" title="Đặt ngay trọn bộ" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Đặt ngay trọn bộ giải pháp</b><span>Tiết kiệm lên đến 647.000đ</span></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-box content-style-4">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              BỘ SẢN PHẨM SẮC NGỌC KHANG<span><b>KHẮC PHỤC NÁM, SẠM & TÀN NHANG </b>NHƯ THẾ NÀO ?</span>
            </h3>
          </div>
          <div class="card card-gray">
            <div class="card-block">
              <p>Bộ sản phẩm bao gồm Viên uống Sắc Ngọc Khang – tác động từ sâu bên trong cơ thể, giúp loại bỏ nguyên nhân gốc rễ gây nám da, sạm da, tàn nhang & ngăn ngừa nám da, sạm da, tàn nhang quay trở lại. Bộ ba mỹ phẩm Sữa rửa mặt, Nước hoa hồng & Kem dưỡng da Sắc Ngọc Khang giúp xóa nhanh vết nám, sạm, tàn nhang đã hình thành.</p>
            </div>
          </div>
          <div class="item row">
            <div class="col-md-5"><img src="public/site/clickfunel/images/asset/snk-moi.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid"></div>
            <div class="col-md-7">
              <h3>VIÊN UỐNG SẮC NGỌC KHANG VI TẢO LỤC</h3>
              <p>Với Astaxanthin từ Vi Tảo Lục Nhật Bản và các thành phần thiên nhiên giúp loại bỏ nguyên nhân gây nám, sạm, tàn nhang từ bên trong cơ thể:</p>
              <ul class="list-unstyled">
                <li>Astaxanthin: chống oxy hóa mạnh gấp 550 lần Vitamin E, gấp 6000 lần Vitamin C giúp ngăn ngừa lão hóa da.</li>
                <li>Tinh chất mầm đậu tương: cân bằng nội tiết tố nữ – loại bỏ nguyên nhân chủ yếu gây nám, sạm, tàn nhang.</li>
                <li>Quy râu, ngưu tất, thục địa, ích mẫu: làm tăng cường lưu thông máu tới nuôi dưỡng các tế bào da giúp da luôn hồng hào, tươi trẻ.</li>
              </ul>
            </div>
          </div>
          <div class="item row">
            <div class="col-md-5 push-md-7"><img src="public/site/clickfunel/images/asset/snk-tinh-chat.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid"></div>
            <div class="col-md-7 pull-md-5">
              <h3>SỮA RỬA MẶT SẮC NGỌC KHANG</h3>
              <p>Chứa tinh thể Vitamin E & Tinh chất Hoa anh đào giúp:</p>
              <ul class="list-unstyled">
                <li>Làm sạch sâu nhưng dịu nhẹ, không làm hư tổn lớp màng bảo vệ da, không gây khô da.</li>
                <li>Chống lão hóa, giảm nám, sạm, tàn nhang giúp da trắng sáng.</li>
              </ul>
            </div>
          </div>
          <div class="item row">
            <div class="col-md-5"><img src="public/site/clickfunel/images/asset/snk-nuoc-hoa-hong.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid"></div>
            <div class="col-md-7">
              <h3>NƯỚC HOA HỒNG SẮC NGỌC KHANG</h3>
              <p>Chiết xuất từ <b>5 loại thảo dược </b>thiên nhiên - hoa hồng, cúc la mã, cúc xu  xi, hoa anh đào, cây phỉ - mang lại <b>4 tác dụng</b>:</p>
              <ul class="list-unstyled">
                <li>Làm sạch sâu, giúp hoạt chất từ kem dưỡng có thể thấm tốt vào da</li>
                <li>Làm dịu da sau ngày dài tiếp xúc với ánh nắng, ngăn cản sự hình và lan rộng nám, sạm, tàn nhang.</li>
                <li>Cân bằng độ ẩm giúp làn da luôn căng mịn và ngăn ngừa sự lão hóa.</li>
                <li>Se khít lỗ chân lông hiệu quả.Sản phẩm đặc biệt an toàn & phù hợp với mọi loại da, đảm bảo <b>3 không </b>– không chứa cồn – không gây khô da – không gây kích ứng da.</li>
              </ul>
            </div>
          </div>
          <div class="item row last-child">
            <div class="col-md-5 push-md-7"><img src="public/site/clickfunel/images/asset/snk-kem.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid"></div>
            <div class="col-md-7 pull-md-5">
              <h3>KEM DƯỠNG DA SẮC NGỌC KHANG</h3>
              <p>Chiết xuất Hoa anh đào cùng dầu Macadamia, dầu dừa phân đoạn giúp:</p>
              <ul class="list-unstyled">
                <li>Thấm sâu vào tận cùng da, tác động tận gốc cơ chế sinh nám.</li>
                <li>Ức chế enzym Tyrosinaze, ngăn ngừa sự hình thành sắc tố melanin, làm mờ vết nám, sạm, tàn nhang.</li>
                <li>Dưỡng ẩm, ngăn ngừa lão hóa giúp da luôn mịn màng và tươi trẻ.</li>
              </ul>
            </div>
          </div>
          <div class="text-xs-center group-cta"><a onclick="click_position(this,'click_position',3,1,'')" href="#cta-plan" title="Đặt ngay trọn bộ" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Đặt ngay trọn bộ giải pháp</b><span>Tiết kiệm lên đến 647.000đ</span></a></div>
        </div>
      </div>
      <div class="content-box content-style-5">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              KẾ THỪA TINH HOA<span><b>DƯỢC HỌC CỔ TRUYỀN DÂN TỘC & ỨNG DỤNG THÀNH QUẢ KHOA HỌC THẾ GIỚI</b></span>
            </h3>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card"><img src="public/site/clickfunel/images/asset/chiet-xuat.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid">
                <h3 class="bg-primary">KHOA HỌC THẾ GIỚI</h3>
                <div class="card-block">
                  <p>Astaxanthin từ vi tảo lục Nhật Bản (chiết xuất được chứng minh có tác dụng chống lão hóa da gấp <b class="text-primary">550 lần Vitamin E, gấp 6000 lần vitamin C</b> <b>giúp đẩy lùi các tình trạng nám da, sạm da, tàn nhang rất hiệu quả.</b></p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card"><img src="public/site/clickfunel/images/asset/duoc-lieu.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid">
                <h3 class="bg-primary">DƯỢC HỌC CỔ TRUYỀN DÂN TỘC</h3>
                <div class="card-block">
                  <p>Các dược liệu cổ truyền được trồng tại vùng dược liệu sạch -  Tỉnh Nam Định, Hà Giang - <b class="text-primary">theo tiêu chuẩn GACP - WHO </b>như : Quy râu, Ngưu tất, Thục địa, Ích mẫu…</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-box">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              SẢN XUẤT TRÊN DÂY CHUYỀN HIỆN ĐẠI <span><b>THEO TIÊU CHUẨN QUỐC TẾ</b></span>
            </h3>
          </div>
          <div class="row">
            <div class="col-xs-12 col-lg-10 offset-lg-1">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/BIdPeigFURs" allowfullscreen="" class="embed-responsive-item"></iframe>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <p>Bộ sản phẩm <b class="text-primary">Sắc Ngọc Khang </b>được sản sản xuất tại Nhà máy Hoa Thiên Phú Bình Dương - tọa lạc tại khu công nghiệp <b>Singapore Ascendas Protrade </b>và được xây dựng theo mô hình hiện đại của thế giới.</p>
                </div>
                <div class="col-md-6">
                  <p>Nhà máy <b class="text-primary">Hoa Thiên Phú </b>Bình Dương có diện tích lên đến 12.000m2, cùng hệ thống máy móc, trang thiết bị hiện đại, dây chuyền kiểm soát chất lượng được hoạt động theo quy trình khép kín. Nhờ đó, chất lượng sản phẩm viên uống Sắc Ngọc Khang đáp ứng đầy đủ các tiêu chuẩn khắt khe của Bộ Y Tế</p>
                </div>
              </div>
              <div class="text-xs-center group-cta"><a onclick="click_position(this,'click_position',4,1,'')" href="#cta-plan" title="Đặt ngay trọn bộ" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Đặt ngay trọn bộ giải pháp</b><span>Tiết kiệm lên đến 647.000đ </span></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-box content-style-6">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              MANG LẠI HIỆU QUẢ TỐI ƯU <span><b>CHO NGƯỜI TIÊU DÙNG</b></span>
            </h3>
          </div>
          <div class="card card-gray">
            <div class="card-block">
              <p>Chân gốc nám, tàn nhang  nằm sâu dưới các lớp da, cần có 1 khoảng thời gian đủ theo liệu trình để sắc tố đen melanin được đào thải hoàn toàn. Các chuyên gia đã nghiên cứu và kết luận: </p>
              <ul class="list-unstyled">
                <li>Cần ít nhất <b>3 tháng </b>để các hoạt chất của bộ sản phẩm <b class="text-primary">Sắc Ngọc Khang </b>tác động và đào thải nám, sạm, tàn nhang  tận gốc. </li>
                <li>Nếu bạn tuân thủ đúng hướng dẫn, làn da sẽ sáng đẹp rõ rệt. </li>
                <li>Ngoài những cải thiện trên da, Viên uống Sắc Ngọc Khang còn đem lại những chuyển biến tích cực trên cơ thể người dùng</li>
              </ul>
            </div>
          </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <img src="public/site/clickfunel/images/asset/1.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-responsive">
                    </div>
                      <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <img src="public/site/clickfunel/images/asset/2.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-responsive">
                    </div>
                      <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <img src="public/site/clickfunel/images/asset/3.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-responsive">
                    </div>
                      <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <img src="public/site/clickfunel/images/asset/4.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-responsive">
                    </div>
            </div>
        </div>
      </div>
      <div class="content-box content-style-7">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              HÀNG TRIỆU KHÁCH HÀNG<span>ĐÃ TIN DÙNG <b>SẮC NGỌC KHANG</b></span>
            </h3>
          </div>
          <div class="row">
            <div class="col-xs-12 col-lg-10 offset-lg-1">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/zPTsNmmPfd8" allowfullscreen="" class="embed-responsive-item"></iframe>
              </div>
            </div>
            <div class="col-md-6 col-lg-5 offset-lg-1"><img src="public/site/clickfunel/images/asset/combo.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid"></div>
            <div class="col-md-6 col-lg-5">
              <h3><span>Gồm 3 hộp</span><b>Sắc Ngọc Khang vi tảo lục</b></h3>
              <ul class="list-unstyled">
                <li>01 Sữa rửa mặt Sắc Ngọc Khang.</li>
                <li>01 Nước hoa hồng Sắc Ngọc Khang.</li>
                <li>01 Kem dưỡng da Sắc Ngọc Khang.</li>
              </ul>
            </div>
          </div>
          <div class="text-xs-center group-cta"><a onclick="click_position(this,'click_position',5,1,'')" href="#cta-plan" title="Đặt ngay trọn bộ" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Đặt ngay trọn bộ giải pháp</b><span>Tiết kiệm lên đến 647.000đ </span></a></div>
        </div>
      </div>
      <div class="content-box">
        <div class="container-fluid">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              BÍ QUYẾT CHĂM SÓC SẮC ĐẸP CỦA<span><b>HOA HẬU VIỆT NAM</b></span>
            </h3>
          </div>
          <div class="row">
            <div class="col-xs-12"><img src="public/site/clickfunel/images/asset/hoa-hau-viet-nam.jpg" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid w-100"></div>
          </div>
        </div>
      </div>
      <div class="content-box content-text-2">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              CÁC THÀNH TỰU <span><b>ĐƯỢC CỘNG ĐỒNG </b>CÔNG NHẬN</span>
            </h3>
          </div>
          <div class="row">
            <div class="col-md-7"><img src="public/site/clickfunel/images/asset/thanh-tuu.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid"></div>
            <div class="col-md-5">
              <ul class="list-unstyled">
                <li> <b class="text-primary">Hàng Việt Nam Chất Lượng Cao</b> nhiều năm liên tiếp.</li>
                <li> <b class="text-primary">TOP 1 Việt Nam</b> về dòng sản phẩm khắc phục nám da (Theo số liệu của QuintilesIMS USA - Tập đoàn toàn cầu chuyên về nghiên cứu thị trường và thử nghiệm lâm sàng công nhận là thương hiệu số 1 Việt Nam trong 05 năm liên tiếp).</li>
                <li><b class="text-primary">Sản phẩm vàng </b>vì sức khỏe cộng đồng.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div id="cta-plan" class="content-box content-style-9">
        <div class="container">
            <div class="content-box-heading">
            <h3 class="content-box-title">
               
              SỞ HỮU TRỌN BỘ SẢN PHẨM <span><b>SẮC NGỌC KHANG </b>CHÍNH HÃNG VỚI GIÁ ƯU ĐÃI</span>
            </h3>
          </div>
          <div class="row no-gutters">
            <div class="col-md-4">
              <div class="item">
                  <img src="public/site/clickfunel/images/asset/Giai-phap-co-ban.png" class="img img-responsive" />
                <div class="plan-title">
                  <p class="plan-info"><b>Giải pháp cơ bản</b><span class="text-muted">Khởi đầu trải nghiệm trong 1 tháng</span></p>
                  <h3 class="plan-pricing">649,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Hộp Sắc Ngọc Khang vi tảo lục <br>(60 viên / hộp)</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p style="color:#d71149"> <i class="snk snk-heart"></i>Giá gốc <span style="text-decoration: line-through;font-style: italic;">832,000đ</span><br> Tiết kiệm ngay <b>183,000đ</b></p><a onclick="add_to_cart(this,'add_cart',19,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
            <div class="col-md-4 premium-plan">
              <div class="item"><span class="bagde left-badge">Best Value</span>
                  <img src="public/site/clickfunel/images/asset/Giai-phap-ngan-ngua.png" class="img img-responsive" />
                <div class="plan-title">
                  <p class="plan-info"><b>Giải pháp ngăn ngừa</b><span class="text-muted">Hiệu quả tối ưu trong 2 tháng</span></p>
                  <h3 class="plan-pricing">1,249,000 VNĐ									</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>06 Hộp Sắc Ngọc Khang vi tảo lục <br>(60 viên / hộp)</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p style="color:#d71149"> <i class="snk snk-heart"></i>Giá gốc <span style="text-decoration: line-through;font-style: italic;">1,664,000đ</span><br> Tiết kiệm ngay <b>415,000đ</b></p>
                  <p> <i class="snk snk-gift"></i>Tặng túi thời trang canvas trị giá 99,000 (áp dụng 500 khách hàng đầu tiên)</p><a  onclick="add_to_cart(this,'add_cart',21,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
            <div class="col-md-4 diamond-plan">
              <div class="item">
                  <img src="public/site/clickfunel/images/asset/Giai-phap-cai-thien.png" class="img img-responsive" />
                <div class="plan-title">
                  <p class="plan-info"><b>Giải pháp cải thiện</b><span class="text-muted">Kết quả tối đa trong 3 tháng</span></p>
                  <h3 class="plan-pricing">1,849,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>09 Hộp Sắc Ngọc Khang vi tảo lục <br>(60 viên / hộp)</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p style="color:#d71149"> <i class="snk snk-heart"></i>Giá gốc <span style="text-decoration: line-through;font-style: italic;">2,496,000đ</span><br> Tiết kiệm ngay <b>647,000đ</b></p>
                <p> <i class="snk snk-gift"></i>Tặng túi du lịch trị giá 199,000 (áp dụng 200 khách hàng đầu tiên)</p><a  onclick="add_to_cart(this,'add_cart',22,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
          </div>
          <div class="text-xs-center"><a onclick="click_position(this,'click_position',7,1,'')" href="#last-box" title="Tôi muốn mua sản phẩm lẻ khác" class="btn btn-primary btn-lg background-green"><i class="snk snk-right-big"> </i><b>Tôi muốn mua sản phẩm lẻ khác		</b></a></div>
        </div>
      </div>
      <div id="comment-box" class="content-box content-style-8">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">
               
              Người dùng bình luận về vấn đề<span><b>Nám, sạm & tàn nhang </b></span>
            </h3>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="comment-content">
                <ul class="media-list">
                    <?php 
                    if(count($Comments)>0){
                        foreach($Comments as $key=>$row){
                            $class= $key>2 ? "hidden" : "";
                        ?>
                        <li class="media <?php echo $class ?>">
                            <div class="media-body">
                                <h6 class="media-heading"> <b><?php echo $row->Name ?> </b><span>| Ngày <?php echo date('d/m/Y',strtotime($row->Create)) ?> vào lúc <?php echo date('H:i',strtotime($row->Create)) ?></span></h6>
                                <p><?php echo $row->Comment ?></p>
                            </div>
                        </li>
                        <?php 
                        }
                    }
                    ?>
                </ul>
                <?php 
                if(count($Comments)>3){
                ?>
                <p class="text-center"><a onclick="show_more_comment(this)" class="btn-link"> <i class="snk snk-right-dir"></i>Xem thêm bình luận (<?php echo count($Comments)-3 ?>)</a></p>
                <?php 
                }
                ?>
              </div>
              <div class="text-xs-center"><a href="" data-toggle="modal" data-target="#comment-modal" data-dismiss="modal" title="Gửi bình luận của bạn" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Gửi bình luận của bạn</b></a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-box content-style-9 content-combo" id="last-box">
        <div class="container">
          <div class="row no-gutters">
            <div class="col-md-4">
              <div class="item">
                <div class="plan-title">
                  <p class="plan-info"><b>Combo Làm sạch da</b><span class="text-muted">Khởi đầu trải nghiệm</span></p>
                  <h3 class="plan-pricing">539,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 hộp 6 tuýp Sữa rửa mặt Sắc Ngọc Khang 50g</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 hộp 6 chai Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p> <i class="snk snk-heart"></i>Tiết kiệm ngay 31,000 VNĐ</p><a  onclick="add_to_cart(this,'add_cart',23,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
            <div class="col-md-4 premium-plan">
              <div class="item"><span class="bagde left-badge">Best Value</span>
                <div class="plan-title">
                  <p class="plan-info"><b> Combo Kem Sắc Ngọc Khang </b><span class="text-muted">Hiệu quả tối ưu</span></p>
                  <h3 class="plan-pricing">849,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>03 hộp Kem dưỡng da Sắc Ngọc Khang 30g</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p> <i class="snk snk-heart"></i>Tiết kiệm ngay 51,000 VNĐ</p><a  onclick="add_to_cart(this,'add_cart',24,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
            <div class="col-md-4 diamond-plan">
              <div class="item">
                <div class="plan-title">
                  <p class="plan-info"><b>Combo Viên uống Sắc Ngọc Khang ++</b><span class="text-muted">Kết quả tối đa</span></p>
                  <h3 class="plan-pricing">999,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>02 hộp Sắc Ngọc Khang ++</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p> <i class="snk snk-heart"></i>Tiết kiệm ngay 41,000 VNĐ</p><a  onclick="add_to_cart(this,'add_cart',25,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
          </div>
          <div class="row no-gutters" style="margin-top:30px">
            <div class="col-md-4">
              <div class="item">
                <div class="plan-title">
                  <p class="plan-info"><b>Combo Dưỡng đẹp da</b><span class="text-muted">Trải nghiệm trong 2 tháng</span></p>
                  <h3 class="plan-pricing">499,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 tuýp Sữa rửa mặt Sắc Ngọc Khang 100g</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 chai Nước hoa hồng Sắc Ngọc Khang 145ml</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 hộp Kem dưỡng da Sắc Ngọc Khang 30g</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>01 hộp Kem dưỡng da Sắc Ngọc Khang 10g</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p> <i class="snk snk-heart"></i>Tiết kiệm ngay 33,000 VNĐ</p><a  onclick="add_to_cart(this,'add_cart',28,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
            <div class="col-md-4 premium-plan">
              <div class="item"><span class="bagde left-badge">Best Value</span>
                <div class="plan-title">
                  <p class="plan-info"><b> Combo viên uống Sắc Ngọc Khang vi tạo lục</b><span class="text-muted">Ngăn ngừa trong 2 tháng</span></p>
                  <h3 class="plan-pricing">1,149,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>06 hộp viên uống Sắc Ngọc Khang vi tảo lục (60 viên/hộp)</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>Tặng 01 hộp sữa rửa mặt 50g trị giá 36,000 VNĐ</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p> <i class="snk snk-heart"></i>Tiết kiệm ngay 51,000 VNĐ</p><a  onclick="add_to_cart(this,'add_cart',29,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
            <div class="col-md-4 diamond-plan">
              <div class="item">
                <div class="plan-title">
                  <p class="plan-info"><b>Combo Sắc Ngọc Khang ++ bonus</b><span class="text-muted">Hiệu quả trong 3 tháng</span></p>
                  <h3 class="plan-pricing">2,997,000 VNĐ</h3>
                </div>
                <ul class="list-unstyled">
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>06 hộp viên uống Sắc Ngọc Khang ++ (60 viên/hộp)</span></li>
                  <li class="available"><i class="snk snk-ok-circled text-success"></i><span>Tặng 01 hộp kem dưỡng da Sắc Ngọc Khang 10g trị giá 110,000 VNĐ</span></li>
                </ul>
                <p><i class="snk snk-truck"></i>Giao hàng tận nơi</p>
                <p> <i class="snk snk-heart"></i>Tiết kiệm ngay 123,000 VNĐ</p><a  onclick="add_to_cart(this,'add_cart',30,1)" class="btn btn-primary btn-block"> <i class="snk snk-right"> </i>Đặt mua</a>
              </div>
            </div>
          </div>
          <div class="text-xs-center"><a onclick="click_position(this,'click_position',8,1,'')" href="#cta-plan" title="Tôi muốn mua trọn bộ giải pháp" class="btn btn-primary btn-lg background-green"><i class="snk snk-right-big"> </i><b>Tôi muốn mua trọn bộ giải pháp		</b></a></div>
        </div>
      </div>
    </div>
    <div id="order-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade"></div>
    <div id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>Gửi bình luận</span></h5>
          </div>
          <div class="modal-body">
            <form id="form-comment">
            <div class="row">
              <div class="col-xs-12 col-md-6 form-group">
                <input placeholder="Họ tên" type="text" class="form-control" name="Name">
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
              </div>
              <div class="col-xs-12 form-group">
                <textarea placeholder="Nội dung bình luận..." rows="3" name="Comments" class="form-control"></textarea>
              </div>
            </div>
            <div class="row hidden" id="box-alert-comment" style="margin-top:10px;">
                <div class="col-xs-12">
                    <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                </div>
            </div>
                <input type="hidden" name="FunelID" value="1" />
            </form>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_comment(this,'form-comment','send_comment')">Gửi</button>
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
      
      <div id="callme-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>TƯ VẤN CHO TÔI</span></h5>
          </div>
          <div class="modal-body">
            <form id="form-callme">
            <div class="row">
              <div class="col-xs-12 col-md-6 form-group">
                <input placeholder="Họ tên" type="text" class="form-control" name="Name">
              </div>
              <div class="col-xs-12 col-md-6 form-group">
                <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
              </div>
              <div class="col-xs-12 form-group">
                <textarea placeholder="Nội dung cần chúng tôi tư vấn..." rows="3" name="Note" class="form-control"></textarea>
              </div>
            </div>
            <div class="row hidden" id="box-alert-callme" style="margin-top:10px;">
                <div class="col-xs-12">
                    <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                </div>
            </div>
            </form>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_callme(this,'form-callme','send_callme')">Gửi</button>
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="bottom-function">
      <div id="scrollUp" class="scroll-up pull-right"><i class="snk snk-angle-up snk-2x"></i></div>
    </div>
    <a id="alo-phoneIcon" data-toggle="modal" data-target="#callme-modal" class="alo-phone alo-green alo-show">
      <div class="alo-ph-circle"></div>
      <div class="alo-ph-circle-fill"></div>
      <div class="alo-ph-img-circle"><span class="glyphicon glyphicon-earphone"></span></div><span class="alo-ph-text">19006033</span>
  </a>
      <a id="messageus" target="_blank" href="https://m.me/sacngockhang" title="Gửi tin nhắn cho chúng tôi trên Facebook">Nhắn tin cho chúng tôi</a>
    <div class="cta-scroll"><a href="#cta-plan" title="Đặt hàng" class="btn btn-primary"><span>Đặt hàng</span><i class="snk snk-right-big"></i></a><b style="color:#090"><?php echo number_format($FunelOrder + 36) ?> đã mua</b></div>
    <footer>
      <div class="foot-content">
        <div class="container">
          <div class="main-foot row">
            <div class="col-xs-12 col-md-4 logo">
              <h3><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h3>
            </div>
            <div class="col-xs-12 col-md-4">
              <div class="fb-page" data-href="https://www.facebook.com/sacngockhang/" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/sacngockhang/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/sacngockhang/">Sắc Ngọc Khang</a></blockquote></div>
            </div>
            <div class="col-xs-12 col-md-4 logo">
              <h5 class="title">Thông tin liên hệ</h5>
              <p>Số 10 Nguyễn Cửu Đàm, P. Tân Sơn Nhì, Q. Tân Phú, TP HCM</p>
              <p>Email: <a href="mailto:support@sacngockhang.com" class="btn-link">support@sacngockhang.com  </a></p>
              <p>Hotline: <b>19006033</b></p>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom-foot">
        <div class="container">
          <p class="copyright">© 2017 Sắc Ngọc Khang.  All rights reserved.</p>
        </div>
        <!-- End footer-->
      </div>
    </footer>
      <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10&appId=196633390834520";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <!-- End footer-->
    <input type="hidden" id="baseurl" value="<?php echo base_url() ?>" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
    <!-- Latest compiled JavaScript-->
    <script src="public/site/clickfunel/js/tether.min.js" type="text/javascript"></script>
    <script src="public/site/clickfunel/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="public/site/clickfunel/js/script.js"></script>
  </body>
</html>