<div role="document" class="modal-dialog modal-lg">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title"> <span>Thông tin đơn hàng</span></h4>
          </div>
          <div class="modal-body">            
            <div class="row">
              <form id="create-order">
                <div class="col-md-6 col-xs-12">
                  <div class="cart-view">
                    <table class="table">
                      <tbody>
                        <?php 
                        $total = 0;
                        if(is_array($cart) && count($cart)>0){
                            foreach($cart as $key=>$row){
                            ?>
                            <tr class="row">
                                <td class="col-xs-4 col-md-3"><img src="<?php echo $row['Image'] ?>" alt=""></td>
                                <td class="col-xs-8 col-md-9 product-detail">
                                  <h4><?php echo $row['Title'] ?></h4>
                                  <div class="price-wrapper-cart">
                                    <p class="prd-price pull-left"><span class="price"><?php echo number_format($row['Price']) ?>đ </span></p>
                                    <p class="prd-quanlity pull-left">
                                      <input type="number" value="<?php echo $row['Amount'] ?>" name="product-quality" onChange="change_amount_cart(this,'add_cart',<?php echo $key ?>)" class="form-control form-control-sm">
                                    </p>
                                    <a onclick="remove_cart(this,'remove_products',<?php echo $key ?>)" class="group-icon pull-right remove-item"><i class="snk snk-trash-empty"></i> xóa sản phẩm</a>                                    
                                  </div>
                                </td>
                              </tr>
                            <?php 
                                $total = $total + ($row['Price']*$row['Amount']);
                            }
                        }
                        ?>
                      </tbody>
                    </table>
                    <p><span>Tổng giá tiền:</span><b class="pull-right" id="total" total="<?php echo $total ?>"><?php echo number_format($total) ?>đ</b></p>
                  </div>
                  <?php 
                    $show=false;
                     if($show) :
                  ?>
                  <div class="row">
                        <div class="col-xs-12 uudai"><p>Bạn có mã ưu đãi không ?</p></div>
                        <div class="col-md-6 grid-m-7" style="margin-bottom:15px;">
                            <input type="text" class="form-control" id="coupon_code" coupon="0" name="coupon_code" placeholder="Nhập mã giảm giá của bạn vào đây">
                        </div>
                        <div class="col-md-6 grid-m-5">
                            <button type="button" class="btn btn-secondary" onclick="check_coupon_code(this,'check_coupon_code')">Kiểm tra mã</button>
                        </div>
                        <div class="col-xs-12 hidden" id="coupon_message"><div class="alert alert-danger" style="margin-bottom:15px;"></div></div>
                  </div>
                <?php endif;?>
                  <div class="row hidden" id="error-box-cart">
                    <div class="col-xs-12">
                        <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span></span></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 form-group">
                          <div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input id="inlineRadio1" type="radio" name="Sex" value="1" class="form-check-input"><span>Anh </span>
                            </label>
                          </div>
                          <div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input id="inlineRadio2" type="radio" name="Sex" value="0" checked="checked" class="form-check-input"><span>Chị </span>
                            </label>
                          </div>
                        </div>
                        <div class="col-md-6 grid-m-6 form-group">
                          <input placeholder="Họ tên" type="text" name="Name" class="form-control">
                        </div>
                        <div class="col-md-6 grid-m-6 form-group">
                          <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
                        </div>
                        <div class="col-md-6 grid-m-6 form-group">
                          <input id="Email" name="Email" placeholder="Email (không bắt buộc)" type="email" class="form-control">
                        </div>
                        <div class="col-md-6 grid-m-6 form-group">
                          <input placeholder="Ghi chú" name="Note" type="text" class="form-control">
                        </div>
                        <div class="col-xs-12 form-group"><b class="db">Để được phục vụ tốt nhất, vui lòng chọn:</b><br>
                          <div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input id="inlineRadio1" type="radio" onclick="tab(this,1)" checked="checked" name="AddressChoose" value="1" class="form-check-input"><span>Giao tận nơi</span>
                            </label>
                          </div>                          
                        </div>
                        <div class="col-xs-12 address-input">
                          <div class="card">
                            <div class="card-block">
                              <div class="row" id="tab1">
                                <div class="col-md-6 grid-m-6 form-group">
                                  <select id="selectCity" name="CityID" class="form-control" onchange="get_district(this)">
                                      <?php 
                                      if(count($city)>0){
                                          foreach($city as $row){
                                              $selected = $row->ID==30 ? 'selected="selected"' : '' ;
                                              echo "<option value='$row->ID' $selected>$row->Title</option>";
                                          }
                                      }
                                      ?>
                                  </select>
                                </div>
                                <div class="col-md-6 grid-m-6 form-group">
                                  <select id="selectDistrict" onchange="getfee(this,'getfee')" name="DistrictID" class="form-control">
                                    <option value="0">Chọn quận, huyện</option>
                                      <?php 
                                      if(count($district)>0){
                                          foreach($district as $row){
                                              echo "<option value='$row->ID'>$row->Title</option>";
                                          }
                                      }
                                      ?>
                                  </select>
                                </div>
                                <div class="col-xs-12 form-group">
                                  <input id="AddressOrder" name="AddressOrder" placeholder="Số nhà, tên đường, phường/ xã" type="text" class="form-control">
                                </div>
                              </div>                             
                              <div class="row">
                                <div class="col-xs-12 hidden" id="fee" fee="0"><div class="alert alert-warning" style="margin-top:0px;">Chi phí vận chuyển phát sinh cho khu vực bạn vừa chọn : <b>0đ</b></div></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <div class="row hidden" id="error-box">
                          <div class="col-xs-12">
                              <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                          </div>
                      </div>
                      <input type="hidden" name="FunelID" value="1" />
                </div>
              </form>                
            </div>            
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary buttonred animated infinite pulse" onclick="send_order(this,'create-order','create_order')">Đặt hàng		</button>
          </div>
    </div>
</div>
