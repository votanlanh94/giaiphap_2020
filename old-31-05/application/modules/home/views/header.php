    <?php if(isset($show_menu_main) && $show_menu_main): ?>
    <nav class="top-menu navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=$homepage?>">
            <img src="public/site/clickfunel/images/logo.svg" style="max-width:150px;margin:7px auto 10px auto" class="img-responsive" />
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?=$homepage?>">VỀ TRANG CHỦ</a></li>
            <?php 
              if(isset($menu) && count($menu)>0) {
                foreach($menu as $key=>$item) {
                  if($key>0) {
                    $url = $homepage.'/'.$item->Alias;
                    echo "<li><a href='$url' target='_blank'>$item->Title</a></li>";
                  }
                }

              }
            ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <?php endif;?>
    <?php if(isset($show_banner) && $show_banner): ?>
    <div class="container-fluid" id="slider">
      <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
      <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <a href="/dat-hang?click-at=banner#order">
              <img class="first-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-4.jpg" alt="First slide">              
            </a>
            <div class="container">
            </div>
          </div>
          <div class="item">
            <a href="#sieukhuyenmai">
              <img class="second-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-5.jpg" alt="Second slide">
            </a>
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
          <div class="item">
            <a href="#dotpha">
              <img class="third-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-6.jpg" alt="Third slide">
            </a>
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div><!-- /.carousel -->
    </div>
    <?php endif;?>