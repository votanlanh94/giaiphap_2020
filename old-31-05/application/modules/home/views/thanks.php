  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Serum Sắc Ngọc Khang | Dưỡng trắng diệu kỳ</title>
    
    <!-- Bootstrap-->
    <!-- Latest compiled and minified CSS -->
    <link rel="icon" type="image/png" sizes="16x16" href="public/site/clickfunel/images/icon-120x120.png">
    <link rel="icon" type="image/png" sizes="32x32" href="public/site/clickfunel/images/icon-120x120.png">

    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick-theme.css"/>
    <link href="public/site/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/site/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="public/site/clickfunel/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/site/clickfunel/css/timeline.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;amp;subset=vietnamese" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Serum Sắc Ngọc Khang - dưỡng trắng diệu kỳ" />
    <meta property="og:description" content="Serum Sắc Ngọc Khang - hiệu quả gấp 10 lần so với kem trắng da thông thường." />
    <meta property="og:image" content="<?php echo base_url().'/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-1.jpg'; ?>" />

    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-28114834-7"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-28114834-7');
    </script>
  </head>
  <body class="">
    <?php $homepage = "http://sacngockhang.com"; ?>
    <nav class="top-menu navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=$homepage?>">
            <img src="public/site/clickfunel/images/logo.svg" style="max-width:150px;margin:7px auto 10px auto" class="img-responsive" />
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?=$homepage?>">VỀ TRANG CHỦ</a></li>
            <?php 
              if(isset($menu) && count($menu)>0) {
                foreach($menu as $key=>$item) {
                  if($key>0) {
                    $url = $homepage.'/'.$item->Alias;
                    echo "<li><a href='$url' target='_blank'>$item->Title</a></li>";
                  }
                }

              }
            ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container-fluid" id="slider">
      <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
      <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <a href="#binhluan">
              <img class="first-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-1.jpg" alt="First slide">              
            </a>
            <div class="container">
            </div>
          </div>
          <div class="item">
            <a href="#sieukhuyenmai">
              <img class="second-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-2.jpg" alt="Second slide">
            </a>
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
          <div class="item">
            <a href="#dotpha">
              <img class="third-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-3.jpg" alt="Third slide">
            </a>
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div><!-- /.carousel -->
    </div>

   

    <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"> <span>ĐẶT HÀNG THÀNH CÔNG</span></h5>
          </div>
          <div class="modal-body text-center">
            <p>Cảm ơn quý khách hàng đã quan tâm và sử dụng sản phẩm Sắc Ngọc Khang của công ty cổ phần dược phẩm Hoa Thiên Phú. Bộ phận chăm sóc khách hàng của công ty sẽ liên hệ giao hàng cho quý khách trong thời gian sớm nhất. Trân trọng !</p>
              <?php 
              //if($Total>=300000 && $Phone!=''){
                if(1==0){
                ?>
                <p class="alert alert-success">Chúc mừng bạn !!! Đơn hàng của bạn được may mắn nhận được một lượt quay thưởng nhận quà từ chương trình quay thưởng của http://sacngockhang.com.</p>
                <a href="<?php echo 'http://sacngockhang.com/uudaisacngockhang/event_luckydraw/'.$Phone ?>" class="btn btn-danger" style="color:#fff;border-radius:20px;">Quay thưởng nhận quà</a>
                <?php
              }else{
              ?>
                <a href="<?php echo base_url() ?>" class="btn btn-primary" style="color:#fff;border-radius:20px;">TRỞ LẠI TRANG CHỦ</a>
                <a href="https://www.facebook.com/sharer.php?u=<?php echo base_url() ?>" class="btn btn-info" style="color:#fff;border-radius:20px;">CHIA SẺ THÔNG TIN NÀY</a>
              <?php 
              }
              ?>
          </div>
          <div class="modal-footer text-center">
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang">
                <img src="public/site/clickfunel/images/logo.svg" style="max-width:250px;margin:7px auto 10px auto" class="img-responsive">
              </a></h2>
            </div>
          </div>
        </div>
    </div>
  

    <div id="order-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade"></div>
    <div id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>Gửi bình luận</span></h5>
          </div>
          <div class="modal-body">
            <form id="form-comment">
            <div class="row">
              <div class="col-xs-6 form-group">
                <input placeholder="Họ tên" type="text" class="form-control" name="Name">
              </div>
              <div class="col-xs-6 form-group">
                <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
              </div>
              <div class="col-xs-12 form-group">
                <textarea placeholder="Nội dung bình luận..." rows="3" name="Comments" class="form-control"></textarea>
              </div>
            </div>
            <div class="row hidden" id="box-alert-comment" style="margin-top:10px;">
                <div class="col-xs-12">
                    <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                </div>
            </div>
                <input type="hidden" name="FunelID" value="1" />
            </form>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_comment(this,'form-comment','send_comment')">Gửi</button>
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
      
    <div id="callme-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>TƯ VẤN CHO TÔI</span></h5>
          </div>
          <div class="modal-body">
            <form id="form-callme">
            <div class="row">
              <div class="col-xs-6 form-group">
                <input placeholder="Họ tên" type="text" class="form-control" name="Name">
              </div>
              <div class="col-xs-6 form-group">
                <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
              </div>
              <div class="col-xs-12 form-group">
                <textarea placeholder="Nội dung cần chúng tôi tư vấn..." rows="3" name="Note" class="form-control"></textarea>
              </div>
            </div>
            <div class="row hidden" id="box-alert-callme" style="margin-top:10px;">
                <div class="col-xs-12">
                    <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                </div>
            </div>
            </form>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_callme(this,'form-callme','send_callme')">Gửi</button>
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="notice-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>Thông báo</span></h5>
          </div>
          <div class="modal-body">
            <div class="notice-content"></div>
          </div>
          <div class="modal-footer text-center">
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="bottom-function">
      <div id="scrollUp" class="scroll-up pull-right"><i class="snk snk-angle-up snk-2x"></i></div>
    </div>
    <a id="alo-phoneIcon" data-toggle="modal" data-target="#callme-modal" class="alo-phone alo-green alo-show">
      <div class="alo-ph-circle"></div>
      <div class="alo-ph-circle-fill"></div>
      <div class="alo-ph-img-circle"><span class="glyphicon glyphicon-earphone"></span></div><span class="alo-ph-text">19006033</span>
    </a>
    
    <footer>
      <div class="footer-content content">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              <div class="fb-page" data-href="https://www.facebook.com/sacngockhang/" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/sacngockhang/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/sacngockhang/">Sắc Ngọc Khang</a></blockquote></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
              <div class="row logo">
                <div class="col-xs-12 col-sm-4 col-md-2">
                  <img src="public/site/clickfunel/images/logo-htp.png" class="img-responsive" />
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                  <img src="public/site/images/logo-sacngockhang.png" alt="logo serum footer" class="img-responsive" />
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                  <img src="public/site/clickfunel/images/sacngockhang-thong-bao-bo-cong-thuong.png" alt="sắc ngọc khang thông báo bộ công thương" class="img-responsive" />
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 info">
                  <hr>
                  <p>Địa chỉ trụ sở : số 10 Nguyễn Cửu Đàm, phường Tân Sơn Nhì, quận Tân Phú, TPHCM.</p>
                  <p>Tổng đài tư vấn / đặt hàng : <b class="text-htp">19006033</b></p>
                  <p>MST/ĐKKD/QĐTL : 0307205818</p>
                </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
    </footer>
    <!-- End footer-->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId            : '218440352049462',
          autoLogAppEvents : true,
          xfbml            : true,
          version          : 'v2.12'
        });
      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/vi_VN/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="fb-customerchat"
      page_id="141058836006826"
      theme_color="#fa3c4c"
      logged_in_greeting="Xin chào ! Anh/Chị cần hỗ trợ gì ạ ?"
      logged_out_greeting="Xin chào ! Anh/Chị cần hỗ trợ gì ạ ?"
    >
    </div>

    <input type="hidden" id="baseurl" value="<?php echo base_url() ?>" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript-->
    <script src="public/site/clickfunel/js/tether.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="public/site/clickfunel/js/slick.min.js"></script>
    <script src="public/site/clickfunel/js/script.js"></script>
    <script>
      $('#myCarousel').carousel({interval:5000});
    </script>
    
    </body>
</html>

