<?php 
class Home extends CI_Controller { 
	public function __construct() { 
		parent::__construct();
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$this->load->library('template'); 
		$this->template->set_template('site');
		// $staticconfig = $this->db->query("select * from ttp_static_config where ID=1")->row();
		$lang = $this->uri->segment(1);
		$lang = $lang=="en" ? "en" : "vi" ;
		$datafooter = array(
			"static"=>$staticconfig,
			'lang'  =>$lang
		);
		define('CDN_API_ONLINE', CDN.'administrator/api/');
		//load menu
		$api_get_cat_post = $this->call_api('api_order/get_category_post','GET');
		$data['menu'] = array();
		if(!$api_get_cat_post->error) {
			$data['menu'] = $api_get_cat_post->data;
		}
		$data['homepage'] = 'http://sacngockhang.com';
		$data['show_menu_main'] = false;
		$data['show_banner'] = true;
		$this->template->add_js("public/site/clickfunel/js/script.js");
		$this->template->add_js("public/site/clickfunel/js/cart.js");
    $this->template->write_view('header','header',$data);
    $this->template->write_view('footer','footer');
		$this->template->add_doctype();
	}
	
	public $data = array();

	public function index(){
		$this->load->helper('captcha');
		$vals = array(
			'img_path'   => APPPATH.'../assets/captcha/images/',
			'img_url'    => base_url().'assets/captcha/images/',
			'font_path'  => APPPATH.'../public/site/fonts/VNAVAN.TTF',
			'img_width'  => '170',
			'img_height' => '45',
			'expiration' => 7200
		);

		

		$a = rand(1,50);
    $b = rand(1,50);
    $total = $a + $b;
    $cap = $a . ' + ' . $b . ' =';
    $this->data['cap'] = $cap;
    $this->data['total'] = $cap;
    $this->session->set_userdata("captcha",$total);

		$data = array("FunelID"=>1);

    $Comments = $this->call_api('api_order/load_comment','POST',$data);
		$this->data['Comments'] = $Comments->data;

    $getCity = $this->call_api('api_order/get_city','GET');
    $city = $getCity->data;
    $getDistrict = $this->call_api('api_order/get_district', 'GET');
    $district = $getDistrict->data;
    $this->data['city'] = $city;
    $this->data['district'] = $district;

    $this->data['product_id_of_page'] = $product_id_of_page = 75;
    $sendGetProduct = array(
    	'table' => 'ttp_report_products',
    	'column'=> 'ID',
    	'value' => $product_id_of_page,
    	'type'  => 'row'
    );
    $getProduct = $this->call_api('api_order/get_data','POST',$sendGetProduct);


    $this->data['product'] = array();
    if(!$getProduct->isError) {
    	$this->data['product'] = $product = $getProduct->data;
	    
	    //init cart
	    $cart = $this->session->userdata("cart_funel");

	    if(!$cart) {
	    	$cart_data[$product->ID] = array(
	        'Title' => $product->Title,
	        'Price' => $product->Price,
	        'Image' => CDN.$product->PrimaryImage,
	        'Amount'=> 1
	      );
	    	$this->session->set_userdata('cart_funel', $cart_data);
	    }
    }
 
		$this->template->write_view('content','home',$this->data);
		$this->template->render();
	}

	private function call_api($url='index', $method='GET', $data=array()) {
    $url_api = CDN_API_ONLINE.$url;
    $method == 'POST' ? true : false;
    $curl = curl_init();
    $res = array(
      'isError' => false,
      'msg'     => '',
      'data'    => ''
    );
		$data_string = http_build_query($data);
		$ch = curl_init($url_api);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
    return json_decode($response);
    curl_close($curl);
  }
	
	public function thanks_page(){
		$session = $this->session->userdata("customer");
		$total = isset($session['Total']) ? $session['Total'] : 0 ;
		$phone = isset($session['Phone']) ? $session['Phone'] : '' ;
		$this->load->view("thanks",array('Total'=>$total,'Phone'=>$phone));
	}
	
	public function click_position($click=1,$position=0,$funelID=0,$title=''){
		$position = isset($_POST['position']) ? (int)$_POST['position']:$position;
		$funelID = isset($_POST['funel']) ? (int)$_POST['funel'] : $funelID ;
		$title = isset($_POST['title']) ? $_POST['title'] : $title ;
		if($funelID>0){
			$data = array(
				'FunelsID'  => $funelID,
				'Click'     => $click,
				'TitleButton'=> $title,
				'Position'  => $position,
				'YesNo'     => 1,
				'Created'   => date('Y-m-d H:i:s')
			);
			$this->db->insert('ttp_funels_report',$data);
		}
		if($click==1)
		echo json_encode(array('error'=>false,'message'=>''));
	}
	
	
	public function sendmail($Title="HTP | Khách hàng đặt hàng từ website",$message=""){
		if($Title!="" && $message!=''){
			$config = $this->db->query("select * from ttp_email where Published=1 limit 0,1")->row();
			if($config){
				$json = $config->Data!='' ? json_decode($config->Data) : (object)array();
				$this->load->library("email");
				$this->load->library("my_email");
				$data = array(
					'message'       =>$message,
					'user'          =>$json->SMTP_user,
					'password'      =>$json->SMTP_password,
					'protocol'      =>$json->Protocol,
					'smtp_host'     =>$json->SMTP_host,
					'smtp_port'     =>$json->SMTP_port,
					'from_sender'   =>$json->SMTP_user,
					'subject_sender'=>$Title,
					'to_receiver'   =>$json->Email_reciver,
					'name_sender'	=>$Title
				);
				$this->my_email->config($data);
				$this->my_email->sendmail();
			}
		}
	}
	
	public function view_info(){
		//$this->sendmail('HTP | Khách hàng đặt hàng từ website','abcd');
		//echo file_get_contents("http://sms.vietguys.biz/api/?u=hoathien&pwd=r5c8h&from=HoaThienPhu&phone=84966865632&sms=Quy khach vua dat hang thanh cong tai website giaiphap.sacngockhang.com. Nhan vien tu van se xac nhan don hang trong thoi gian som nhat. Cam on Quy Khach.");
		//echo file_get_contents("http://sms.vietguys.biz/api/?u=hoathien&pwd=r5c8h&from=HoaThienPhu&phone=84966865632&sms=Khach hang Vo Trung Hieu dat hang tren website so dien thoai khach hang 01214160374");
		$data = array(
			'u'     => 'hoathien',
			'pwd'   => 'r5c8h',
			'from'  => 'HoaThienPhu',
			'phone' => '84966865632',
			'sms'   => 'Khach hang Vo Trung Hieu dat hang tren website so dien thoai khach hang 01214160374'
		);
		//echo file_get_contents("http://sms.vietguys.biz/api/?".http_build_query($data));
	}
}
?>