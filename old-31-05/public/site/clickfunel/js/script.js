var base_url = "",
    page_order = 1;
$(document).ready(function() {
    $(window).bind("load", function() {
        var a = $("#scrollUp"),
            b = $(".bottom-function");
        $(window).scroll(function() {
            1E3 <= $(this).scrollTop() ? b.show() : b.hide()
        });
        a.click(function() {
            $("html,body").animate({
                scrollTop: 0
            }, 600)
        });
        768 <= $(window).width() && $("ul.nav li.dropdown").hover(function() {
            $(this).find(".dropdown-menu").stop(!0, !0).delay(100).fadeIn(300)
        }, function() {
            $(this).find(".dropdown-menu").stop(!0, !0).delay(100).fadeOut(300)
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        a =
            $("#scrollUp");
        b = $(".bottom-function");
        $(window).scroll(function() {
            1E3 <= $(this).scrollTop() ? b.show() : b.hide()
        });
        a.click(function() {
            $("html,body").animate({
                scrollTop: 0
            }, 600)
        })
    });
    $("#dathang").find(".loading-wrapper").hide()
});

function scrollto(a, b) {
    $("html,body").animate({
        scrollTop: $("#" + b).offset().top - 63
    }, "slow");
    $(".header ul li a").removeClass("current");
    $(a).addClass("current")
}
base_url = $("#baseurl").val();
$("#box-item-order").on("scroll", function() {
    $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && $.post(base_url + "next_order", {
        page: page_order
    }, function(a) {
        $("#box-item-order").append(a);
        page_order++
    }, "html")
});

function tab(a, b) {
    $("#order-modal").find(".tab").addClass("hidden");
    $("#order-modal").find("#tab" + b).removeClass("hidden")
}

function send_comment(a, b, c) {
    $(a).addClass("saving");
    $.post(base_url + c, $("#" + b).serializeArray(), function(c) {
        $(a).removeClass("saving");
        1 == c.error ? ($("#" + b).find("#box-alert-comment").removeClass("hidden"), $("#" + b).find("#box-alert-comment").find(".alert span").html(c.message), $("#" + b).find("input[name='" + c.focus + "']").focus()) : ($("#" + b).find("#box-alert-comment").addClass("hidden"), $("#" + b).find("input[type='text']").val(""), $("#" + b).find("textarea").val(""), $("#comment-modal").modal("hide"), $(".notice-content").html("<p class='text-center'>Ch\u00fang t\u00f4i \u0111\u00e3 ghi nh\u1eadn c\u00e2u h\u1ecfi c\u1ee7a b\u1ea1n v\u00e0 s\u1ebd ph\u1ea3n h\u1ed3i trong th\u1eddi gian s\u1edbm nh\u1ea5t.</p>"),
            $("#notice-modal").modal("show"))
    }, "json")
}

function send_callme(a, b, c) {
    $(a).addClass("saving");
    $.post(base_url + c, $("#" + b).serializeArray(), function(c) {
        $(a).removeClass("saving");
        1 == c.error ? ($("#" + b).find("#box-alert-callme").removeClass("hidden"), $("#" + b).find("#box-alert-callme").find(".alert span").html(c.message), $("#" + b).find("input[name='" + c.focus + "']").focus()) : ($("#" + b).find("#box-alert-callme").removeClass("hidden").find(".alert").removeClass("alert-danger").addClass("alert-success").html(c.message), $("#" + b).find("input[type='text']").val(""),
            $("#" + b).find("textarea").val(""))
    }, "json")
}

function show_more_comment(a) {
    $(a).hide();
    $(a).parents("div.comment-content").find(".hidden").removeClass("hidden")
}

function click_position(a, b, c, d, e) {
    $.post(base_url + b, {
        position: c,
        funel: d,
        title: $(a).attr("title")
    }, function(a) {
        "" != e && $(e).modal("show")
    }, "json")
}
var widthBody = $("body").width(),
    countItem = $(".guide .step-item").length,
    widthItem = $(".guide .step-item").css("width"),
    fullWidth = parseInt(widthItem) * parseInt(countItem),
    ipadSize = 765,
    iphoneSize = 375;

function showDay() {
    var a = fullWidth / 5;
    widthBody <= ipadSize && (a = widthBody / 3);
    widthBody <= iphoneSize && (a = widthItem);
    $("#btn-night").removeClass("clicked");
    $("#btn-day").addClass("clicked");
    $(".guide .step-item:last-child").show("400");
    $(".step-number .num").css({
        background: "linear-gradient(to right, #ab8439 , #e4bf57, #ab8439 , #e4bf57)"
    });
    $(".guide .step-item").animate({
        width: a - 1,
        speed: 400
    })
}

function showNight() {
    var a = widthBody / 4;
    widthBody <= ipadSize && (a = widthBody / 2);
    widthBody <= iphoneSize && (a = widthItem);
    $("#btn-night").addClass("clicked");
    $("#btn-day").removeClass("clicked");
    $(".guide .step-item:last-child").hide();
    $(".step-number .num").css({
        background: "#211f1f"
    });
    $(".guide .step-item").animate({
        width: a,
        speed: 400
    })
}
var heightButton, heightLastItem, heightMenu;
$(window).load(function() {
    heightButton = $("#quytrinh").offset().top;
    heightLastItem = $(".step-item:last-child").offset().top;
    heightMenu = $("header").offset().top
});
$(window).bind("scroll", function() {
    var a = $(window).scrollTop(),
        b = $("#sieukhuyenmai").offset().top,
        c = $("#trainghiem").offset().top,
        d = $("#dotpha").offset().top,
        e = $("#quytrinh").offset().top,
        g = $("#danhhieu").offset().top,
        f = $("#binhluan").offset().top;
    heightButton <= a && a <= heightLastItem ? ($("#btn-day").css({
        position: "fixed",
        top: "50px",
        left: "10%"
    }), $("#btn-night").css({
        position: "fixed",
        top: "50px",
        right: "10%"
    })) : $("#btn-day,#btn-night").css({
        position: "static"
    });
    heightMenu <= a ? $("header").css({
            position: "fixed"
        }) :
        $("header").css({
            position: "relative"
        });
    b >= a && ($("a.current").removeClass("current"), $("#nav-sieukhuyenmai").addClass("current"));
    c - 200 <= a && a < d && ($("a.current").removeClass("current"), $("#nav-trainghiem").addClass("current"));
    d - 200 <= a && a < e && ($("a.current").removeClass("current"), $("#nav-dotpha").addClass("current"));
    e - 200 <= a && a < g && ($("a.current").removeClass("current"), $("#nav-quytrinh").addClass("current"));
    g - 200 <= a && a < f && ($("a.current").removeClass("current"), $("#nav-danhhieu").addClass("current"));
    f - 200 <= a && ($("a.current").removeClass("current"), $("#nav-binhluan").addClass("current"))
});
$(document).ready(function() {
    $(".slick").slick({
        dots: !0,
        infinite: !1,
        speed: 300,
        autoplay: !0,
        autoplaySpeed: 1E3,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: !0,
                dots: !0
            }
        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: !1
            }
        }]
    });
    $(".accordion").collapse()
});
var detailPromotion = $("#chitiet-khuyenmai");
$(document).ready(function() {
    $(detailPromotion).hide()
});

function openContentPromo() {
    // console.log("red");
    $(detailPromotion).show("slow");
    var a = setTimeout(function() {
        scrollto(this, "chitiet-khuyenmai");
        clearTimeout(a)
    }, 1E3)
}

function closeContentPromo() {
    $(detailPromotion).hide("slow");
    var a = setTimeout(function() {
        scrollto(this, "sieukhuyenmai");
        clearTimeout(a)
    }, 1E3)
};