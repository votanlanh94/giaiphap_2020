var base_url = "",
    page_order = 1;
base_url = $("#baseurl").val();
$(document).ready(function() {
    var api_url = $('meta[name="API_URL"]').attr('content');
});
function event_form(a, b, c) {
    $("#" + b).find("#error-box").addClass("hidden");
    var d = $(a).html();
    $(a).html("Loading...");
    $.post(base_url + c, $("#" + b).serializeArray(), function(c) {
        $(a).html(d);
        1 == c.error ? ($("#" + b).find("#error-box").removeClass("hidden"), $("#" + b).find("#error-box").find(".alert span").html(c.message), $("#" + b).find("input[name='" + c.focus + "']").focus(), "Captcha" == c.focus && $("#" + b).find("#imgcapt").html(c.image)) : $("#" + b).html("<img src='public/site/images/register/" + c.gift + ".png' style='margin:15px auto' class='img-responsive' />" +
            c.message).addClass("alert alert-success")
    }, "json")
}

function load_box_check_order(a, b) {
    $.get(base_url + b, function(a) {
        $("#create-order").html(a)
    });
    $("#create-order").modal("show")
}

function check_coupon_code_frontend(a, b) {
    $(a).addClass("saving");
    var c = $("form#create-order-page").find("#total").val(),
        d = $("form#create-order-page").find("#fee").attr("fee"),
        e = $("form#create-order-page").find("#coupon_code").val();
    "readonly" != $("form#create-order-page").find("#coupon_code").attr("readonly") && "" != e && $.post(base_url + b, {
        fee: d,
        total: c,
        coupon: e
    }, function(b) {
        $(a).removeClass("saving");
        1 == b.error ? ($(b.focus).focus(), $("form#create-order-page").find("#coupon_message").removeClass("hidden").find(".alert").html(b.message)) :
            ($("form#create-order-page").find("#coupon_code").attr("readonly", !0), $("form#create-order-page").find("#coupon_code").attr("coupon", b.coupon), $("form#create-order-page").find("#coupon_message").removeClass("hidden").find(".alert").addClass("alert-success").removeClass("alert-danger").html(b.message), getfee_frontend($("form#create-order-page").find("#selectDistrict"), "getfee"))
    }, "json")
}

function check_coupon_code(a, b) {
    $(a).addClass("saving");
    var c = $("#create-order").find("#total").attr("total"),
        d = $("#create-order").find("#fee").attr("fee"),
        e = $("#create-order").find("#coupon_code").val();
    $("form#create-order-page").find("#coupon_code").val();
    "readonly" != $("#create-order").find("#coupon_code").attr("readonly") && "" != e && $.post(base_url + b, {
        fee: d,
        total: c,
        coupon: e
    }, function(b) {
        $(a).removeClass("saving");
        1 == b.error ? ($(b.focus).focus(), $("#create-order").find("#coupon_message").removeClass("hidden").find(".alert").html(b.message)) :
            ($("#create-order").find("#coupon_code").attr("readonly", !0), $("#create-order").find("#coupon_code").attr("coupon", b.coupon), $("#create-order").find("#coupon_message").removeClass("hidden").find(".alert").addClass("alert-success").removeClass("alert-danger").html(b.message), getfee($("#create-order").find("#selectDistrict"), "getfee"))
    }, "json")
}

function getfee(a, b, c) {
    var api_url = $('meta[name="API_URL"]').attr('content');
    c = $("#create-order").find("#total").attr("total");
    var d = $("#create-order").find("#coupon_code").attr("coupon");
    0 < $(a).val() && $.post(api_url + 'api_order/' + b, {
        district: $(a).val(),
        total: c,
        coupon: d
    }, function(a) {
        console.log(a);
        $("#create-order").find("#fee").attr("fee", a.number_fee).removeClass("hidden").find(".alert").html(a.fee)
    }, "json")
}

function getfee_frontend(a, b, c) {
    var api_url = $('meta[name="API_URL"]').attr('content');
    c = $("form#create-order-page").find("#total").val();
    var d = $("form#create-order-page").find("#coupon_code").attr("coupon");
    0 < $(a).val() && $.post(api_url + 'api_order/' + b, {
        district: $(a).val(),
        total: c,
        coupon: d
    }, function(a) {
        $("form#create-order-home-page").find("#fee").attr("fee", a.number_fee).removeClass("hidden").find(".alert").html(a.fee)
    }, "json")
}

function check_order(a, b, c) {
    $("#create-order").find("#error-box-cart").addClass("hidden");
    $(a).addClass("saving");
    $.post(base_url + c, $("#" + b).serializeArray(), function(d) {
        $(a).removeClass("saving");
        1 == d.error ? ($("#create-order").find("#error-box").removeClass("hidden"), $("#create-order").find("#error-box").find(".alert span").html(d.message), $("#create-order").find("#" + b).find("input[name='" + d.focus + "']").focus()) : $("#create-order").load(base_url + "load_order_details")
    }, "json")
}

function add_to_cart_and_redirect(a, b, c, d, e) {
    $.post(base_url + b, {
        products: c,
        amount: d
    }, function(a) {
    	// console.log(a);
      // window.location.href=window.location.origin + '/dat-hang?click-at=' + e +'#order';
        location.reload();
        // $.get(base_url + "load_order", function(a) {
        // })
    }, "json");
}

function add_to_cart(a, b, c, d) {
    $.post(base_url + b, {
        products: c,
        amount: d,
        async: false
    }, function(a) {
    	// console.log(a);
    	$("#create-order").find("#total").attr('total',a.data);
    	$("#create-order").find("#total").html(number_format(a.data, 0, "-", ",") + " đ");
    }, "json");
}

function remove_cart(a, b, c) {
    $.post(base_url + b, {
        products: c
    }, function(a) {

        if( 0 == a.error ) {
            $.get(base_url + "load_order", function(a) {
                // $("#create-order").html(a);
                window.location.reload();
            })
        } else {
            $("#create-order").find("#error-box").addClass("hidden"), $("#create-order").find("#error-box-cart").removeClass("hidden");
            $("#create-order").find("#error-box-cart span").html(a.message);
        }

    }, "json")
}

function set_address(a, b, c) {
    $("#create-order").find("#selectCity").val(b);
    $.post(base_url + "load_district_from_city", {
        CityID: b
    }, function(a) {
        $("#create-order").find("#selectDistrict").html(a);
        $("#create-order").find("#selectDistrict").val(c)
    }, "html");
    $("#create-order").find("#AddressOrder").val($(a).parent("label").text().trim())
}

function get_district(a) {
    a = $(a).val();
    $.post(base_url + "load_district_from_city", {
        CityID: a
    }, function(a) {
        $("#create-order").find("#selectDistrict").html(a);
        $("#create-order").find("#selectDistrict").change();
        $("form#create-order-home-page").find("#selectDistrict").html(a);        
        $("form#create-order-home-page").find("#selectDistrict").change()
    }, "html")
}

function change_quantity(a, b) {
    var c = parseInt($(a).val());
    0 < c && $.post(base_url + "add_cart", {
        products: b,
        amount: c
    }, function(a) {
        a.error || ($("form#create-order-page").find(".price").html(number_format(a.data, 0, "-", ",") + " VN\u0110"), $("form#create-order-page").find("#total").val(a.data), getfee_frontend($("form#create-order-page").find("#selectDistrict"), "getfee"))
    }, "json")
}
function change_quantity_fe(a, b) {
    var c = parseInt($(a).val());
    0 < c && $.post(base_url + "add_cart", {
        products: b,
        amount: c,
        cart_name: 'cart_fe'
    }, function(a) {        
        a.error || ($("form#create-order-home-page").find(".price").html(number_format(a.data, 0, "-", ",") + " VN\u0110"), $("form#create-order-page").find("#total").val(a.data), getfee_frontend($("form#create-order-page").find("#selectDistrict"), "getfee"))
    }, "json")
}

function number_format(a, b, c, d) {
    var e = isNaN(b = Math.abs(b)) ? 2 : b;
    b = void 0 == c ? "," : c;
    d = void 0 == d ? "." : d;
    c = 0 > a ? "-" : "";
    var g = parseInt(a = Math.abs(+a || 0).toFixed(e)) + "",
        f = 3 < (f = g.length) ? f % 3 : 0;
    return c + (f ? g.substr(0, f) + d : "") + g.substr(f).replace(/(\d{3})(?=\d)/g, "$1" + d) + (e ? b + Math.abs(a - g).toFixed(e).slice(2) : "")
}

function send_order(a, b, c) {
    $("#create-order").find("#error-box-cart").addClass("hidden");
    $(a).addClass("saving");
    $.post(base_url + c, $("#" + b).serializeArray(), function(c) {
        $(a).removeClass("saving");
        1 == c.error ? ($("#"+b).find("#error-box").removeClass("hidden"), $("#"+b).find("#error-box").find(".alert span").html(c.message), b = "create-order-home-page", $("form#create-order-home-page").find("#error-box").removeClass("hidden"), $("form#create-order-home-page").find("#error-box").find(".alert span").html(c.message),
            $("#" + b).find("input[name='" + c.focus + "']").focus(), $("form#create-order-home-page").find("#error-box-cart").removeClass("hidden"), $("form#create-order-home-page").find("#error-box-cart").find(".alert span").html(c.message)) : window.location.href = base_url + "thanks_page"
    }, "json")
}

function change_amount_cart(a, b, c) {
    var d = parseInt($(a).val());
    add_to_cart(a, b, c, d);
    // window.location.reload();
}

//add 12/2020
//loop time weekly
var curday;
var secTime;
var ticker;

getSeconds();

function getSeconds() {
    var nowDate = new Date();
    var dy = 6 ; //Sunday through Saturday, 0 to 6
    var countertime = new Date(nowDate.getFullYear(),nowDate.getMonth(),nowDate.getDate(),20,0,0); //20 out of 24 hours = 8pm

    var curtime = nowDate.getTime(); //current time
    var atime = countertime.getTime(); //countdown time
    var diff = parseInt((atime - curtime)/1000);
    if (diff > 0) { curday = dy - nowDate.getDay() }
    else { curday = dy - nowDate.getDay() -1 } //after countdown time
    if (curday < 0) { curday += 7; } //already after countdown time, switch to next week
    if (diff <= 0) { diff += (86400 * 7) }
    startTimer (diff);
}

function startTimer(secs) {
    secTime = parseInt(secs);
    ticker = setInterval("tick()",1000);
    tick(); //initial count display
}

function tick() {
    var secs = secTime;
    if (secs>0) {
        secTime--;
    }
    else {
        clearInterval(ticker);
        getSeconds(); //start over
    }

    var days = Math.floor(secs/86400);
    secs %= 86400;
    var hours= Math.floor(secs/3600);
    secs %= 3600;
    var mins = Math.floor(secs/60);
    secs %= 60;

    //update the time display
    document.getElementById("days").innerText =((curday < 10 ) ? "0" : "" ) + curday;
    document.getElementById("hours").innerText = ((hours < 10 ) ? "0" : "" ) + hours;
    document.getElementById("minutes").innerText = ( (mins < 10) ? "0" : "" ) + mins;
    document.getElementById("seconds").innerText = ( (secs < 10) ? "0" : "" ) + secs;
}
