var base_url = "",
    page_order = 1;
$(document).ready(function() {
    $(window).bind("load", function() {
        var a = $("#scrollUp"),
            b = $(".bottom-function");
        $(window).scroll(function() {
            1E3 <= $(this).scrollTop() ? b.show() : b.hide()
        });
        a.click(function() {
            $("html,body").animate({
                scrollTop: 0
            }, 600)
        });
        768 <= $(window).width() && $("ul.nav li.dropdown").hover(function() {
            $(this).find(".dropdown-menu").stop(!0, !0).delay(100).fadeIn(300)
        }, function() {
            $(this).find(".dropdown-menu").stop(!0, !0).delay(100).fadeOut(300)
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        a =
            $("#scrollUp");
        b = $(".bottom-function");
        $(window).scroll(function() {
            1E3 <= $(this).scrollTop() ? b.show() : b.hide()
        });
        a.click(function() {
            $("html,body").animate({
                scrollTop: 0
            }, 600)
        })
    });
    $("#dathang").find(".loading-wrapper").hide()
});

function scrollto(a, b) {
    $("html,body").animate({
        scrollTop: $("#" + b).offset().top - 63
    }, "slow");
    $(".header ul li a").removeClass("current");
    $(a).addClass("current")
}
base_url = $("#baseurl").val();
$("#box-item-order").on("scroll", function() {
    $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && $.post(base_url + "next_order", {
        page: page_order
    }, function(a) {
        $("#box-item-order").append(a);
        page_order++
    }, "html")
});

function tab(a, b) {
    $("#order-modal").find(".tab").addClass("hidden");
    $("#order-modal").find("#tab" + b).removeClass("hidden")
}

function send_comment(a, b, c) {
    $(a).addClass("saving");
    $.post(base_url + c, $("#" + b).serializeArray(), function(c) {
        $(a).removeClass("saving");
        1 == c.error ? ($("#" + b).find("#box-alert-comment").removeClass("hidden"), $("#" + b).find("#box-alert-comment").find(".alert span").html(c.message), $("#" + b).find("input[name='" + c.focus + "']").focus()) : ($("#" + b).find("#box-alert-comment").addClass("hidden"), $("#" + b).find("input[type='text']").val(""), $("#" + b).find("textarea").val(""), $("#comment-modal").modal("hide"), $(".notice-content").html("<p class='text-center'>Ch\u00fang t\u00f4i \u0111\u00e3 ghi nh\u1eadn c\u00e2u h\u1ecfi c\u1ee7a b\u1ea1n v\u00e0 s\u1ebd ph\u1ea3n h\u1ed3i trong th\u1eddi gian s\u1edbm nh\u1ea5t.</p>"),
            $("#notice-modal").modal("show"))
    }, "json")
}

function send_callme(a, b, c) {
    $(a).addClass("saving");
    $.post(base_url + c, $("#" + b).serializeArray(), function(c) {
        $(a).removeClass("saving");
        1 == c.error ? ($("#" + b).find("#box-alert-callme").removeClass("hidden"), $("#" + b).find("#box-alert-callme").find(".alert span").html(c.message), $("#" + b).find("input[name='" + c.focus + "']").focus()) : ($("#" + b).find("#box-alert-callme").removeClass("hidden").find(".alert").removeClass("alert-danger").addClass("alert-success").html(c.message), $("#" + b).find("input[type='text']").val(""),
            $("#" + b).find("textarea").val(""))
    }, "json")
}

function show_more_comment(a) {
    $(a).hide();
    $(a).parents("div.comment-content").find(".hidden").removeClass("hidden")
}

function click_position(a, b, c, d, e) {
    $.post(base_url + b, {
        position: c,
        funel: d,
        title: $(a).attr("title")
    }, function(a) {
        "" != e && $(e).modal("show")
    }, "json")
}
var widthBody = $("body").width(),
    countItem = $(".guide .step-item").length,
    widthItem = $(".guide .step-item").css("width"),
    fullWidth = parseInt(widthItem) * parseInt(countItem),
    ipadSize = 765,
    iphoneSize = 375;

function showDay() {
    var a = fullWidth / 5;
    widthBody <= ipadSize && (a = widthBody / 3);
    widthBody <= iphoneSize && (a = widthItem);
    $("#btn-night").removeClass("clicked");
    $("#btn-day").addClass("clicked");
    $(".guide .step-item:last-child").show("400");
    $(".step-number .num").css({
        background: "linear-gradient(to right, #ab8439 , #e4bf57, #ab8439 , #e4bf57)"
    });
    $(".guide .step-item").animate({
        width: a - 1,
        speed: 400
    })
}

function showNight() {
    var a = widthBody / 4;
    widthBody <= ipadSize && (a = widthBody / 2);
    widthBody <= iphoneSize && (a = widthItem);
    $("#btn-night").addClass("clicked");
    $("#btn-day").removeClass("clicked");
    $(".guide .step-item:last-child").hide();
    $(".step-number .num").css({
        background: "#211f1f"
    });
    $(".guide .step-item").animate({
        width: a,
        speed: 400
    })
}
var heightButton, heightLastItem, heightMenu;
$(window).load(function() {
    // heightButton = $("#quytrinh").offset().top;
    // heightLastItem = $(".step-item:last-child").offset().top;
    heightMenu = $("header").offset().top
});
$(window).bind("scroll", function() {
    var a = $(window).scrollTop(),
        b = $("#thanhphan").offset().top,
        c = $("#congthucmoi").offset().top,
        d = $("#nhamayhiendai").offset().top,
        e = $("#danhhieu").offset().top,
        f = $("#hoahau").offset().top,
        g = $("#trainghiem").offset().top;
        // h = $("#quytrinh").offset().top;

    heightButton <= a && a <= heightLastItem ? ($("#btn-day").css({
        position: "fixed",
        top: "50px",
        left: "10%"
    }), $("#btn-night").css({
        position: "fixed",
        top: "50px",
        right: "10%"
    })) : $("#btn-day,#btn-night").css({
        position: "static"
    });
    heightMenu <= a ? $("header").css({
        position: "fixed"
    }) :
    $("header").css({
        position: "relative"
    });

    //add class current for block b
    b >= a && ($("a.current").removeClass("current"), $("#nav-biquyet").addClass("current"));
    //add class current for block c
    c - 200 <= a && a < d && ($("a.current").removeClass("current"), $("#nav-congthucmoi").addClass("current"));
    //add class current for block d
    d - 200 <= a && a < e && ($("a.current").removeClass("current"), $("#nav-nhamay").addClass("current"));
    //add class current for block e
    e - 200 <= a && a < f && ($("a.current").removeClass("current"), $("#nav-danhhieu").addClass("current"));
    //add class current for block f
    f - 200 <= a && a < g && ($("a.current").removeClass("current"), $("#nav-hoahau").addClass("current"));
    //add class current for block g
    g - 200 <= a && a && ($("a.current").removeClass("current"), $("#nav-trainghiem").addClass("current"));

    // h - 200 <= a  && ($("a.current").removeClass("current"), $("#nav-quytrinh").addClass("current"));
});
$(document).ready(function() {
    $(".slick").slick({
        dots: !0,
        infinite: !1,
        speed: 300,
        autoplay: !0,
        autoplaySpeed: 1E3,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: !0,
                dots: !0
            }
        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: !1
            }
        }]
    });
    $(".accordion").collapse()
});
var detailPromotion = $("#chitiet-khuyenmai");
$(document).ready(function() {
    $(detailPromotion).hide()
});

function openContentPromo() {
    // console.log("red");
    $(detailPromotion).show("slow");
    var a = setTimeout(function() {
        scrollto(this, "chitiet-khuyenmai");
        clearTimeout(a)
    }, 1E3)
}

function closeContentPromo() {
    $(detailPromotion).hide("slow");
    var a = setTimeout(function() {
        scrollto(this, "sieukhuyenmai");
        clearTimeout(a)
    }, 1E3)
};

//POPUPMUAHANG/////////////////////////
    $(document).ready(function () {
        var hr = (new Date()).getHours();
        var view_random = 100 + getRandomInt(10,200);
        if( hr > 22 || hr < 8) {
            view_random = 50 + getRandomInt(10,50);
        }
        if( hr > 8 && hr < 16) {
            view_random = 200 + getRandomInt(100,300);
        }
        $('.view-page > span').html(view_random);
        setInterval(show_popup_offer, 20000);
    });

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

function show_popup_offer() {
    var products = $('#products_array').val();
    product = JSON.parse(atob(products));
    // console.log(product);
    var locs = Array('Hà Nội', 'TP.HCM', 'Hải Phòng', 'Khánh Hòa', 'Nha Trang', 'Đồng Nai', 'Vũng Tàu', 'Quảng Ngãi', 'Bến tre');
    var qtys = Array('3', '6');
    var tims = Array('40', '27', '18', '12');
    var loc = locs[Math.floor(Math.random() * locs.length)];
    var qty = qtys[Math.floor(Math.random() * qtys.length)];
    var tim = tims[Math.floor(Math.random() * tims.length)];
    // var popmsg = 'Khách hàng ở ' + loc + ' vừa đặt mua <span class="proname"> ' + qty + ' sản phẩm ' + product.Title +'</span> <span class="poptime">' + tim + ' phút trước</span>';
    var popmsg = 'Khách hàng ở ' + loc + ' vừa đặt mua <span class="proname"> ' + 'Combo' + qty + ' viên uống Sắc Ngọc Khang</span> <span class="poptime">' + tim + ' phút' +
    ' trước</span>';
    $('.popimg > img').attr('src',product.Image);
    $('.popimg > img').attr('alt', product.Title);
    $('.popmsg').html(popmsg);
    $('#popmua').toggleClass('popanimation');
}

function add_to_cart_and_redirect(a, b, c, d, e) {
    $.post(base_url + b, {
        products: c,
        amount: d
    }, function(a) {
        // console.log(a);
        window.location.href=window.location.origin + '/dat-hang?click-at=' + e +'#order';
        // $.get(base_url + "load_order", function(a) {
        // })
    }, "json");
}

// const second = 1000,
//       minute = second * 60,
//       hour = minute * 60,
//       day = hour * 24;

// let countDown = new Date('Dec 31 2019 23:59:59').getTime(),
//     x = setInterval(function() {

//       let now = new Date().getTime(),
//           distance = countDown - now;

//         document.getElementById('days').innerText = Math.floor(distance / (day)),
//         document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
//         document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
//         document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);
//     }, second)

//loop time weekly
//     var curday;
//     var secTime;
//     var ticker;
//
//     getSeconds();
//
//     function getSeconds() {
//      var nowDate = new Date();
//      var dy = 6 ; //Sunday through Saturday, 0 to 6
//      var countertime = new Date(nowDate.getFullYear(),nowDate.getMonth(),nowDate.getDate(),20,0,0); //20 out of 24 hours = 8pm
//
//      var curtime = nowDate.getTime(); //current time
//      var atime = countertime.getTime(); //countdown time
//      var diff = parseInt((atime - curtime)/1000);
//      if (diff > 0) { curday = dy - nowDate.getDay() }
//      else { curday = dy - nowDate.getDay() -1 } //after countdown time
//      if (curday < 0) { curday += 7; } //already after countdown time, switch to next week
//      if (diff <= 0) { diff += (86400 * 7) }
//      startTimer (diff);
//     }
//
//     function startTimer(secs) {
//      secTime = parseInt(secs);
//      ticker = setInterval("tick()",1000);
//      tick(); //initial count display
//     }
//
//     function tick() {
//          var secs = secTime;
//          if (secs>0) {
//           secTime--;
//          }
//          else {
//           clearInterval(ticker);
//           getSeconds(); //start over
//          }
//
//          var days = Math.floor(secs/86400);
//          secs %= 86400;
//          var hours= Math.floor(secs/3600);
//          secs %= 3600;
//          var mins = Math.floor(secs/60);
//          secs %= 60;
//
//          //update the time display
//          document.getElementById("days").innerText =((curday < 10 ) ? "0" : "" ) + curday;
//          document.getElementById("hours").innerText = ((hours < 10 ) ? "0" : "" ) + hours;
//          document.getElementById("minutes").innerText = ( (mins < 10) ? "0" : "" ) + mins;
//          document.getElementById("seconds").innerText = ( (secs < 10) ? "0" : "" ) + secs;
//     }