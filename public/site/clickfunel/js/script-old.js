$(document).ready(function(){
	$(window).bind("load", function() {
		// scrollUp
		var scrollButton = $('#scrollUp');
		var bottomFuntion = $('.bottom-function');
		$(window).scroll(function(){
			$(this).scrollTop() >= 1000 ? bottomFuntion.show() : bottomFuntion.hide();			
			// console.log($(this).scrollTop());
		});
		
		// Click on scrollUp button
		scrollButton.click(function(){
			// console.log("click");
			$('html,body').animate({scrollTop:0},600);
			
		});

		// Fix main-nav
		if ($(window).width() >= 768) {
			$('ul.nav li.dropdown').hover(function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
			}, function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
			});
		}


		// Auto pop-up promoModal
		// $('#promoModal').modal('show')

		// Enable tooltips everywhere
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})

		// scrollUp
		var scrollButton = $('#scrollUp');
		var bottomFuntion = $('.bottom-function');
		$(window).scroll(function(){
			$(this).scrollTop() >= 1000 ? bottomFuntion.show() : bottomFuntion.hide();			
			// console.log($(this).scrollTop());
		});
		
		// Click on scrollUp button
		scrollButton.click(function(){
			// console.log("click");
			$('html,body').animate({scrollTop:0},600);
			
		});

		
	});
});

function scrollto(ob,element){
    var object = element;
    $('html,body').animate({scrollTop: $('#'+object).offset().top-63},'slow');
    $(".header ul li a").removeClass('current');
    $(ob).addClass('current');
}

var base_url = $("#baseurl").val();

function add_to_cart(ob,action,id,amount){
    $.post(base_url+action,{products:id,amount:amount},function(result){
        $.get(base_url+"load_order",function(result){
            $("#order-modal").html(result);
        });
    },"json");
    $("#order-modal").modal("show");
}

function remove_cart(ob,action,id){
    $.post(base_url+action,{products:id},function(result){
        if(result.error==false){
            $.get(base_url+"load_order",function(result){
                $("#order-modal").html(result);
            });
        }else{
            $("#order-modal").find("#error-box").addClass("hidden");
            $("#order-modal").find("#error-box-cart").removeClass("hidden");
            $("#order-modal").find("#error-box-cart span").html(result.message);
        }
    },"json");
}

function set_address(ob,city,district){
    $("#order-modal").find("#selectCity").val(city);
    $.post(base_url+"load_district_from_city",{CityID:city},function(result){
        $("#order-modal").find("#selectDistrict").html(result);
        $("#order-modal").find("#selectDistrict").val(district);
    },"html");
    $("#order-modal").find("#AddressOrder").val($(ob).parent("label").text().trim());
}

function get_district(ob){
    var city = $(ob).val();
    $.post(base_url+"load_district_from_city",{CityID:city},function(result){
        $("#order-modal").find("#selectDistrict").html(result);
    },"html");
}

function tab(ob,tab){
    $("#order-modal").find(".tab").addClass("hidden");
    $("#order-modal").find("#tab"+tab).removeClass("hidden");
}

function send_order(ob,form,action){
    $("#order-modal").find("#error-box-cart").addClass("hidden");
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#order-modal").find("#error-box").removeClass("hidden");
            $("#order-modal").find("#error-box").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            window.location = base_url+"thanks_page";
        }
    },'json');
}

function change_amount_cart(ob,action,id){
    var amount = parseInt($(ob).val());
    add_to_cart(ob,action,id,amount);
}

function send_comment(ob,form,action){
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#"+form).find("#box-alert-comment").removeClass("hidden");
            $("#"+form).find("#box-alert-comment").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#"+form).find("#box-alert-comment").addClass("hidden");
            $("#"+form).find("input[type='text']").val('');
            $("#"+form).find("textarea").val('');
            $("#comment-modal").modal("hide");
            $.post(base_url+'load_comment',{FunnelID:1},function(result){
               $(".comment-content").html(result);
            });
        }
    },'json');
}

function send_callme(ob,form,action){
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#"+form).find("#box-alert-callme").removeClass("hidden");
            $("#"+form).find("#box-alert-callme").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#"+form).find("#box-alert-callme").addClass("hidden");
            $("#"+form).find("input[type='text']").val('');
            $("#"+form).find("textarea").val('');
            $("#callme-modal").modal("hide");
        }
    },'json');
}

function show_more_comment(ob){
    $(ob).hide();
    $(ob).parents('div.comment-content').find(".hidden").removeClass("hidden");
}

function click_position(ob,action,position,funel,showform){
    $.post(base_url+action,{position:position,funel:funel,title:$(ob).attr('title')},function(result){
        if(showform!=''){
            $(showform).modal('show');
        }
    },'json');
}