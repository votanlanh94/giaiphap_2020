var base_url = "";
var page_order = 1;

$(document).ready(function(){
    $(window).bind("load", function() {
        // scrollUp
        var scrollButton = $('#scrollUp');
        var bottomFuntion = $('.bottom-function');
        $(window).scroll(function(){
            $(this).scrollTop() >= 1000 ? bottomFuntion.show() : bottomFuntion.hide();          
            // console.log($(this).scrollTop());
        });

        // Click on scrollUp button
        scrollButton.click(function(){
            // console.log("click");
            $('html,body').animate({scrollTop:0},600);
        });

        // Fix main-nav
        if ($(window).width() >= 768) {
            $('ul.nav li.dropdown').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
            });
        }

        // Auto pop-up promoModal
        // $('#promoModal').modal('show')

        // Enable tooltips everywhere
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        // scrollUp
        var scrollButton = $('#scrollUp');
        var bottomFuntion = $('.bottom-function');
        $(window).scroll(function(){
            $(this).scrollTop() >= 1000 ? bottomFuntion.show() : bottomFuntion.hide();          
            // console.log($(this).scrollTop());
        });
        
        // Click on scrollUp button
        scrollButton.click(function(){
            // console.log("click");
            $('html,body').animate({scrollTop:0},600);
            
        });

        
    });

    $('#dathang').find('.loading-wrapper').hide();

});

function scrollto(ob,element){
    var object = element;
    $('html,body').animate({scrollTop: $('#'+object).offset().top-63},'slow');
    $(".header ul li a").removeClass('current');
    $(ob).addClass('current');
}

var base_url = $("#baseurl").val();

$('#box-item-order').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        $.post(base_url+"next_order",{page:page_order},function(result){
            $('#box-item-order').append(result);
            page_order++;
        },"html");
    }
});

function event_form(ob,form,action){
    $("#"+form).find("#error-box").addClass("hidden");
    var html = $(ob).html();
    $(ob).html("Loading...");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).html(html);
        if(result.error==true){
            $("#"+form).find("#error-box").removeClass("hidden");
            $("#"+form).find("#error-box").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
            if(result.focus=="Captcha"){
                $("#"+form).find("#imgcapt").html(result.image);
            }
        }else{
            $("#"+form).html("<img src='public/site/images/register/"+result.gift+".png' style='margin:15px auto' class='img-responsive' />"+result.message).addClass("alert alert-success");
        }
    },'json');
}

function load_box_check_order(ob,action){
    $.get(base_url+action,function(result){
        $("#order-modal").html(result);
    });
    $("#order-modal").modal("show");
}

function check_coupon_code(ob,action){
    $(ob).addClass("saving");
    var total = $("#order-modal").find("#total").attr("total");
    var fee = $("#order-modal").find("#fee").attr("fee");
    var coupon = $("#order-modal").find("#coupon_code").val();
    var couponFrontEnd = $("form#create-order").find("#coupon_code").val();
    var status = $("#order-modal").find("#coupon_code").attr("readonly");
    if(status!="readonly" && coupon!=''){
        $.post(base_url+action,{fee:fee,total:total,coupon:coupon},function(result){
            $(ob).removeClass("saving");
            if(result.error==true){
                $(result.focus).focus();
                $("#order-modal").find("#coupon_message").removeClass("hidden").find('.alert').html(result.message);
                
                if(couponFrontEnd != '') {                    
                    $("form#create-order").find("#error-box").removeClass("hidden").find('.alert').html(result.message);
                }  else {
                    $("form#create-order").find("#error-box").addClass("hidden");
                }
            }else{
                $("#order-modal").find("#coupon_code").attr("readonly",true);
                $("#order-modal").find("#coupon_code").attr("coupon",result.coupon);
                $("#order-modal").find("#coupon_message").removeClass("hidden").find('.alert').addClass("alert-success").removeClass("alert-danger").html(result.message);
                getfee($("#order-modal").find("#selectDistrict"),"getfee");

                $("form#create-order").find("#coupon_code").attr("readonly",true);
                $("form#create-order").find("#coupon_code").attr("coupon",result.coupon);
                $("form#create-order").find("#error-box").removeClass("hidden").find('.alert').addClass("alert-success").removeClass("alert-danger").html(result.message);
                getfee($("form#create-order").find("#selectDistrict"),"getfee");
            }
        },'json');
    }
}

function getfee(ob,action,totala){
    var total = $("#order-modal").find("#total").attr("total");
    var coupon = $("#order-modal").find("#coupon_code").attr("coupon");    
    if($(ob).val()>0){
        $.post(base_url+action,{district:$(ob).val(),total:total,coupon:coupon},function(result){
            $("#order-modal").find("#fee").attr("fee",result.number_fee).removeClass("hidden").find('.alert').html(result.fee);
        },'json');
    }
}

function getfee_frontend(ob,action,totala){
    var total = $("form#create-order").find("#total").val();
    var coupon = $("form#create-order").find("#coupon_code").attr("coupon");    
    if($(ob).val()>0){
        $.post(base_url+action,{district:$(ob).val(),total:total,coupon:coupon},function(result){
            $("form#create-order").find("#fee").attr("fee",result.number_fee).removeClass("hidden").find('.alert').html(result.fee);
        },'json');
    }
}

function check_order(ob,form,action){
    $("#order-modal").find("#error-box-cart").addClass("hidden");
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#order-modal").find("#error-box").removeClass("hidden");
            $("#order-modal").find("#error-box").find(".alert span").html(result.message);
            $("#order-modal").find("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#order-modal").load(base_url+"load_order_details");
        }
    },'json');
}

function add_to_cart(ob,action,id,amount){
    $.post(base_url+action,{products:id,amount:amount},function(result){
        $.get(base_url+"load_order",function(result){
            $("#order-modal").html(result);
        });
    },"json");
    $("#order-modal").modal("show");
}

function remove_cart(ob,action,id){
    $.post(base_url+action,{products:id},function(result){
        if(result.error==false){
            $.get(base_url+"load_order",function(result){
                $("#order-modal").html(result);
            });
        }else{
            $("#order-modal").find("#error-box").addClass("hidden");
            $("#order-modal").find("#error-box-cart").removeClass("hidden");
            $("#order-modal").find("#error-box-cart span").html(result.message);
        }
    },"json");
}

function set_address(ob,city,district){
    $("#order-modal").find("#selectCity").val(city);
    $.post(base_url+"load_district_from_city",{CityID:city},function(result){
        $("#order-modal").find("#selectDistrict").html(result);
        $("#order-modal").find("#selectDistrict").val(district);
    },"html");
    $("#order-modal").find("#AddressOrder").val($(ob).parent("label").text().trim());
}

function get_district(ob){
    var city = $(ob).val();
    $.post(base_url+"load_district_from_city",{CityID:city},function(result){
        $("#order-modal").find("#selectDistrict").html(result);
        $("#order-modal").find("#selectDistrict").change();

        $("form#create-order").find("#selectDistrict").html(result);
        $("form#create-order").find("#selectDistrict").change();
    },"html");
}

function change_quantity(ob,id) {
    // $('#dathang').find('.loading-wrapper').show();
    // $('#dathang').addClass('loading-bg');
    var amount = parseInt($(ob).val());
    if(amount > 0) {
        $.post(base_url+'add_cart',{products:id,amount:amount},function(result){
            if(!result.error) {
                $("form#create-order").find(".price").html( number_format(result.data,0,'-',',')+' VNĐ');
                $("form#create-order").find("#total").val(result.data);
            }
            // $('#dathang').find('.loading-wrapper').hide();
            // $('#dathang').removeClass('loading-bg');
        },"json");        
    }
}

function number_format( number, decimals, dec_point, thousands_sep ) {
    var n = number, c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
    var d = dec_point == undefined ? "," : dec_point;
    var t = thousands_sep == undefined ? "." : thousands_sep, s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
                              
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function tab(ob,tab){
    $("#order-modal").find(".tab").addClass("hidden");
    $("#order-modal").find("#tab"+tab).removeClass("hidden");
}

function send_order(ob,form,action){
    $("#order-modal").find("#error-box-cart").addClass("hidden");
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#order-modal").find("#error-box").removeClass("hidden");
            $("#order-modal").find("#error-box").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();

            $("form#create-order").find("#error-box-cart").removeClass("hidden");
            $("form#create-order").find("#error-box-cart").find(".alert span").html(result.message);
        }else{
            // $("#order-modal").load(base_url+"thanks_page");
            var url = base_url+"thanks_page";
            window.location.href=url;
        }
    },'json');
}

function change_amount_cart(ob,action,id){
    var amount = parseInt($(ob).val());
    add_to_cart(ob,action,id,amount);
}

function send_comment(ob,form,action){
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#"+form).find("#box-alert-comment").removeClass("hidden");
            $("#"+form).find("#box-alert-comment").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#"+form).find("#box-alert-comment").addClass("hidden");
            $("#"+form).find("input[type='text']").val('');
            $("#"+form).find("textarea").val('');
            $("#comment-modal").modal("hide");

            $(".notice-content").html("<p class='text-center'>Chúng tôi đã ghi nhận câu hỏi của bạn và sẽ phản hồi trong thời gian sớm nhất.</p>");
            $("#notice-modal").modal("show");
            // $.post(base_url+'load_comment',{FunelID:1},function(result){
            //    $(".comment-content").html(result);
            // });
        }
    },'json');
}

function send_callme(ob,form,action){
    $(ob).addClass("saving");
    $.post(base_url+action,$("#"+form).serializeArray(),function(result){
        $(ob).removeClass("saving");
        if(result.error==true){
            $("#"+form).find("#box-alert-callme").removeClass("hidden");
            $("#"+form).find("#box-alert-callme").find(".alert span").html(result.message);
            $("#"+form).find("input[name='"+result.focus+"']").focus();
        }else{
            $("#"+form).find("#box-alert-callme").removeClass("hidden").find('.alert').removeClass("alert-danger").addClass("alert-success").html(result.message);
            $("#"+form).find("input[type='text']").val('');
            $("#"+form).find("textarea").val('');
        }
    },'json');
}

function show_more_comment(ob){
    $(ob).hide();
    $(ob).parents('div.comment-content').find(".hidden").removeClass("hidden");
}

function click_position(ob,action,position,funel,showform){
    $.post(base_url+action,{position:position,funel:funel,title:$(ob).attr('title')},function(result){
        if(showform!=''){
            $(showform).modal('show');
        }
    },'json');
}
var widthBody = $('body').width();
var countItem = $('.guide .step-item').length;
var widthItem = $('.guide .step-item').css('width');
var fullWidth = parseInt(widthItem)*parseInt(countItem);
var ipadSize = 765;
var iphoneSize = 375;
function showDay() {
    var width = fullWidth/5;
    if(widthBody <= ipadSize) {
        width = widthBody/3;
    } 
    if(widthBody <= iphoneSize) {
        width = widthItem;
    }
    $('#btn-night').removeClass('clicked');
    $('#btn-day').addClass('clicked');
    $('.guide .step-item:last-child').show('400');
    $('.step-number .num').css({'background':'linear-gradient(to right, #ab8439 , #e4bf57, #ab8439 , #e4bf57)'});
    $('.guide .step-item').animate({'width':width-1,"speed":400});           
}

function showNight() {
    var width = widthBody/4;
    if(widthBody <= ipadSize) {
        width = widthBody/2;
    }
    if(widthBody <= iphoneSize) {
        width = widthItem;
    }
    $('#btn-night').addClass('clicked');
    $('#btn-day').removeClass('clicked');
    $('.guide .step-item:last-child').hide();
    $('.step-number .num').css({'background':'#211f1f'});
    $('.guide .step-item').animate({'width':width,"speed":400});           
}

var heightButton;
var heightLastItem;
var heightMenu;
$(window).load(function(){
    heightButton   = $('#quytrinh').offset().top;
    heightLastItem = $('.step-item:last-child').offset().top;    
    heightMenu     = $('header').offset().top;
});


$(window).bind('scroll', function () {
    var windowCurrent = $(window).scrollTop();

    var khuyenmai  = $('#sieukhuyenmai').offset().top;
    var trainghiem = $('#trainghiem').offset().top;
    var dotpha     = $('#dotpha').offset().top;
    var quytrinh   = $('#quytrinh').offset().top;
    var danhhieu   = $('#danhhieu').offset().top;
    var binhluan   = $('#binhluan').offset().top;

    if( heightButton <= windowCurrent && windowCurrent <= heightLastItem) {
        $('#btn-day').css({
            'position':'fixed',
            'top'     : '50px',
            'left'    : '10%'
        });
        $('#btn-night').css({
            'position':'fixed',
            'top'     : '50px',
            'right'   : '10%'
        });
    } else {
        $('#btn-day,#btn-night').css({
            'position':'static',
        });
    }   

    if(heightMenu <= windowCurrent) {
        $('header').css({
            'position':'fixed'
        });
    } else {
        $('header').css({
            'position':'relative'
        });
    }

    if(khuyenmai >= windowCurrent) { 
        $('a.current').removeClass('current');
        $('#nav-sieukhuyenmai').addClass('current'); 
    }
    if(trainghiem-200 <= windowCurrent && windowCurrent < dotpha) { 
        $('a.current').removeClass('current');
        $('#nav-trainghiem').addClass('current'); 
    }
    if(dotpha-200 <= windowCurrent && windowCurrent < quytrinh) { 
        $('a.current').removeClass('current');
        $('#nav-dotpha').addClass('current'); 
    }
    if(quytrinh-200 <= windowCurrent && windowCurrent < danhhieu) { 
        $('a.current').removeClass('current');
        $('#nav-quytrinh').addClass('current'); 
    }
    if(danhhieu-200 <= windowCurrent && windowCurrent < binhluan) { 
        $('a.current').removeClass('current');
        $('#nav-danhhieu').addClass('current'); 
    }
    if(binhluan-200 <= windowCurrent) { 
        $('a.current').removeClass('current');
        $('#nav-binhluan').addClass('current'); 
    }

});


$(document).ready(function(){
  $('.slick').slick({
    dots          : true,
    infinite      : false,
    speed         : 300,
    autoplay      : true,
    autoplaySpeed : 1000,
    slidesToShow  : 3,
    slidesToScroll: 1,
    responsive    : [
        {
          breakpoint: 1024,
          settings  : {
            slidesToShow  : 2,
            slidesToScroll: 2,
            infinite      : true,
            dots          : true
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow  : 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow  : 1,
            slidesToScroll: 1,
            dots          : false,
          }
        }
    ]
  });

  $('.accordion').collapse();
});


/*========================================
=            detail Promotion            =
========================================*/
var detailPromotion = $('#chitiet-khuyenmai');
$(document).ready(function() {
    $(detailPromotion).hide();
});
function openContentPromo() {
    console.log('red');    
    $(detailPromotion).show('slow');
    var waitShow = setTimeout(function() {
        scrollto(this,'chitiet-khuyenmai');
        clearTimeout(waitShow);
    },1000);
    
}
function closeContentPromo() {
    $(detailPromotion).hide('slow');
    var waitShow = setTimeout(function() {
        scrollto(this,'sieukhuyenmai');
        clearTimeout(waitShow);
    },1000);
}


/*=====  End of detail Promotion  ======*/
