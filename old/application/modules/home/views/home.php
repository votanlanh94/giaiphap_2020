  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Serum Sắc Ngọc Khang | Dưỡng trắng diệu kỳ</title>
    
    <!-- Bootstrap-->
    <!-- Latest compiled and minified CSS -->
    <link rel="icon" type="image/png" sizes="16x16" href="public/site/clickfunel/images/icon-120x120.png">
    <link rel="icon" type="image/png" sizes="32x32" href="public/site/clickfunel/images/icon-120x120.png">

    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick-theme.css"/>
    <link href="public/site/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/site/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="public/site/clickfunel/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/site/clickfunel/css/timeline.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;amp;subset=vietnamese" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Serum Sắc Ngọc Khang - dưỡng trắng diệu kỳ" />
    <meta property="og:description" content="Serum Sắc Ngọc Khang - hiệu quả gấp 10 lần so với kem trắng da thông thường." />
    <meta property="og:image" content="<?php echo base_url().'/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-1.jpg'; ?>" />

    <!-- Google Tag Manager -->
    <!-- End Google Tag Manager -->
  </head>
  <body class="">
    <?php $homepage = "http://sacngockhang.com"; ?>
    <nav class="top-menu navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=$homepage?>">
            <img src="public/site/clickfunel/images/logo.svg" style="max-width:150px;margin:7px auto 10px auto" class="img-responsive" />
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?=$homepage?>">VỀ TRANG CHỦ</a></li>
            <?php 
              if(isset($menu) && count($menu)>0) {
                foreach($menu as $key=>$item) {
                  if($key>0) {
                    $url = $homepage.'/'.$item->Alias;
                    echo "<li><a href='$url' target='_blank'>$item->Title</a></li>";
                  }
                }

              }
            ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container-fluid" id="slider">
      <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
      <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <a href="#binhluan">
              <img class="first-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-4.jpg" alt="First slide">              
            </a>
            <div class="container">
            </div>
          </div>
          <div class="item">
            <a href="#sieukhuyenmai">
              <img class="second-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-5.jpg" alt="Second slide">
            </a>
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
          <div class="item">
            <a href="#dotpha">
              <img class="third-slide" src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-6.jpg" alt="Third slide">
            </a>
            <div class="container">
              <div class="carousel-caption">
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div><!-- /.carousel -->
    </div>

    <header class="header">
      <!-- End top-header-->
      <div class="main-head">
        <div class="container">
          <div class="row">
            <div class="col-xs-5 col-sm-4 col-md-2 text-center bg-logo">
              <a href="/">
                <img src="public/site/images/logo-sacngockhang.png" style="max-width:150px;margin:10px auto" class="img-responsive" />
              </a>
            </div>
            <div class="col-xs-6 col-sm-8 col-md-10">
              <label for="openmenu" class="hidden-md hidden-lg"><span class="ion-android-menu"></span></label>
              <input type="checkbox" id="openmenu" class="hidden">
              <ul id="main-menu">                
                <li><a onclick="scrollto(this,'sieukhuyenmai')" id="nav-sieukhuyenmai" class="current">Ưu đãi <span>sốc</span></a></li>
                <li><a onclick="scrollto(this,'trainghiem')" id="nav-trainghiem">Người dùng <span>đánh giá</span></a></li>
                <li><a onclick="scrollto(this,'congthucmoi')" id="nav-congthucmoi">Giải <span>pháp</span></a></li>
                <li><a onclick="scrollto(this,'quytrinh')" id="nav-quytrinh">Bí Quyết <span>dưỡng da</span></a></li>
                <li><a onclick="scrollto(this,'danhhieu')" id="nav-danhhieu">Thương hiệu <span>danh giá</span></a></li>
                <li><a onclick="scrollto(this,'binhluan')" id="nav-binhluan">Câu hỏi <span>thường gặp</span></a></li>
                <li>
                  <button onclick="add_to_cart(this,'add_cart',<?=$product_id_of_page?>,1)" class="btn btn-danger btn-menu">đặt ngay</button>
                  <button class="btn btn-secondary btn-menu btn-call-sm"><i class="glyphicon glyphicon-earphone" onclick="location.href='tel:19006033';"></i> 19006033</button>
                </li>
                <li><button class="btn btn-secondary btn-menu btn-call-lg" onclick="location.href='tel:19006033';"><i class="glyphicon glyphicon-earphone"></i> 19006033</button></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- End main-head-->
    </header>

    <div class="content-box content-style-4" id="sieukhuyenmai">
      <div class="container-fluid">
        <div class="item row promotion-wrapper">
          <div class="col-xs-12 col-sm-12 col-md-4">
            <img src="public/site/images/serum-trang-da-sac-ngoc-khang-moi.png" alt="Serum trắng da sắc ngọc khang" class="img img-fluid" class="img-product">
            <p class="text-center deadline">
              Thời gian áp dụng:
              <span>đến hết ngày 30/04/2018</span>
              <button class="btn btn-view-promotion" onclick="openContentPromo()">Xem Chi Tiết</button>
            </p>                              
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8"> 
            <div class="promoDetail">                
              <h3><b>CHƯƠNG TRÌNH ƯU ĐÃI</b></h3>
              <h1><b>DUY NHẤT VÀ LỚN NHẤT</b></h1>
              <p class="price">Chỉ 
                <b class="special-price">
                  <?php 
                    echo count($product) > 0 ? number_format($product->Price) : '000,000';                    
                  ?>
                </b>
                 / chai 20ml</p>
              <p>
              <p class="text-center">
                <button type="button" onclick="add_to_cart(this,'add_cart',<?=$product_id_of_page?>,1)" class="btn-lg btn btn-primary btn-addCart">Đặt mua sản phẩm này</button>                  
              </p>
              <div class="promotion">
                <div class="promo-head">
                  <div class="promo-image">
                    <img src="/public/site/clickfunel/images/serum-mua-1-tang-2.png" alt="Serum sắc ngọc khang - mua 1 tặng 2">
                  </div>
                  <div class="promo-image">
                    <img src="/public/site/clickfunel/images/serum-mua-6-tang-1.png" alt="Serum sắc ngọc khang - mua 6 tặng 1">
                  </div>
                </div>              
                              
              </div> 
            </div>             
          </div>
        </div>
      </div>
    </div>
    
     <div class="content" id="chitiet-khuyenmai">
      <div class="container">
        <div class="row">

          <div class="col-xs-12 col-md-12 noidungkhuyenmai">
            <p>Từ ngày 20/03/2018 đến 30/04/2018, khách hàng khi mua 1 hộp Serum Sắc Ngọc Khang 20ml được tặng ngay 1 bộ sản phẩm gồm: Sữa rửa mặt Sắc Ngọc Khang 50g và Nước hoa hồng Sắc Ngọc Khang 145ml trị giá 102.000đ.</p>
            <p>Mua tích lũy đủ 6 hộp Serum Sắc ngọc Khang 20ml sẽ được tặng ngay 1 hộp Serum Sắc Ngọc Khang 20ml. </p>
            <h3><b>Cách thức tham gia và nhận quà:</b></h3>
            <ul>
              <li>
                <p>Bộ sản phẩm sữa rửa mặt và nước hoa hồng: nhận ngay khi mua hàng.</p>
              </li>
              <li>
                <p>Serum Sắc Ngọc Khang 20ml: Khi mua tích lũy đủ 6 hộp Serum Sắc Ngọc Khang 20ml, khách hàng cào 6 mã an ninh và nhắn tin 6 mã này về tổng đài 8077 trên cùng 1 số điện thoại duy nhất của khách hàng. Công ty sẽ xác nhận và gửi quà tặng: 1 hộp Serum Sắc Ngọc Khang 20ml đến địa chỉ quý khách đã đăng ký.</p>
              </li>
            </ul>
            <h3><b>Thời gian nhận quà: từ ngày 20/03/2018 đến 30/4/2018</b></h3>
            <hr>
            <p><i>Chi tiết liên hệ:</i></p>
            <ul>
              <li>
                <p>Công ty CP Dược Phẩm Hoa Thiên Phú - Số 10 Nguyễn Cửu Đàm, Phường Tân Sơn Nhì, Q.Tân Phú,  TPHCM.</p>
              </li>
              <li>Hotline: 1900 6033 (từ 08:00 giờ đến 17:00 giờ trong khoảng thời gian nhận quà từ 20/03/2018 đến 30/04/2018, trừ CN và ngày lễ).</li>
            </ul>
          </div>

        </div>
        <div class="text-center dong-chitiet">
          <button class="btn-dong-chitiet" onclick="closeContentPromo();"><i class="glyphicon glyphicon-menu-up"></i></button>          
        </div>
      </div>
    </div>

    <div id="trainghiem" class="container-fluid content VTL-content content-box">
      <div class="content-style-1">
        
        <div class="content-box-heading">
          <h3 class="content-box-title">                 
            <b>Người dùng đánh giá về</b><span>SERUM TRẮNG DA SẮC NGỌC KHANG</span>
          </h3>              
        </div>        

        <div class="container">
          <div class="col-sm-12">
            <div class="tab" role="tabpanel">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#danhgia" data-toggle="tab">Đánh giá</a></li>
                <!-- <li><a href="#messages2" data-toggle="tab">Cảm nhận</a></li> -->
                <!-- <li><a href="#profile2" data-toggle="tab"></a></li> -->
                <li><a href="#media" data-toggle="tab">Video</a></li>
              </ul>

              <div class="tab-content">               

                <div class="tab-pane active" id="danhgia">
                  <div class="row">
                    <div class="col-xs-12">                        
                      <div class="slick">

                        <div class="slick-item">
                          <div class="thumbnail">
                            <img src="/public/site/images/hoa-hau-mai-phuong-thuy-danh-gia-serum-sac-ngoc-khang-(2).jpg" alt="Hoa hậu Mai Phương Thúy đánh giá serum Sắc Ngọc Khang" class="img img-responsive img-circle">
                            <div class="caption">
                              <p class="position">Hoa Hậu</p>
                              <h3>Mai Phương Thúy</h3>
                              <p>Thúy sử dụng sản phẩm này để dưỡng da hàng ngày, serum thấm nhanh, giúp da trắng đều màu mà không cần phải trang điểm quá nhiều mỗi khi ra ngoài.</p>                            
                            </div>
                          </div>
                        </div>

                        <div class="slick-item">
                          <div class="thumbnail">
                            <img src="/public/site/images/mc-tuong-van-danh-gia-serum-sacngockhang.jpg" class="img img-responsive img-circle" alt="MC Tường Vân đánh giá về serum Sắc Ngọc Khang">
                            <div class="caption">
                              <p class="position">MC đài truyền hình TP.HCM</p>
                              <h3>Tường Vân</h3>
                              <p>Do da Vân rất nhạy cảm nên trước khi dùng Serum, Vân lấy một ít thoa lên nhẹ lên mu bàn tay, cảm nhận dưỡng chất thấm nhanh vào da, da mịn mà không hề gây kích ứng. Sau đó, Tường Vân mạnh dạn sử dụng và cảm nhận được làn da tươi sáng, rạng rỡ hơn sau vài ngày.   </p>                            
                            </div>
                          </div>
                        </div>

                        <div class="slick-item">
                          <div class="thumbnail">
                            <img src="/public/site/images/btv-van-nguyen-danh-gia-serum-sacngockhang.jpg" class="img img-responsive img-circle" alt="biên tập viên Vân Nguyễn đánh giá về serum sắc ngọc khang">
                            <div class="caption">
                              <p class="position">Biên tập viên</p>
                              <h3>Vân Nguyễn</h3>
                              <p>Chỉ mới sử dụng 2 lần nhưng ấn tượng ban đầu của mình về sản phẩm là khá tốt. Mình dùng serum dưỡng ban đêm cùng các sản phẩm khác của Sắc Ngọc Khang và sáng ngủ dậy thì thấy da mịn hơn hẳn và không bị đổ dầu. Giờ thì Sắc Ngọc Khang có trọn bộ dưỡng da rồi nên không cần phải dùng serum của thương hiệu khác nữa.</p>
                            </div>
                          </div>
                        </div>                          

                        <div class="slick-item">
                          <div class="thumbnail">
                            <img src="/public/site/images/nguoi-tieu-dung-ngoc-nguyen.jpg" alt="người tiêu dùng Nguyễn Phúc Bảo Ngọc đánh giá serum sắc ngọc khang" class="img img-responsive img-circle">
                            <div class="caption">
                              <p class="position">Nhân viên văn phòng</p>
                              <h3>Nguyễn Phúc Bảo Ngọc</h3>
                              <p>Ngọc xài Serum vào buổi sáng và buổi tối trước khi đi ngủ. Vì Ngọc thường xuyên làm việc trong môi trường văn phòng, ngồi máy lạnh nhiều nên da thường xuyên bị khô ráp. Từ khi sử dụng Serum Sắc Ngọc Khang, Ngọc thấy da rất mềm, không hề khô ráp và có mùi hương dễ chịu. Đặc biệt sau khi ngủ dậy, Ngọc cảm nhận da mềm mịn và tươi sáng hẳn.</p>                           
                            </div>
                          </div>
                        </div>

                      </div>                      
                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="media">
                  <section class="testimonial">
                    <div class="row">                          
                      <div class="col-md-12">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="carousel slide media-carousel" id="abc">
                                  <div class="carousel-inner">

                                    <!-- /.slide 1 -->
                                    <div class="item  active">
                                      <div class="col-md-6 col-sm-12 col-xs-12">

                                        <div class="panel panel-default">
                                          <div class="panel-body">
                                            <div class="embed-responsive embed-responsive-16by9">
                                              <iframe width="100%" height="315" src="https://www.youtube.com/embed/pOlHWy9wlNg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>

                                          </div>

                                          <div class="panel-footer">
                                            <div class="panel-footer-txt">
                                              <p></p>
                                              <div style="float:right; margin-top: -25px;">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>


                                      </div>
                                      <div class="col-md-6 col-sm-12 col-xs-12">

                                        <div class="panel panel-default">
                                          <div class="panel-body">
                                            <div class="embed-responsive embed-responsive-16by9">
                                              <iframe width="100%" height="315" src="https://www.youtube.com/embed/7FP0R66VkZU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>

                                          </div>

                                          <div class="panel-footer">
                                            <div class="panel-footer-txt">
                                              <p></p>
                                              <div style="float:right; margin-top: -25px;">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                      </div>

                                    </div>


                                    <!-- /.slide 2 -->
                                    <div class="item">

                                      <div class="col-md-6 col-sm-12 col-xs-12">

                                        <div class="panel panel-default">
                                          <div class="panel-body">
                                            <div class="embed-responsive embed-responsive-16by9">
                                              <iframe width="100%" height="315" src="https://www.youtube.com/embed/6gaH8vfYYXo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>
                                          </div>

                                          <div class="panel-footer">
                                            <div class="panel-footer-txt">
                                              <p></p>
                                              <div style="float:right; margin-top: -25px;">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6 col-sm-12 col-xs-12">

                                        <div class="panel panel-default">
                                          <div class="panel-body">
                                            <div class="embed-responsive embed-responsive-16by9">
                                              <iframe width="100%" height="315" src="https://www.youtube.com/embed/r5HHapANIyo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                            </div>

                                          </div>

                                          <div class="panel-footer">
                                            <div class="panel-footer-txt">
                                              <p></p>
                                              <div style="float:right; margin-top: -25px;">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <a data-slide="prev" href="#abc" class="leftar carousel-control"><img src="/public/site/clickfunel/images/arrowleft.png" class="img-responsive" alt="control"></a>
                                  <a data-slide="next" href="#abc" class="rightar carousel-control"><img src="/public/site/clickfunel/images/arrowright.png" alt="right arrow" class="img-responsive"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- /.col-12  end -->
                    </div>
                    <!-- /. row end -->
                  </section>
                </div>

                <div class="tab-pane" id="messages2">
                  <p>3. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, commodi delectus accusamus optio quam odit illum laudantium dolorum nostrum recusandae corporis quo. Veniam, excepturi illo eveniet quas aperiam provident inventore.</p>
                </div>

              </div>
            </div>
          </div>
        </div>

      </div>
    </div>

    <div id="dotpha" class="content-box content-style-2">
      <div class="container">
        <div class="content-box-heading">
          <h3 class="content-box-title">
            <b>CÔNG THỨC ĐỘT PHÁ</b> <span>VỚI NGUỒN NGUYÊN LIỆU NHẬP KHẨU TỪ NHẬT BẢN & CHÂU ÂU</span>
          </h3>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 congthucchitiet">
            <div class="row">
              <!-- Video -->
              <div class="col-xs-12 col-sm-12 col-md-6 video">
                <div class="col-xs-12">
                  <iframe width="100%" src="https://www.youtube.com/embed/VpP_gu3UqNQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                  <p>
                    <strong>Serum trắng da Sắc Ngọc Khang</strong> được sản xuất tại Nhà máy Hoa Thiên Phú Bình Dương - tọa lạc tại khu công nghiệp <strong>Singapore Ascendas Protrade</strong> và được xây dựng theo mô hình hiện đại của thế giới.
                  </p>
                  <p>
                    <strong>Nhà máy Hoa Thiên Phú Bình Dương</strong> có diện tích lên đến 12.000m2, cùng hệ thống máy móc, trang thiết bị hiện đại, dây chuyền kiểm soát chất lượng được hoạt động theo quy trình khép kín. Nhờ đó, chất lượng sản phẩm của <strong>thương hiệu Sắc Ngọc Khang</strong> đáp ứng đầy đủ các tiêu chuẩn khắt khe của Bộ Y Tế.
                  </p>
                  <p class="text-center">
                    <button type="button" onclick="add_to_cart(this,'add_cart',<?=$product_id_of_page?>,1)" class="btn-lg btn btn-primary btn-addCart">Mua ngay sản phẩm <br><span style="font-size: 15px;">Nhận quà liền tay</span></button>
                  </p>
                </div>
              </div>
              <!-- end video -->
              <!-- cong thuc -->
              <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                      <img src="public/site/images/cong-thuc-dot-pha-serum-trang-da-sac-ngoc-khang.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8">
                      <div class="item"> 
                        <h6>Hệ dưỡng trắng vượt trội</h6>
                        <p>[Hoạt chất VC-IP<sup>TM</sup>]* ức chế melanin, kích hoạt sắc da trắng khỏe, đều màu, chống oxy hóa và phục hồi tổn thương do môi trường.</p>
                        <p>(*)Hoạt chất làm trắng da Vitamin C thế hệ mới nhập khẩu từ Nhật Bản.</p>
                      </div>
                    </div>
                  </div> 
                  <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                      <img src="public/site/images/Leaflet_SerumDTD-SNK_thaychai-13.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8">
                      <div class="item"> 
                        <h6>Lớp bảo vệ toàn diện</h6>
                        <p>[Dầu Jojoba] kháng khuẩn, kháng viêm, khôi phục & tái tạo kết cấu da, giúp da mịn màng căng mọng.</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-4">
                      <img src="public/site/images/Leaflet_SerumDTD-SNK_thaychai-14.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid">
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-8">
                      <div class="item"> 
                        <br>                          
                        <p>[Squalance] dưỡng ẩm, cân bằng độ ẩm và dầu, ngăn chặn tác hại của tia UV</p>                      
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-xs-12 col-sm-5 col-md-4">
                    <img src="public/site/images/Leaflet_SerumDTD-SNK_thaychai-15.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid" width="80%" style="margin: 0px auto;">
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-8">
                    <div class="item"> 
                      <h6>Độ an toàn tối ưu</h6>
                      <p>Không kích ứng, không chất bảo quản, không nhờn rít.</p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-sm-5 col-md-4">
                    <img src="public/site/images/6-gia-tri-sac-ngoc-khang-mang-lai-2.png" alt="Sắc Ngọc Khang Vi Tảo Lục" class="img img-fluid" width="80%" style="margin: 0px auto;">
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-8">
                    <div class="item"> 
                      <h6>Thích hợp mọi làn da</h6>
                      <p>Dưỡng chất trong Serum phù hợp với mọi làn da, kể cả làn da nhạy cảm nhất.</p>
                    </div>
                  </div>
                </div>        
              </div>
              <!-- end cong thuc -->
            </div>  
          </div>              
        </div>
        <!-- end row  -->
      </div>
    </div>
     

    <div class="content-box" style="padding: 0px;">
      <div class="container-fluid" style="padding-left: 0px; padding-right:0px">
        <img src="/public/site/clickfunel/images/banner/serum-trang-da-sacngockhang-slider-1.jpg" alt="" class="img img-fluid w-100">
      </div>
    </div>

    <div id="quytrinh" class="content-box">
      <div class="container-fluid">
        <div class="content-box-heading">
          <h3 class="content-box-title">                   
            <b>Bí quyết dưỡng da</b>
            <span>QUY TRÌNH CHĂM SÓC DA NGÀY & ĐÊM <span class="display-block">SẮC NGỌC KHANG</span></span>
            <button id="btn-day" class="btn clicked" onclick="showDay()">Ban Ngày</button>
            <button id="btn-night" class="btn" onclick="showNight()">Ban Đêm</button>
          </h3>
        </div>
        <div class="row">
          <div class="guide">
            <div class="step-item">
              <div class="step-content-wrapper">
                <div class="step-number">
                  <div class="num">Bước 1</div>                      
                </div>                    
                <div class="step-content">
                  <img src="public/site/images/sua-rua-mat-sac-ngoc-khang.png" class="img-responsive" alt="">
                  <h3>Sữa rửa mặt</h3>
                  <p>Làm sạch dịu nhẹ cho da, thích hợp với mọi làn da, kể cả da nhạy cảm.</p>                      
                </div>
              </div>
            </div>
            <div class="step-item">
              <div class="step-content-wrapper">
                <div class="step-number">
                  <div class="num">Bước 2</div>                      
                </div>                    
                <div class="step-content">
                  <img src="public/site/images/nuoc-hoa-hong-sac-ngoc-khang.png" class="img-responsive" alt="Nước hoa hồng sắc ngọc khang">
                  <h3>Nước hoa hồng</h3>
                  <p>Là bước  làm sạch sâu, se khít lỗ chân lông, cân bằng độ ẩm cho da, giúp các bước chăm sóc da tiếp theo được hấp thụ tốt nhất để phát huy.</p>                      
                </div>
              </div>
            </div>
            <div class="step-item">
              <div class="step-content-wrapper">
                <div class="step-number">
                  <div class="num">Bước 3</div>                      
                </div>                    
                <div class="step-content">
                  <img src="public/site/images/serum-trang-da-sac-ngoc-khang.png" class="img-responsive" alt="">
                  <h3>Serum</h3>
                  <p>Dưỡng chất dẫn sâu, thấm nhanh, phục hồi hoàn hảo cho làn da sáng khỏe, đều màu và rạng rỡ đến diệu kỳ.</p>                   
                </div>
              </div>
            </div>
            <div class="step-item">
              <div class="step-content-wrapper">
                <div class="step-number">
                  <div class="num">Bước 4</div>                      
                </div>                    
                <div class="step-content">
                  <img src="public/site/images/snk-kem-duong-trang.png" class="img-responsive" alt="">
                  <h3>Kem dưỡng trắng</h3>
                  <p>Tinh chất cô đặc dưỡng sáng đều màu, khóa ẩm, tái tạo da.</p>                      
                </div>
              </div>
            </div>
            <div class="step-item">
              <div class="step-content-wrapper">
                <div class="step-number step-5">
                  <div class="num">Bước 5</div>                      
                </div>                    
                <div class="step-content">
                  <img src="public/site/images/kem-chong-nang-sac-ngoc-khang.png" class="img-responsive" alt="Kem Chống Nắng Sắc Ngọc Khang">
                  <h3>Kem chống nắng</h3>
                  <p>Bảo vệ da khỏi ánh nắng mặt trời và các tác nhân gây hại.</p>                      
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="danhhieu" class="content-box content-text-2">
        <div class="container">
          <div class="content-box-heading">
            <h3 class="content-box-title">                   
              <b>Thương hiệu danh giá</b>
            </h3>
          </div>
          <div class="row">
            <div class="award-wrapper">
              <div class="award-item">
                <img src="public/site/images/nha-may-chuan-quoc-te-serum-hoa-thien-phu.png" class="img-responsive" alt="">
                  <h3>Nhà Máy Sản Xuất</h3>
                  <p>Sản phẩm được sản xuất trên dây chuyền đạt chuẩn quốc tế CGMP-ASEAN do Bộ Y Tế cấp phép</p>   
              </div>
              <div class="award-item">
                <img src="public/site/images/top-my-pham-hang-dau-viet-nam-sacngockhang.png" class="img-responsive" alt="">
                  <h3>Thương Hiệu</h3>
                  <p>Top thương hiệu Dược Mỹ Phẩm Việt Nam</p>   
              </div>
              <div class="award-item">
                <img src="public/site/images/thanh-tuu-sac-ngoc-khang.png" class="img-responsive" alt="">
                  <h3>Danh Hiệu</h3>
                  <p>Thương hiệu giành được nhiều danh hiệu, giải thưởng uy tín</p>   
              </div>
              <div class="award-item">
                <img src="public/site/images/HDSD_SerumDTD-SNK_09032018_FA-03.png" class="img-responsive" alt="">
                  <h3>Tin dùng</h3>
                  <p>Thương hiệu được tin dùng bởi hoa hậu Việt Nam</p>   
              </div>
              <div class="award-item">
                <img src="public/site/images/chuyen-gia-tu-van-sac-ngoc-khang.png" class="img-responsive" alt="">
                  <h3>Đội ngũ chuyên gia</h3>
                  <p>Thương hiệu được cố vấn bởi đội ngũ chuyên gia, dược sỹ nhiều kinh nghiệm</p>   
              </div>
            </div>
          </div>
        </div>
    </div>

    <div id="binhluan" class="content-box content-style-8">
      <div class="container">
        <div class="content-box-heading">
          <h3 class="content-box-title">                   
            <b>Câu hỏi thường gặp về</b><span>Serum Sắc Ngọc Khang</span>
          </h3>
        </div>
        <div class="row">
          <div class="col-xs-12">               
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                  <div class="row">
                    <div class="col-xs-12 1">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-1">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              1. Serum là gì?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-1">
                          <div class="panel-body">
                            Serum là sản phẩm chăm sóc da chuyên sâu. Các dưỡng chất trong Serum nhiều gấp 10 lần các loại kem dưỡng, với khả năng dẫn sâu, thấm nhanh vào 3 lớp biểu bì, thân bì và hạ bì để điều trị và nuôi dưỡng da từ gốc, mang lại tác dụng nhanh chóng và hiệu quả hơn.
                          </div>
                        </div>
                      </div>  
                    </div>

                    <div class="col-xs-12 2">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-2">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                              2. Loại Serum được sử dụng phổ biến nhất là gì?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-2">
                          <div class="panel-body">
                            Serum có nhiều loại để khắc phục từng nhược điểm khác nhau của da, và loại serum được sử dụng phổ biến nhất là serum chứa nhiều Vitamin C giúp làm sáng da, kích thích sản xuất collagen, cải thiện cấu trúc da, phục hồi làn da rạng rỡ và chống lại nếp nhăn.
                          </div>
                        </div>
                      </div>                 
                    </div>

                    <div class="col-xs-12 3">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-3">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                              3. Độ tuổi nào sử dụng Serum dưỡng da?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-3">
                          <div class="panel-body">
                            Serum dưỡng da có thể được dùng ở bất kỳ độ tuổi nào, tùy theo từng nhu cầu cần điều trị của da như: thâm sạm, lão hóa, mụn…
                          </div>
                        </div>
                      </div> 
                    </div>

                    <div class="col-xs-12 4">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-4">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                              4. Serum Sắc Ngọc Khang có tác dụng như thế nào?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-4">
                          <div class="panel-body">
                            Serum Sắc Ngọc Khang có tác dụng dưỡng da trắng khỏe, tươi sáng, đều màu; làm mờ những vùng da thâm sạm, xỉn màu, đồng thời, tăng độ săn chắc và đàn hồi cho da.
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-xs-12 5">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-5">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                              5. Serum Sắc Ngọc Khang có những thành phần gì?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-5">
                          <div class="panel-body">
                            Serum Sắc Ngọc Khang có công thức đột phá với nguồn nguyên liệu nhập khẩu từ Nhật Bản và châu Âu:
                            <ul>
                              <li>
                                <p>[Hoạt chất VC-IPTM]* ức chế melanin, kích hoạt sắc da trắng khỏe, đều màu, chống oxy hóa và phục hồi tổn thương do môi trường.</p>
                              </li>
                              <li>
                                <p>[Squalance] dưỡng ẩm, cân bằng độ ẩm và dầu, ngăn chặn tác hại của tia UV</p>
                              </li>
                              <li>
                                <p>[Dầu Jojoba] kháng khuẩn, kháng viêm, khôi phục & tái tạo kết cấu da, giúp da mịn màng căng mọng.</p>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>                        
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                  <div class="row">
                    <div class="col-xs-12 6">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-6">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                              6. Điểm khác biệt của Serum dưỡng trắng da Sắc Ngọc Khang?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-6">
                          <div class="panel-body">
                            <ul>
                              <li>
                                <p>Sản phẩm của công ty Cổ phần Dược phẩm Hoa Thiên Phú</p>
                              </li>
                              <li>
                                <p>Top thương hiệu Dược Mỹ Phẩm hàng đầu Việt Nam.</p>
                              </li>
                              <li>
                                <p>Được cố vấn bởi đội ngũ chuyên gia, dược sỹ nhiều kinh nghiệm.</p>
                              </li>
                              <li>
                                <p>
                                  Sản xuất trên dây chuyền đạt chuẩn quốc tế CGMP-ASEAN.
                                </p>
                              </li>
                              <li>
                                <p>Thương hiệu dành được nhiều danh hiệu, giải thưởng uy tín.</p>
                              </li>
                              <li>
                                <p>Sản phẩm Serum Sắc Ngọc Khang thích hợp với mọi làn da.</p>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>   
                    </div>

                    <div class="col-xs-12 7">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-7">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                              7. Serum Sắc Ngọc Khang có đảm bảo độ an toàn không?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-7">
                          <div class="panel-body">
                            <p>Serum Sắc Ngọc Khang đảm bảo độ an toàn tối ưu:</p>
                            <ul>
                              <li>
                                <p>Không gây kích ứng.</p>
                              </li>
                              <li>
                                <p>
                                  Không gây kích ứng.
                                </p>
                              </li>
                              <li>
                                <p>
                                  Không chứa chất bảo quản.
                                </p>
                              </li>
                              <li>
                                <p>
                                  Không nhờn rít.      
                                </p>
                              </li>
                              <li>
                                <p>
                                  Phù hợp với mọi loại da, kể cả da nhạy cảm.
                                </p>
                              </li>
                            </ul>                                                        
                          </div>
                        </div>
                      </div>                     
                    </div>

                    <div class="col-xs-12 8">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-8">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                              8. Cách sử dụng Serum Sắc Ngọc Khang như thế nào?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-8">
                          <div class="panel-body">
                            <ul>
                              <li>
                                <p>
                                  Làm sạch da, lấy lượng sản phẩm vừa đủ, thoa đều lên mặt và massage nhẹ nhàng.
                                </p>
                              </li>
                              <li>
                                <p>Thoa tinh chất mỗi sáng và tối trước khi sử dụng kem dưỡng da Sắc Ngọc Khang.</p>
                              </li>
                              <li>
                                <p>Ban ngày nên kết hợp sử dụng thêm kem chống nắng Sắc Ngọc Khang SPF 50 PA++++ để bảo vệ da.</p>
                              </li>
                            </ul>

                          </div>
                        </div>
                      </div>                     
                    </div>

                    <div class="col-xs-12 9">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-9">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                              9. Nên chăm sóc da thế nào khi sử dụng Serum Sắc Ngọc Khang?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-9">
                          <div class="panel-body">
                            Các chuyên gia của Sắc Ngọc Khang tư vấn cách sử dụng Serum theo quy trình sau:
                            <ul>                                  
                              <li>
                                <p>Bước 1: sử dụng Sữa rửa mặt Sắc Ngọc Khang  làm sạch dịu nhẹ cho da.
                                </p>
                              </li>
                              <li>
                                <p>Bước 2: dùng Nước hoa hồng Sắc Ngọc Khang để làm sạch sâu, se khít lỗ chân lông, cân bằng độ ẩm cho da, giúp các bước chăm sóc da tiếp theo được hấp thụ tốt nhất để phát huy hiệu quả.
                                </p>
                              </li>
                              <li>
                                <p>Bước 3: sử dụng Serum Sắc Ngọc Khang để dưỡng chất dẫn sâu, thấm nhanh, phục hồi hoàn hảo cho làn da sáng khỏe, đểu màu và rạng rỡ đến diệu kỳ.
                                </p>
                              </li>
                              <li>
                                <p>Bước 4: dùng Kem dưỡng da Sắc Ngọc Khang với tinh chất cô đặc dưỡng sáng đều màu, khóa ẩm, tái tạo da.
                                </p>
                              </li>
                              <li>
                                <p>Bước 5: ban ngày nên kết hợp thêm Kem chống nắng Sắc Ngọc Khang SPF 50 PA++++ để bảo vệ da.
                                </p>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>         
                    </div>

                    <div class="col-xs-12 10">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="number-10">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen">
                              10. Có nên dùng thêm kem chống nắng sau khi sử dụng Serum?
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-10">
                          <div class="panel-body">
                            Ban ngày, bạn nên kết hợp sử dụng thêm Kem chống nắng Sắc Ngọc Khang SPF 50 PA++++ để bảo vệ làn da khỏi tia cực tím và các tác nhân gây hại.
                          </div>
                        </div>
                      </div>    
                    </div>
                  </div>
                </div>              
                
              </div>
                               
            </div>
            <div class="text-xs-center"><a href="" data-toggle="modal" data-target="#comment-modal" data-dismiss="modal" title="Gửi bình luận của bạn" class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Gửi câu hỏi của bạn</b></a></div>
          </div>
        </div>
      </div>
    </div>     

    <div id="dathang" class="content-box">
      <!-- Loading  -->
      <div class="loading-wrapper">
        <div class="bouncing-loader">
          <div class="bouncing-loader__round"></div>
          <div class="bouncing-loader__round"></div>
          <div class="bouncing-loader__round"></div>
        </div>      
      </div>
      <!-- end loading -->
      <div class="container">
        <div class="content-box-heading">
          <h3 class="content-box-title">                   
            <b>Đặt hàng ngay</b><span>Serum Sắc Ngọc Khang</span>
          </h3>
        </div>
        <div class="row">          
            <form id="create-order">
              <div class="container">
                <div class="row">  
                <?php                
                  $cart = $this->session->userdata('cart_funel');
                  if($cart && count($cart) > 0):
                    foreach($cart as $product_id=>$product) :
                      $product = (object)$product;                             
                ?> 
                  <div class="col-xs-12 col-sm-12 col-md-3 hidden-md-down order-form-item">
                    <img src="public/site/images/serum-trang-da-sac-ngoc-khang-moi.png" alt="sản phẩm Serum trắng da Sắc Ngọc Khang" class="img-responsive" width="100%">
                  </div>           

                  <div class="col-xs-12 col-sm-6 col-md-4 order-form-item">
                    <div class="row">
                      <div class="col-xs-12">
                        <h4 class="text-center">Thông tin đơn hàng</h4>
                      </div>
                      <div class="col-xs-12 col-md-12">
                      </div>
                   
                      <div class="col-xs-12 col-md-12 product-info-cart"> 
                        <h4><?=$product->Title?></h4>
                        <div class="quantity-wrapper">         
                          <p>Giá: <?=number_format($product->Price)?> vnđ</p>
                          <p>Số lượng</p>
                          <p class="prd-quanlity">     
                            <input type="number" value="<?=$product->Amount?>" id="product-quality" name="product-quality" class="form-control form-control-sm" onchange="change_quantity(this,<?=$product_id?>)">
                            <input type="hidden" value="<?=$product->Price?>" id="product-price" name="product-price" class="form-control form-control-sm">
                          </p>                  
                        </div>

                        <div class="total-wrapper">         
                          <p>Thành tiền:</p>                        
                          <p class="price"><?=number_format($product->Price*$product->Amount)?> VNĐ</p>
                          <input type="hidden" id="total" value="<?=$product->Price*$product->Amount?>">
                        </div>

                      </div>                 

                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-12 uudai"><p>Bạn có mã ưu đãi không ?</p></div>
                          <div class="col-md-6 col-sm-12 col-xs-12" style="margin-bottom:15px;">
                              <input type="text" class="form-control" id="coupon_code" coupon="0" name="coupon_code" placeholder="Nhập mã giảm giá của bạn vào đây">
                          </div>
                          <div class="col-md-6 col-sm-12 col-xs-12">
                              <button type="button" class="btn btn-secondary" onclick="check_coupon_code(this,'check_coupon_code')">Kiểm tra mã</button>
                          </div>                  
                        </div>
 
                        <!-- /error box -->
                        <div class="row hidden" id="error-box-cart">
                          <div class="col-xs-12">
                              <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span></span></div>
                          </div>
                        </div>          
                        <div class="row hidden" id="error-box">
                            <div class="col-xs-12">
                                <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                            </div>
                        </div>
                      </div>

                    </div>
                  </div>
                  <?php endforeach;?>
                <?php else: ?>
                  <div class="col-xs-12 col-md-6">                    
                    <img src="http://via.placeholder.com/350x250" class="img-responsive" width="100%">
                  </div> 
                <?php endif;?>

                  <div class="col-xs-12 col-sm-6 col-md-5 order-form-item thongtinkhachhang">                
                    <div class="row">
                      
                      <div class="col-xs-12 form-group">
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input id="inlineRadio1" type="radio" name="Sex" value="1" class="form-check-input"><span>Anh </span>
                          </label>
                        </div>
                        <div class="form-check form-check-inline">
                          <label class="form-check-label">
                            <input id="inlineRadio2" type="radio" name="Sex" checked="checked" value="0" class="form-check-input"><span>Chị </span>
                          </label>
                        </div>
                      </div>
                      <div class="col-md-6 grid-m-6 form-group">
                        <input placeholder="Họ tên" type="text" name="Name" class="form-control">
                      </div>
                      <div class="col-md-6 grid-m-6 form-group">
                        <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
                      </div>
                      <div class="col-md-6 grid-m-6 form-group">
                        <input id="Email" name="Email" placeholder="Email (không bắt buộc)" type="email" class="form-control">
                      </div>
                      <div class="col-md-6 grid-m-6 form-group">
                        <input placeholder="Ghi chú" name="Note" type="text" class="form-control">
                      </div>                    
                      <div class="col-xs-12 address-input">
                        <div class="card">
                          <div class="card-header">
                            Địa chỉ giao hàng:
                          </div>
                          <div class="card-block">
                            <div class="row" id="tab1">
                              <div class="col-md-6 grid-m-6 form-group">
                                <select id="selectCity" name="CityID" class="form-control" onchange="get_district(this)">
                                    <?php 
                                    if(count($city)>0){
                                        foreach($city as $row){
                                            $selected = $row->ID==30 ? 'selected="selected"' : '' ;
                                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                                        }
                                    }
                                    ?>
                                </select>
                              </div>
                              <div class="col-md-6 grid-m-6 form-group">
                                <select id="selectDistrict" onchange="getfee_frontend(this,'getfee')" name="DistrictID" class="form-control">
                                  <option value="0">Chọn quận, huyện</option>
                                    <?php 
                                    if(count($district)>0){
                                        foreach($district as $row){
                                            echo "<option value='$row->ID'>$row->Title</option>";
                                        }
                                    }
                                    ?>
                                </select>
                              </div>
                              <div class="col-xs-12 form-group">
                                <input id="AddressOrder" name="AddressOrder" placeholder="Số nhà, tên đường, phường/ xã" type="text" class="form-control">
                              </div>
                            </div>                             
                            <div class="row">
                              <div class="col-xs-12 hidden" id="fee" fee="0"><div class="alert alert-warning" style="margin-top:0px;">Chi phí vận chuyển phát sinh cho khu vực bạn vừa chọn : <b>0đ</b></div></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12">
                        <div class="row">
                          <div class="col-xs-12 btn-create-order">
                            <button type="button" class="btn btn-primary buttonred animated infinite pulse" onclick="send_order(this,'create-order','create_order')">Đặt hàng   </button>
                          </div>
                        </div>                        
                        <input type="hidden" name="FunelID" value="1" />
                        </div>
                      </div>
                    </div>
                  </div>        
                </div>

              </div>
              <!-- end container -->
              
            </form>                
        </div>
        
      </div>
    </div>
  

    <div id="order-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade"></div>
    <div id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>Gửi bình luận</span></h5>
          </div>
          <div class="modal-body">
            <form id="form-comment">
            <div class="row">
              <div class="col-xs-6 form-group">
                <input placeholder="Họ tên" type="text" class="form-control" name="Name">
              </div>
              <div class="col-xs-6 form-group">
                <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
              </div>
              <div class="col-xs-12 form-group">
                <textarea placeholder="Nội dung bình luận..." rows="3" name="Comments" class="form-control"></textarea>
              </div>
            </div>
            <div class="row hidden" id="box-alert-comment" style="margin-top:10px;">
                <div class="col-xs-12">
                    <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                </div>
            </div>
                <input type="hidden" name="FunelID" value="1" />
            </form>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_comment(this,'form-comment','send_comment')">Gửi</button>
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
      
    <div id="callme-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>TƯ VẤN CHO TÔI</span></h5>
          </div>
          <div class="modal-body">
            <form id="form-callme">
            <div class="row">
              <div class="col-xs-6 form-group">
                <input placeholder="Họ tên" type="text" class="form-control" name="Name">
              </div>
              <div class="col-xs-6 form-group">
                <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
              </div>
              <div class="col-xs-12 form-group">
                <textarea placeholder="Nội dung cần chúng tôi tư vấn..." rows="3" name="Note" class="form-control"></textarea>
              </div>
            </div>
            <div class="row hidden" id="box-alert-callme" style="margin-top:10px;">
                <div class="col-xs-12">
                    <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                </div>
            </div>
            </form>
          </div>
          <div class="modal-footer text-center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_callme(this,'form-callme','send_callme')">Gửi</button>
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="notice-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
            <h5 class="modal-title"> <span>Thông báo</span></h5>
          </div>
          <div class="modal-body">
            <div class="notice-content"></div>
          </div>
          <div class="modal-footer text-center">
            <div class="logo">
              <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
            </div>
          </div>
        </div>
      </div>
    </div>

    

    <style>
      .loading-bg {
        background: #fff;
        opacity: .3;        
      }
      .loading-wrapper {
        z-index: 9;
        opacity: 1;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
            -ms-flex-pack: center;
                justify-content: center;
        -webkit-box-align: center;
            -ms-flex-align: center;
                align-items: center;
        margin: 0;
        position: absolute;
        height: 100%;
        width: 100%;
      }
      .bouncing-loader {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
      }
      .bouncing-loader__round {
        width: 20px;
        height: 20px;
        background-color: #ab8439;
        border-radius: 50%;
        -webkit-animation: bounce 0.6s infinite alternate;
                animation: bounce 0.6s infinite alternate;
      }
      .bouncing-loader__round:not(:first-child) {
        margin-left: 10px;
      }
      .bouncing-loader__round:nth-child(2) {
        -webkit-animation-delay: 0.2s;
                animation-delay: 0.2s;
      }
      .bouncing-loader__round:nth-child(3) {
        -webkit-animation-delay: 0.4s;
                animation-delay: 0.4s;
      }

      @-webkit-keyframes bounce {
        from {
          opacity: 1;
          -webkit-transform: translateY(0);
                  transform: translateY(0);
        }
        to {
          opacity: 0.5;
          -webkit-transform: translateY(-20px);
                  transform: translateY(-20px);
        }
      }

      @keyframes bounce {
        from {
          opacity: 1;
          -webkit-transform: translateY(0);
                  transform: translateY(0);
        }
        to {
          opacity: 0.5;
          -webkit-transform: translateY(-20px);
                  transform: translateY(-20px);
        }
      }
    </style>
    
    <div class="bottom-function">
      <div id="scrollUp" class="scroll-up pull-right"><i class="snk snk-angle-up snk-2x"></i></div>
    </div>
    <a id="alo-phoneIcon" data-toggle="modal" data-target="#callme-modal" class="alo-phone alo-green alo-show">
      <div class="alo-ph-circle"></div>
      <div class="alo-ph-circle-fill"></div>
      <div class="alo-ph-img-circle"><span class="glyphicon glyphicon-earphone"></span></div><span class="alo-ph-text">19006033</span>
    </a>
    
    <footer>
      <div class="footer-content content">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              <div class="fb-page" data-href="https://www.facebook.com/sacngockhang/" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/sacngockhang/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/sacngockhang/">Sắc Ngọc Khang</a></blockquote></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                <div class="row logo">
                  <div class="col-xs-12 col-sm-6 col-md-2 logo-htp-footer">
                    <img src="public/site/clickfunel/images/logo-htp.png" class="img-responsive" />
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <img src="public/site/images/logo-sacngockhang.png" alt="logo serum footer" class="img-responsive" />
                  </div>
                  <div class="col-xs-4 col-sm-6 col-md-3 logo-bct-footer">
                    <img src="public/site/clickfunel/images/sacngockhang-thong-bao-bo-cong-thuong.png" alt="sắc ngọc khang thông báo bộ công thương" class="img-responsive" />
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 info">
                    <hr>
                    <p>Địa chỉ trụ sở : số 10 Nguyễn Cửu Đàm, phường Tân Sơn Nhì, quận Tân Phú, TPHCM.</p>
                    <p>Tổng đài tư vấn / đặt hàng : <b class="text-htp">19006033</b></p>
                    <p>MST/ĐKKD/QĐTL : 0307205818</p>
                  </div>
                </div>
            </div>
          </div>
        </div>        
      </div>
    </footer>
    <!-- End footer-->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId            : '218440352049462',
          autoLogAppEvents : true,
          xfbml            : true,
          version          : 'v2.12'
        });
      };
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/vi_VN/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="fb-customerchat"
      page_id="141058836006826"
      theme_color="#fa3c4c"
      logged_in_greeting="Xin chào ! Anh/Chị cần hỗ trợ gì ạ ?"
      logged_out_greeting="Xin chào ! Anh/Chị cần hỗ trợ gì ạ ?"
    >
    </div>

    <input type="hidden" id="baseurl" value="<?php echo base_url() ?>" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript-->
    <script src="public/site/clickfunel/js/tether.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="public/site/clickfunel/js/slick.min.js"></script>
    <script src="public/site/clickfunel/js/script.js"></script>
    <script>
      $('#myCarousel').carousel({interval:5000});
    </script>
    
    </body>
</html>