<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['view_info']            = "cart/view_info";
$route['add_cart']             = "cart/add_cart";
$route['getfee']               = "cart/getfee"; 
$route['send_callme']          = "cart/send_callme";
$route['send_comment']         = "cart/send_comment";
$route['load_comment']         = "cart/load_comment"; 
$route['load_order']           = "cart/load_order";
$route['check_order']          = "cart/check_order";
$route['create_order']         = "cart/create_order";
$route['remove_products']      = "cart/remove_products";
$route['check_coupon_code']    = "cart/check_coupon_code";
$route['load_box_check_order'] = "cart/load_box_check_order";
$route['load_order_details']   = "cart/load_order_details";
$route['load_district_from_city'] = "cart/load_district_from_city";

$route['get_total'] = "cart/get_cart_total";

$route['thanks_page'] = "home/thanks_page";

$route['click_position'] = "home/click_position";

$route['uudaisacngockhang'] = "event";
$route['uudaisacngockhang/vongquaymayman'] = "event/event_luckydraw";
$route['uudaisacngockhang/vongquaymayman/(:any)'] = "event/event_luckydraw/$1";
$route['uudaisacngockhang/(:any)'] = "event/$1";

$route['default_controller'] = "home";
$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */