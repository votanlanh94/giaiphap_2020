<?php
class Report_products_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->table = 'report_products';
    }

    public function get_by_list($array_id= array(), $return_array = false) {
        if(!empty($array_id)) {
            $this->db->where_in('ID',$array_id);
        }
        $query = $this->db->get($this->table);
        if($return_array) return $query->result_array();
        return $query->result();
    }

    public function get_by_id($id = 0, $return_array = false) {
        if(!empty($id)) {
            $this->db->where_in('ID',$id);
            $query = $this->db->get($this->table);
            if($return_array) return $query->row_array();
            return $query->row();
        }
        return null;
    }
    public function get_single_products_for_popup($return_array = false){
        $this->db->select('ID,MaSP,Title,PrimaryImage,Donvi,Price,SpecialPrice,SpecialStartday,SpecialStopday,Status,Published');
        $this->db->where("Published", 1);
        $this->db->where("VariantType", 0);
        $this->db->where("TypeProducts !=", 2);
        $query = $this->db->get($this->table);
        if($return_array) return $query->result_array();
        return $query->result();
    }
}
