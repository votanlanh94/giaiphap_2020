<?php
class conf_define extends CI_Model {

    var $tablename    = 'ttp_define';
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_order_status($group,$type,$orderby='name',$sort='asc')
    {
        $this->db->select('id, code, name, del_flg');
        $this->db->where('group',$group); 
        $this->db->where('type',$type); 
        $this->db->order_by($orderby, $sort);
        $res = $this->db->get($this->tablename)->result();
        return $res;
    }
    
    function get_list_by_group($code = null){
        if($code){
            $this->db->select('*');
            $this->db->where('group',$code); 
            $this->db->order_by('name', 'asc'); 
            $result = $this->db->get($this->tablename)->result();
            if($result != null)
            return $result;
        }
        return false;
    }

}