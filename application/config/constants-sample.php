<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); 
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); 
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('ADMINPATH',		'administrator');
define('IMAGECROP',		'600x400,300x200,100x65');
define('IMAGECROPS',     '200x200');
define('NEWS_VI',		'BẢN TIN');
define('NEWS_EN',		'NEWS');
define('MORE_VI',		'TÌM HIỂU THÊM');
define('MORE_EN',		'READ MORE');
define('REGISDESCRIPTION_VI',		'Đăng ký nhận thông tin mới nhất của TTP.');
define('REGISDESCRIPTION_EN',		'Register to receive the latest information of TPP.');
define('REGISTER_VI',		'Đăng ký');
define('REGISTER_EN',		'Register');
define('EMAILADDRESS_VI',		'Địa chỉ Email');
define('EMAILADDRESS_EN',		'Email Address');
define('ADDRESS_VI',			'Địa chỉ');
define('ADDRESS_EN',			'Address');
define('POLICY_VI',				'Chính sách bảo mật');
define('POLICY_EN',				'Privacy policy');
define('TERMS_VI',				'Điều khoản sử dụng');
define('TERMS_EN',				'Terms of use');
define('SHARE_VI',				'Kết nối & chia sẻ');
define('SHARE_EN',				'Connect & share');
define('GROW_VI',				'Vẫn tiếp tục phát triển');
define('GROW_EN',				'Continues to grow');
define('VIEWALL_VI',			'Xem tất cả');
define('VIEWALL_EN',			'View all');
define('VIEWDETAILS_VI',		'Xem chi tiết');
define('VIEWDETAILS_EN',		'View details');
define('APPLY_VI',				'Ứng tuyển ngay');
define('APPLY_EN',				'Apply');
define('DEATHLINE_VI',			'Tiếp nhận hồ sơ đến hết ngày: ');
define('DEATHLINE_EN',			'Receiving records through december: ');
define('APPLYDES_VI',			'Ứng viên quan tâm đến vị trí tuyển dụng. Vui lòng nộp hổ sơ đến địa chỉ email: <a href="mailto:">tuyendung@abc.vn</a> hoặc ứng tuyển trực tiếp trên website');
define('APPLYDES_EN',			'Candidates interested in the vacancy. Please submission to the email address: <a href="mailto:tuyendung@abc.vn">tuyendung@ttp.net.vn</a> or apply directly on the website');
define('POSITION_VI',			'<span>Những vị trí đang</span><strong>Tuyển dụng</strong>');
define('POSITION_EN',			'<span>These positions are</span><strong> recruitment</strong>');
define('WHY_VI',				'<span>Vì sao chọn</span><strong>TTP</strong>');
define('WHY_EN',				'<span>why to choose </span><strong> TTP</strong>');
define('GOTO_VI',				'Ghé thăm website');
define('GOTO_EN',				'Visit website');
define('SEARCH_VI',				'Tìm kiếm');
define('SEARCH_EN',				'Search');
define('FOCAL_VI',				'TIÊU ĐIỂM');
define('FOCAL_EN',				'FOCAL');
define('INFO_VI',				'Thông tin');
define('INFO_EN',				'Information');
define('WORKTIME_VI',			'Thời gian làm việc');
define('WORKTIME_EN',			'Work time');
define('NAME_VI',				'Họ tên');
define('NAME_EN',				'Name');
define('NOIDUNG_VI',			'Nội dung');
define('NOIDUNG_EN',			'Content');
define('FEATURE_VI',			'BÀI VIẾT NỔI BẬT');
define('FEATURE_EN',			'Featured articles');
define('RELATED_VI',			'BÀI VIẾT LIÊN QUAN');
define('RELATED_EN',			'Related article');
define('FORMDES_VI',			'Vui lòng điền đầy đủ thông tin ứng tuyển, chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.');
define('FORMDES_EN',			'Please complete all information apply, we will contact you as soon as possible.');
define('ATTACH_VI',				'Đính kèm CV');
define('ATTACH_EN',				'Attach CV');
define('DOWNLOAD_VI',			'Download CV mẫu');
define('DOWNLOAD_EN',			'Download sample CV');
define('APPLYO_VI',				'Ứng tuyển');
define('APPLYO_EN',				'Apply');


define('GHNDEV', serialize(array(
    'apiUrl'        =>  '',
    'clientID'      =>  0,
    'password'      =>  '',
    'apiKey'        =>  '',
    'apiSecretKey'  =>  '',
    ))
);

define('GHNPUB', serialize(array(
    'apiUrl'        =>  '',
    'clientID'      =>  0,
    'password'      =>  '',
    'apiKey'        =>  '',
    // 'apiSecretKey'  =>  '',
    ))
);

define('SMS_USERNAME',      '');
define('SMS_PASSWORD',      '');
define('SMS_BRANDNAME',     '');
define('CDN',               'http://online.sacngockhang.com/');
/* End of file constants.php */
/* Location: ./application/config/constants.php */