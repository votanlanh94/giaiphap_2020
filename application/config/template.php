<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

// section default
$template['default']['template'] = 'default/default_template';
$template['default']['regions'] = array(
    'top',
    'menu',
    'left',
    'content',
    'right',
    'bottom'
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;


$template['admin']['template'] = 'admin/template_admin';
$template['admin']['regions'] = array(
    'topnav',
    'sitebar',
    'content',
    'footer'
);
$template['admin']['parser'] = 'parser';
$template['admin']['parser_method'] = 'parse';
$template['admin']['parse_template'] = FALSE;

$template['report']['template'] = 'admin/template_report';
$template['report']['regions'] = array(
    'header',
    'sitebar',
    'content'
);
$template['report']['parser'] = 'parser';
$template['report']['parser_method'] = 'parse';
$template['report']['parse_template'] = FALSE;

$template['transporter']['template'] = 'admin/template_transporter';
$template['transporter']['regions'] = array(
    'header',
    'sitebar',
    'content'
);
$template['transporter']['parser'] = 'parser';
$template['transporter']['parser_method'] = 'parse';
$template['transporter']['parse_template'] = FALSE;

$template['pos']['template'] = 'admin/template_pos';
$template['pos']['regions'] = array(
    'header',
    'sitebar',
    'content'
);
$template['pos']['parser'] = 'parser';
$template['pos']['parser_method'] = 'parse';
$template['pos']['parse_template'] = FALSE;

$template['site']['template'] = 'site/template_site';
$template['site']['regions'] = array(
    'MetaDescription',
	'MetaKeywords',
	'MetaExtend',
	'header',
    'content',
	'footer',
    'script'
);
$template['site']['parser'] = 'parser';
$template['site']['parser_method'] = 'parse';
$template['site']['parse_template'] = FALSE;

$template['card']['template'] = 'card/template_card';
$template['card']['regions'] = array(
    'MetaDescription',
    'MetaKeywords',
    'MetaExtend',
    'header',
    'content',
    'footer',
    'script'
);
$template['card']['parser'] = 'parser';
$template['card']['parser_method'] = 'parse';
$template['card']['parse_template'] = FALSE;