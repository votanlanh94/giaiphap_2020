<?php echo $_doctype; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?php echo $_title;?>

    <!-- Bootstrap-->
    <!-- Latest compiled and minified CSS -->
    <link rel="icon" type="image/png" sizes="16x16" href="public/site/clickfunel/images/icon-120x120.png">
    <link rel="icon" type="image/png" sizes="32x32" href="public/site/clickfunel/images/icon-120x120.png">

    <link href="public/site/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/site/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="public/site/clickfunel/css/base.css" rel="stylesheet" type="text/css">
    <link href="public/site/clickfunel/css/timeline.css" rel="stylesheet" type="text/css">
      <link href="public/site/clickfunel/css/hover-min.css" rel="stylesheet" type="text/css">
      <link href="public/site/fonts/css/font-awesome.css" rel="stylesheet" type="text/css">
<!--    <link rel="stylesheet" href="public/site/clickfunel/css/youtube-embed.css">-->
<!--    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick.css"/>-->
<!--    <link rel="stylesheet" type="text/css" href="public/site/clickfunel/css/slick-theme.css"/>-->
    <link rel="stylesheet" href="public/site/clickfunel/css/ionicons.min.css">
    <meta name="API_URL" content="<?php echo CDN_API_ONLINE;?>" />
    <meta name="description" content="Viên Uống Sắc Ngọc Khang đào thải nám tận gốc giúp làm mờ nám, sạm, tàn nhang, dưỡng da hồng hào, khỏe mạnh. Đặt hàng ngay để nhận ưu đãi!" />
    <meta property="og:url" content="<?php echo current_url() ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Viên Uống Sắc Ngọc Khang - Làm mờ nám sạm tàn nhang, chăm sóc da khỏe mạnh" />
    <meta property="og:description" content="Viên Uống Sắc Ngọc Khang đào thải nám tận gốc, giúp làm mờ nám, sạm, tàn nhang, dưỡng da hồng hào, khỏe mạnh. Đặt hàng ngay để nhận ưu đãi!" />
    <meta property="og:image" content="<?php echo base_url().'public/site/images/banner_new.webp'; ?>" />

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        '//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '299203756911918');
        fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=299203756911918&ev=PageView&noscript=1"
      /></noscript>
        <!--add 18/11-->
      <!-- Global site tag (gtag.js) - Google Ads: 1005741814 -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=AW-1005741814"></script>
      <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'AW-1005741814');
      </script>
      <!-- Event snippet for Giỏ hàng mới giaiphap.sacngockhang.com remarketing page -->
      <script>
          gtag('event', 'conversion', {
              'send_to': 'AW-1005741814/aGNuCNTJ_ekBEPbNyd8D',
              'value': 1.0,
              'currency': 'VND',
              'aw_remarketing_only': true
          });
      </script>
      <!-- add 16/11/2020 -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-28114834-6"></script>
      <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-28114834-6');
      </script>
  </head>
  <body class="">
    <!-- NEWOM ADS -->
     <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TGT5RM2" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- End Google Tag Manager (noscript) -->
    <?php $homepage = "http://sacngockhang.com"; ?>
    <?php echo $header; ?>
    <?php echo $content; ?>
    <?php echo $footer; ?>
<!--card ne-->

    <input type="hidden" id="baseurl" value="<?php echo base_url() ?>" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->

    <script src="public/site/clickfunel/js/jquery.js"></script>
    <script src="public/site/clickfunel/js/lazyload.min.js"></script>
    <script>
      $("img").lazyload({
          effect : "fadeIn"
      });
    </script>
<!--    <script src="public/site/clickfunel/js/youtube-embed.js"></script>-->
<!--    <!-- Latest compiled JavaScript-->-->
<!--    <script src="public/site/clickfunel/js/tether.min.js" type="text/javascript"></script>-->
<!--    <script src="public/site/clickfunel/js/bootstrap.min.js"></script>-->
<!--    <script src="public/site/clickfunel/js/slick.min.js"></script>-->
    <?php echo $_scripts; ?>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;amp;subset=vietnamese" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,700&amp;subset=vietnamese" rel="stylesheet">
<!--    <script>-->
<!--      $('#myCarousel').carousel({interval:5000});-->
<!--    </script>-->
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
    window.fbAsyncInit = function() {
      FB.init({
        xfbml            : true,
        version          : 'v4.0'
      });
    };

    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    </script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
      attribution=install_email
      page_id="141058836006826"
      theme_color="#d71149">
    </div>

  </body>
</html>
