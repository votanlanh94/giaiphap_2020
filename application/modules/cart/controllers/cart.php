<?php
class Cart extends CI_Controller {
  public function __construct() {
    parent::__construct();
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $this->load->library('template');
    $this->template->set_template('card');
  //clear cache
  $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
  $this->output->set_header('Pragma: no-cache');
  $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    //load menu
    $api_get_cat_post = $this->call_api('api_order/get_category_post','GET');
    $data['menu'] = array();
    if(!empty($api_get_cat_post) && !$api_get_cat_post->error) {
      $data['menu'] = $api_get_cat_post->data;
    }
    $data['homepage'] = 'http://sacngockhang.com';
    $data['show_menu_main'] = false;
    $data['show_banner'] = false;
    $this->template->write_view('header','home/header',$data);
    $this->template->write_view('footer','home/footer');
  }

  public $data = array();

  public function check_out() {
    $getCity = $this->call_api('api_order/get_city','GET');
    $city = array();
    if(!empty($getCity)) {
      $city = $getCity->data;
    }
    $getDistrict = $this->call_api('api_order/get_district', 'GET');
    $district = array();
    if(!empty($getDistrict)) {
      $district = $getDistrict->data;
    }
    $this->data['city'] = $city;
    $this->data['district'] = $district;

    $this->data['product_id_of_page'] = $product_id_of_page = 1;
    $sendGetProduct = array(
      'table' => 'ttp_report_products',
      'column'=> 'ID',
      'value' => $product_id_of_page,
      'type'  => 'row'
    );
    $getProduct = $this->call_api('api_order/get_data','POST',$sendGetProduct);
    $cart = $this->session->userdata('cart_funel');
    if(empty($cart)) {redirect('/','refresh');}
    if(!$getProduct->isError) {
      if(empty($cart)) {
        $product = $this->get_price_sale($getProduct->data);
        $cart_data[$product->ID] = array(
          'Title' => $product->Title,
          'price_sale' => $product->price_sale,
          'Image' => CDN_IMAGE.$product->PrimaryImage,
          'Amount'=> 1
        );
        $this->session->set_userdata('cart_funel', $cart_data);
        $cart = $cart_data;
      }
    }

    $this->data['cart'] = $cart;
    $this->template->add_title("Giỏ hàng | Viên uống Sắc Ngọc Khang");
    $this->template->add_js("public/site/clickfunel/js/cart.js");
//    $this->template->add_js("public/site/clickfunel/js/script.js");
    $this->template->write_view('content','cart_page',$this->data);
    $this->template->render();
  }

  /**
   * Get price sale this time
   * @param  [object] $product [product info]
   * @return [object]          [product with price now]
   */
  public function get_price_sale($product) {
    $today = date('Y-m-d H:i:s');
    $product->price_sale = $product->Price;
    $price_discount = $product->SpecialPrice;
    $discount_start = $product->SpecialStartday;
    $discount_end = $product->SpecialStopday;

    if(!empty($product)) {
      if( $discount_start <= $today && $today <= $discount_end ) {
        $product->price_sale = $price_discount;
    }
    return $product;
  }
      }

  private function call_api($url='index', $method='GET', $data=array()) {
    $url_api = CDN_API_ONLINE.$url;
    $method == 'POST' ? true : false;
    $curl = curl_init();
    $res = array(
      'isError' => false,
      'msg'     => '',
      'data'    => ''
    );
		$data_string = http_build_query($data);
		$ch = curl_init($url_api);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
    return json_decode($response);
    curl_close($curl);
  }

// ---------------------------------------
  public function test_api() {
    $data = array(
			"CityID"=> 2,
			"coupon" => "5A73B747",
			"total" => 200000,
			"fee" => 0
		);
		$data_string = http_build_query($data);
		$ch = curl_init(CDN_API_ONLINE.'api_order');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);
		var_dump($response);

  }

    public function add_cart(){
      $ProductsID = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
      $Amount = isset($_POST['amount']) ? (int)$_POST['amount'] : 1 ;
      $Amount = $Amount <= 0 ? 1 : $Amount ;
      $cart_name = isset($_POST['cart_name']) ? $this->lib->fill_data($_POST['cart_name']) : "cart_funel" ;
      $cart = $this->session->userdata("{$cart_name}");      

      if($ProductsID>0){
        $dataInput = array('productID' => $ProductsID);
        $getProduct = $this->call_api('api_order/get_product_by_id','POST', $dataInput);
        $Products = !$getProduct->isError ? $this->get_price_sale($getProduct->data) : array();

        if(!empty($Products)){
          $cart[$Products->ID] = array(
            'Title'      => $Products->Title,
            'price_sale' => $Products->price_sale,
            'Price'      => $Products->Price,
            'Image'      => CDN_IMAGE.$Products->PrimaryImage,
            'Amount'     => $Amount
          );

          $this->click_position(2,count($cart),1,$Products->Title);
          $this->session->set_userdata("{$cart_name}",$cart);
        }
      }
      $cart_total = $this->get_cart_total($cart);
      echo json_encode(array('error'=>false,'message'=>'add cart success','data'=>$cart_total, 'cart' => $cart));
    }

    public function load_box_check_order(){
        $this->load->view("load_box_check_order");
    }

    public function get_cart_total($cart){
      // $cart = $this->session->userdata("cart_funel");
      $cart = $cart!='' ? (array)$cart : array();
      $total = 0;
      if(count($cart) > 0) {
        foreach($cart as $product_id=>$row) {
          $total += (int)$row['price_sale'] * $row['Amount'];
        }
      }
      return $total;
    }

    public function load_order(){
      $getWarehouse = $this->call_api('api_order/get_warehouse','POST', array('Published' => 1));
      $warehouse = $getWarehouse->data;
      $getCity = $this->call_api('api_order/get_city','GET');
      $city = $getCity->data;
      $getDistrict = $this->call_api('api_order/get_district', 'GET');
      $district = $getDistrict->data;
      $this->data = array(
          'cart'      => $this->session->userdata("cart_funel"),
          'warehouse' => $warehouse,
          'city'      => $city,
          'district'   => $district
      );
      $this->load->view("cart",$this->data);
    }


    public function thanks_page(){
        $this->load->library('template');
        $this->template->set_template('site');
        $session = $this->session->userdata("customer");
        $session_isOrderGiaiPhap = $this->session->userdata('isOrderGiaiphap');

        $total = isset($session['Total']) ? $session['Total'] : 0 ;
        $Phone = isset($session['Phone']) ? $this->lib->convert_phone_number($session['Phone']) : 0 ;
        $data = array('Total'=>$total, 'Phone'=>$Phone);

        $data['isOrderGiaiphap'] = isset($session_isOrderGiaiPhap) ? $session_isOrderGiaiPhap : 0 ;

        $this->template->write_view("content","thanks",array('Total'=>$total,'Phone'=>$Phone));
        $this->template->render();
    }

    public function remove_products(){
        $response = array('error'=>false,'message'=>'remove cart success');
        $cart = $this->session->userdata("cart_funel");
        $cart = $cart!='' ? $cart : array() ;
        if(count($cart)>1){
            $ProductsID = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
            if($ProductsID>0){
                unset($cart[$ProductsID]);
                $this->session->set_userdata("cart_funel",$cart);
            }
        }else{
            $response = array('error'=>true,'message'=>'Đơn hàng phải có ít nhất 1 sản phẩm');
        }
        echo json_encode($response);
    }

    public function click_position($click=1,$position=0,$funelID=0,$title=''){
        $position = isset($_POST['position']) ? (int)$_POST['position'] : $position ;
        $funelID = isset($_POST['funel']) ? (int)$_POST['funel'] : $funelID ;
        $title = isset($_POST['title']) ? $_POST['title'] : $title ;
        if($funelID>0){
            $data = array(
                'FunelsID'  => $funelID,
                'Click'     => $click,
                'TitleButton'=> $title,
                'Position'  => $position,
                'YesNo'     => 1,
                'Created'   => date('Y-m-d H:i:s')
            );
            // $this->db->insert('ttp_funels_report',$data);
        }
        if($click==1)
        echo json_encode(array('error'=>false,'message'=>''));
    }

    public function create_order() {
        $response = array('error'=>false,'message'=>'','focus'=>'', 'isOrderGiaiphap'=>0);
        $cart_name = isset($_POST['cart_name']) ? $this->lib->fill_data(trim($_POST['cart_name'])) : 'cart_funel' ;
        $cart = $this->session->userdata("{$cart_name}");
        if(count($cart)==0){
          echo json_encode($response);
          return;
        }
        $site         = $_SERVER['HTTP_HOST'];
        $coupon_code  = isset($_POST['coupon_code']) ? mysql_real_escape_string(trim($_POST['coupon_code'])) : '' ;
        $CityID       = isset($_POST['CityID']) ? (int)$_POST['CityID'] : 0 ;
        $DistrictID   = isset($_POST['DistrictID']) ? (int)$_POST['DistrictID'] : 0 ;
        $FunelID      = isset($_POST['FunelID']) ? (int)$_POST['FunelID'] : 0 ;
        $AddressOrder = isset($_POST['AddressOrder']) ? strip_tags(trim($_POST['AddressOrder'])) : '' ;
        $phone = isset($_POST['Phone']) ? $this->lib->convert_phone_number(strip_tags(trim($_POST['Phone']))) : '' ;
        $phone = preg_replace('/[^0-9]+/i', '', $phone);
        $name  = isset($_POST['Name']) ? strip_tags(trim($_POST['Name'])) : '' ;
        $email = isset($_POST['Email']) ? strip_tags(trim($_POST['Email'])) : '' ;
        $note  = isset($_POST['Note']) ? strip_tags(trim($_POST['Note'])) : '' ;
        if($AddressOrder==''){$response['error'] = true;$response['focus'] = 'AddressOrder';$response['message'] = 'Địa chỉ nhận hàng không hợp lệ';}
        if($CityID==0){$response['error'] = true;$response['focus'] = 'CityID';$response['message'] = 'Tỉnh/ Thành phố buộc phải chọn';}
        if($DistrictID==0){$response['error'] = true;$response['focus'] = 'DistrictID';$response['message'] = 'Quận/ Huyện buộc phải chọn';}
        if(strlen($phone)<10 || strlen($phone)>11){$response['error'] = true;$response['focus'] = 'Phone';$response['message'] = 'Số điện thoại không hợp lệ.';}
        if($name=='' || strlen($name)<2){$response['error'] = true;$response['focus'] = 'Name';$response['message'] = 'Họ tên không hợp lệ';}
        if($coupon_code != '') {
          $data = array("coupon" => $coupon_code);
          $apiCheckCoupon=$this->call_api('api_order/check_voucher','POST',$data);
          if($apiCheckCoupon->error) {
            $response['error']=true;
            $response['message']=$apiCheckCoupon->message;
          }
        }
        if($response['error']==false){
          $dataInput = array(
            'cart'        => $cart,
            'site'        => $site,
            'coupon_code' => $coupon_code,
            'CityID'      => $CityID,
            'DistrictID'  => $DistrictID,
            'FulnelID'    => $FunelID,
            'SourceID'    => 9, //web_order
            'AddressOrder'=> $AddressOrder,
            'Phone'       => $phone,
            'Name'        => $name,
            'Email'       => $email,
            'Note'        => $note
          );
          $apiCreateOrder = $this->call_api('api_order/create_order','POST',$dataInput);
        }
        echo json_encode($response);
    }
    public function send_comment(){
      $Name = isset($_POST['Name']) ? strip_tags($_POST['Name']) : '' ;
      $FunelID = isset($_POST['FunelID']) ? (int)$_POST['FunelID'] : 0 ;
      $Phone = isset($_POST['Phone']) ? $this->lib->convert_phone_number(strip_tags($_POST['Phone'])) : '' ;
      $Comments = isset($_POST['Comments']) ? strip_tags($_POST['Comments']) : '' ;
      $response = array('error'=>false,'message'=>'','focus'=>'');
      if($Comments==''){$response['error'] = true;$response['focus'] = 'Comments';$response['message'] = 'Vui lòng nhập bình luận';}
      if($Name==''){$response['error'] = true;$response['focus'] = 'Name';$response['message'] = 'Vui lòng nhập tên';}
      if($response['error']==false){
        $data = array(
          'Name'      => $Name,
          'Phone'     => $Phone,
          'FunelID'   => $FunelID,
          'Comments'  => $Comments,
          'Create'    => date('Y-m-d H:i:s'),
          'Status'    => 1
        );
        $apiSendComment =$this->call_api('api_order/send_comment','POST',$data);
      }
      echo json_encode($response);
    }

    public function load_comment(){
      $FunelID = isset($_POST['FunelID']) ? (int)$_POST['FunelID'] : 0 ;
      $data = array("FunelID"=>$FunelID);
      $Comments = $this->call_api('api_order/load_comment','POST',$data);
      $this->data['Comments'] = $Comments->data;
      if(isset($Comments->data)) {
        $this->load->view("comment",$this->data);
      }
    }

    public function send_callme(){
      $Name = isset($_POST['Name']) ? strip_tags($_POST['Name']) : '' ;
      $Phone = isset($_POST['Phone']) ? $this->lib->convert_phone_number(strip_tags($_POST['Phone'])) : '' ;
      $Comments = isset($_POST['Note']) ? strip_tags($_POST['Note']) : '' ;
      $response = array('error'=>false,'message'=>'','focus'=>'');
      if($Comments==''){$response['error'] = true;$response['focus'] = 'Note';$response['message'] = 'Vui lòng nhập nội dung cấn tư vấn';}
      if($Phone==''){$response['error'] = true;$response['focus'] = 'Phone';$response['message'] = 'Vui lòng nhập số điện thoại';}
      if($Name==''){$response['error'] = true;$response['focus'] = 'Name';$response['message'] = 'Vui lòng nhập tên';}
      if($response['error']==false){
        $data = array(
          'Name'      => $Name,
          'Phone'     => $Phone,
          'Note'      => $Comments,
          'Created'   => date('Y-m-d H:i:s')
        );
        $response = $this->call_api('api_order/send_callme','POST',$data);
      }
      echo json_encode($response);
    }

    public function check_coupon_code(){
      $coupon = isset($_POST['coupon']) ? mysql_real_escape_string(trim($_POST['coupon'])) : 0 ;
      $Total = isset($_POST['total']) ? (int)$_POST['total'] : 0 ;
      $fee = isset($_POST['fee']) ? (int)$_POST['fee'] : 0 ;
      $Total = $Total+$fee;
      $data = array("coupon" => $coupon);
      $apiCheck = $this->call_api('api_order/check_voucher','POST',$data);
      $response = array("error"=>true,"focus"=>"#coupon_code","message"=>"Mã giảm giá bạn vừa nhập không hợp lệ.","data"=>"");

      if(!$apiCheck->error) {
        $check = $apiCheck->data;
        if($check){
          $response["error"] = false;
          if($check->PriceReduce>0){
            $response["message"] = "Mã của bạn được giảm ".number_format($check->PriceReduce)."đ";
            $response["coupon"] = $check->PriceReduce;
          }
          if($check->PercentReduce>0){
            $response["message"] = "Mã của bạn được giảm ".number_format($check->PercentReduce)."%";
            $response["coupon"] = $check->PercentReduce*100/$total;
          }
        }
      } else {
        $response["message"] = $apiCheck->message;
      }
      echo json_encode($response);
    }

    public function load_district_from_city(){
      $CityID = isset($_POST['CityID']) ? (int)$_POST['CityID'] : 0 ;
      $District = $this->call_api('api_order/load_district_from_city','POST',array("CityID"=>$CityID));
      echo $District;
    }

    public function getfee(){
        $id = isset($_POST['district']) ? (int)$_POST['district'] : 0 ;
        $total = isset($_POST['total']) ? (int)$_POST['total'] : 0 ;
        $coupon = isset($_POST['coupon']) ? (int)$_POST['coupon'] : 0 ;
        $total = $total-$coupon;
        $apiGetDistrict = $this->call_api('api_order/get_district_by_id', 'POST', array("id" => $id));
        $check = array();
        if(!$apiGetDistrict->isError) {
          $check = $apiGetDistrict->data;
          $check = $check[0];
        }
        $fee = $this->value_fee($id, $total, $coupon);
        echo json_encode($fee);
        echo json_encode(array("fee"=>$fee>0 ? "Chi phí vận chuyển phát sinh cho khu vực bạn vừa chọn : <b>".number_format($fee)."đ</b>" : "Đơn hàng của bạn được miễn phí vận chuyển giao hàng tận nơi.","number_fee"=> $fee,"total"=> $fee+$total));
    }

    public function value_fee($id = 0, $total = 0, $coupon = 0)
    {
        $total = $total - $coupon;
        $check = $this->call_api('api_order/get_district_by_id', 'POST', array("id" => $id));
        $fee   = $ship_range_1 = $ship_range_2 = 0;
        $o_ship_range = $this->call_api('api_order/get_config', 'POST', array("group" => "shipping", "type" => "ship_range"));
        return $o_ship_range;
        if (!empty($o_ship_range)) {
            foreach ($o_ship_range->data as $i_ship) {
                if ($i_ship->code == 'range_1')
                    $ship_range_1 = $i_ship->name;

                if ($i_ship->code == 'range_2')
                    $ship_range_2 = $i_ship->name;
            }

            if ($check) {
                if ($total < $ship_range_1) {
                    $fee = $check->PriceCost;
                }

                if ($total >= $ship_range_1 && $total < $ship_range_2) {
                    $fee = $check->PriceCost1;
                }

                if ($total >= $ship_range_2) {
                    $fee = $check->PriceCost2;
                }
            }
        }
        return $fee;
    }

    public function clear_cart() {
        $this->session->unset_userdata('cart_funel');
        $this->session->sess_destroy();
    }
}
?>
