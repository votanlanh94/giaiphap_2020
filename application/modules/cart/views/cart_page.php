
<header class="header">
  <!-- End top-header-->
  <div class="main-head">
    <div class="container">
      <div class="row">
        <div class="col-xs-5 col-sm-4 col-md-2 text-center bg-logo">
          <a href="/">
            <img src="public/site/images/logo-sacngockhang.webp" style="max-width:150px;margin:10px auto" class="img-responsive" />
          </a>
        </div>
        <div class="col-xs-6 col-sm-8 col-md-10">
          <label for="openmenu" class="hidden-md hidden-lg"><span class="ion-android-menu"></span></label>
          <input type="checkbox" id="openmenu" class="hidden">
          <ul id="main-menu">
            <li><a href="/" id="trang-chu" class="current">Trang chủ <span>Viên Uống</span></a></li>
            <li>
            	<a href="tel:19006033">Hotline <span>19006033</span></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- End main-head-->
</header>

<div class="container-fluid form-order-wrapper" id="order">
	<img src="public/site/images/logo-sacngockhang.png" style="max-width:250px;margin:10px auto" class="img-responsive">
	<div class="card-wrapper">
		<div class="container">
			<div class="row">
				<h3>Thông tin đơn hàng</h3>
			</div>
			<div class="order-form">
				<div class="row">
		      <form id="create-order">
		        <div class="col-md-6 col-xs-12">
		          <div class="cart-view">
		            <table class="table">
		              <tbody>
		                <?php
		                $total = 0;
		                if(!empty($cart) && is_array($cart) && count($cart)>0){
		                    foreach($cart as $key=>$row){
		                    ?>
		                    <tr class="row">
		                        <td class="col-xs-4 col-md-3">
		                        	<img src="<?php echo $row['Image'] ?>" alt="serum-duong-trang-da" width="100%">
		                        </td>
		                        <td class="col-xs-8 col-md-9 product-detail">
		                          <p><?php echo $row['Title'] ?></p>
		                          <div class="price-wrapper-cart">
		                            <p class="prd-price pull-left"><span class="price"><?php echo number_format($row['price_sale']) ?>đ </span></p>
		                            <p class="prd-quanlity pull-left">
		                              <input type="number" min="1" value="<?php echo $row['Amount'] ?>" name="product-quality" onChange="change_amount_cart(this,'add_cart',<?php echo $key ?>)" class="form-control form-control-sm">
		                            </p>
		                            <a onclick="remove_cart(this,'remove_products',<?php echo $key ?>)" class="group-icon pull-right remove-item"><i class="snk snk-trash-empty"></i><span class="d-sm-none">xóa</span></a>
		                          </div>
		                        </td>
		                      </tr>
		                    <?php
		                        $total = $total + ($row['price_sale']*$row['Amount']);
		                    }
		                }
		                ?>
		              </tbody>
		            </table>
		            <p><span>Tổng giá tiền:</span><b class="pull-right" id="total" total="<?php echo $total ?>"><?php echo number_format($total) ?>đ</b></p>

		            <div class="col-xs-12">
		                <div id="countdown" class="count2">
		                    <p class="font-weight-bold">Thời gian ưu đãi còn lại</p>
		                    <ul>
		                        <li><span id="days" style="color:#595959;padding: 5px"></span>Ngày</li>
		                        <li><span id="hours" style="color:#595959;padding: 5px"></span>Giờ</li>
		                        <li><span id="minutes" style="color:#595959;padding: 5px"></span>Phút</li>
		                        <li><span id="seconds" style="color:#595959;padding: 5px"></span>Giây</li>
		                      </ul>
		                </div>
		            </div>
		          </div>
		          <?php
		            $show=false;
		             if($show) :
		          ?>
		          <div class="row">
		                <div class="col-xs-12 uudai"><p>Bạn có mã ưu đãi không ?</p></div>
		                <div class="col-md-6 grid-m-7" style="margin-bottom:15px;">
		                    <input type="text" class="form-control" id="coupon_code" coupon="0" name="coupon_code" placeholder="Nhập mã giảm giá của bạn vào đây">
		                </div>
		                <div class="col-md-6 grid-m-5">
		                    <button type="button" class="btn btn-secondary" onclick="check_coupon_code(this,'check_coupon_code')">Kiểm tra mã</button>
		                </div>
		                <div class="col-xs-12 hidden" id="coupon_message"><div class="alert alert-danger" style="margin-bottom:15px;"></div></div>
		          </div>
		        <?php endif;?>
		          <div class="row hidden" id="error-box-cart">
		            <div class="col-xs-12">
		                <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span></span></div>
		            </div>
		          </div>
		        </div>
		        <div class="col-md-6 col-xs-12">
		            <div class="row m-t-2">
		                <div class="col-xs-12 form-group">
		                  <div class="form-check form-check-inline">
		                    <label class="form-check-label">
		                      <input id="inlineRadio1" type="radio" name="Sex" value="1" class="form-check-input"><span>Anh </span>
		                    </label>
		                  </div>
		                  <div class="form-check form-check-inline">
		                    <label class="form-check-label">
		                      <input id="inlineRadio2" type="radio" name="Sex" value="0" checked="checked" class="form-check-input"><span>Chị </span>
		                    </label>
		                  </div>
		                </div>
		                <div class="col-md-6 grid-m-6 form-group">
		                  <input placeholder="Họ tên" type="text" name="Name" class="form-control">
		                </div>
		                <div class="col-md-6 grid-m-6 form-group">
		                  <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
		                </div>
		                <div class="col-md-6 grid-m-6 form-group">
		                  <input type="email" id="Email" name="Email" placeholder="Email (không bắt buộc)" class="form-control">
		                </div>
		                <div class="col-md-6 grid-m-6 form-group">
		                  <input placeholder="Ghi chú" name="Note" type="text" class="form-control">
		                </div>
		                <div class="col-xs-12 form-group" style="display: none"><b class="db">Để được phục vụ tốt nhất, vui lòng chọn:</b><br>
		                  <div class="form-check form-check-inline">
		                    <label class="form-check-label">
		                      <input id="inlineRadio1" type="radio" onclick="tab(this,1)" checked="checked" name="AddressChoose" value="1" class="form-check-input"><span>Giao tận nơi</span>
		                    </label>
		                  </div>
		                </div>
		                <div class="col-xs-12 address-input">
		                  <div class="card">
		                    <div class="card-block">
		                      <div class="row" id="tab1">
		                        <div class="col-md-6 grid-m-6 form-group">
		                          <select id="selectCity" name="CityID" class="form-control" onchange="get_district(this)">
		                              <?php
		                              if(count($city)>0){
		                                  foreach($city as $row){
		                                      $selected = $row->ID==30 ? 'selected="selected"' : '' ;
		                                      echo "<option value='$row->ID' $selected>$row->Title</option>";
		                                  }
		                              }
		                              ?>
		                          </select>
		                        </div>
		                        <div class="col-md-6 grid-m-6 form-group">
		                          <select id="selectDistrict" onchange="getfee(this,'getfee')" name="DistrictID" class="form-control">
		                            <option value="0">Chọn quận, huyện</option>
		                              <?php
		                              if(count($district)>0){
		                                  foreach($district as $row){
		                                      echo "<option value='$row->ID'>$row->Title</option>";
		                                  }
		                              }
		                              ?>
		                          </select>
		                        </div>
		                        <div class="col-xs-12 form-group">
		                          <input id="AddressOrder" name="AddressOrder" placeholder="Số nhà, tên đường, phường/ xã" type="text" class="form-control">
		                        </div>
		                      </div>
		                      <div class="row">
		                        <div class="col-xs-12 hidden" id="fee" fee="0"><div class="alert alert-warning" style="margin-top:0px;">Chi phí vận chuyển phát sinh cho khu vực bạn vừa chọn : <b>0đ</b></div></div>
		                      </div>
		                    </div>
		                  </div>
		                </div>
		              </div>

		              <div class="row hidden" id="error-box">
		                  <div class="col-xs-12">
		                      <div class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
		                  </div>
		              </div>
		              <input type="hidden" name="FunelID" value="1" />
		        </div>
		        <div class="text-center">
		        	 <button type="button" class="btn btn-primary buttonred animated infinite pulse" onclick="send_order(this,'create-order','create_order')">Đặt hàng		</button>
		        </div>
		      </form>
		    </div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid m-b-3">
    <div class="row justi-content">
        <div class="col-md-4 hv-scale" style="line-height: 2">
            <div class="pricing-card p-3 text-center py-5 mt-2 p-t-2 p-b-2">
                <div class="images p-l-2 p-r-2"><img src="public/site/images/combo2.webp" width="80%" loading="lazy"></div>
                <p class="p-t-2><span class="d-block font-weight-bold mt-3">570.000đ <del>600.000đ</del></span></p>
                <button class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" type="button" onclick="add_to_cart_and_redirect(this,'add_cart', 106 ,1, 'menu')">
                    Mua Ngay
                </button>
            </div>
        </div>
        <div class="col-md-4 hv-scale" style="line-height: 2">
            <div class="pricing-card p-3 text-center py-5 p-t-2 p-b-2">
                <div class="images p-l-2 p-r-2"><img src="public/site/images/combo3.jpg" width="80%" loading="lazy"></div>
                <p class="p-t-2><span class="d-block font-weight-bold mt-3">699.000đ <del>1.111.000đ</del></span></p>
                <button class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" type="button" onclick="add_to_cart_and_redirect(this,'add_cart', 380 ,1, 'menu')">
                    Mua Ngay
                </button>
            </div>
        </div>
        <div class="col-md-4 hv-scale" style="line-height: 2">
            <div class="pricing-card p-3 text-center py-5 mt-2 p-t-2 p-b-2">
                <div class="images p-l-2 p-r-2"><img src="public/site/images/combo1.webp" width="80%" loading="lazy"></div>
                <p class="p-t-2><span class="d-block font-weight-bold mt-3">1.080.000đ <del>1.200.000đ</del></span></p>
                <button class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" type="button" onclick="add_to_cart_and_redirect(this,'add_cart', 330 ,1, 'menu')">
                    Mua Ngay
                </button>

            </div>
        </div>
    </div>
</div>
<style>
	.form-order-wrapper {
		background-image: url('/public/site/clickfunel/images/bg-promotion.jpg');
		background-size: cover;
		padding: 50px;
		border-style: solid;
		border-color: #fff;
		border-width: 0px;
	}

	.form-order-wrapper .card-wrapper {
		background: #fff;
		padding: 20px;
		box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
	}
</style>

