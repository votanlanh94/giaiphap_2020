    <?php if(isset($show_menu_main) && $show_menu_main): ?>
    <nav class="top-menu navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?=$homepage?>">
            <img src="public/site/clickfunel/images/logo.svg" style="max-width:150px;margin:7px auto 10px auto" class="img-responsive" />
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?=$homepage?>">VỀ TRANG CHỦ</a></li>
            <?php
              if(isset($menu) && count($menu)>0) {
                foreach($menu as $key=>$item) {
                  if($key>0) {
                    $url = $homepage.'/'.$item->Alias;
                    echo "<li><a href='$url' target='_blank'>$item->Title</a></li>";
                  }
                }

              }
            ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <?php endif;?>
    <?php if(isset($show_banner) && $show_banner): ?>
    <div class="container-fluid" id="slider">
      <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
      <!-- Indicators -->
<!--        <ol class="carousel-indicators">-->
<!--          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
<!--          <li data-target="#myCarousel" data-slide-to="1"></li>-->
<!--          <li data-target="#myCarousel" data-slide-to="2"></li>-->
<!--          <li data-target="#myCarousel" data-slide-to="3"></li>-->
<!--        </ol>-->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <a href="#dathang">
                    <img class="third-slide w-100 hidden-sm hidden-xs" src="public/site/images/banner_new.webp" alt="Third slide">
                    <img class="third-slide w-100  hidden-md hidden-lg hidden-lg" src="public/site/images/banner_new_mb.webp" alt="Third slide">
                </a>
                <div class="container">
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>

<!--            <div class="item">-->
<!--                <a href="#dathang">-->
<!--                    <img class="third-slide" src="public/site/clickfunel/images/banner/serum-mua-mot-tang-3.jpg" alt="Third slide">-->
<!--                </a>-->
<!--                <div class="container">-->
<!--                    <div class="carousel-caption">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

<!--          <div class="item">-->
<!--            <a href="/dat-hang?click-at=banner#order">-->
<!--              <img class="first-slide" src="/public/site/clickfunel/images/banner/Ads_Serum-SNK-19.jpg" alt="First slide">-->
<!--            </a>-->
<!--            <div class="container">-->
<!--            </div>-->
<!--          </div>-->
<!--          <div class="item">-->
<!--            <a href="#dotpha">-->
<!--              <img class="third-slide" src="/public/site/clickfunel/images/banner/Ads_Serum-SNK-16.jpg" alt="Third slide">-->
<!--            </a>-->
<!--            <div class="container">-->
<!--              <div class="carousel-caption">-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">-->
<!--          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
<!--          <span class="sr-only">Previous</span>-->
<!--        </a>-->
<!--        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">-->
<!--          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
<!--          <span class="sr-only">Next</span>-->
<!--        </a>-->
      </div><!-- /.carousel -->
    </div>
    <?php endif;?>
