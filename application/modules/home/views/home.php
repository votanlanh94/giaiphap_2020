<?php
$dayend = date('t/m');
?>
<!--header-->
<?php include("detail/header.php"); ?>
<!--gioi thieu-->
<?php include("detail/introduce.php"); ?>
<!--bi quyet-->
<?php include("detail/biquyet.php"); ?>
<!--dot phat-->
<?php //include("detail/dotpha.php"); ?>
<!--form mua hang 2-->
<?php include("detail/muangay.php"); ?>
<!--khuyenmai-->
<?php include("detail/cauhoi.php"); ?>
<!--khuyenmai-->
<?php include("detail/khuyenmai.php"); ?>
<!--chuyengia-->
<?php include("detail/chuyengia.php"); ?>
<!--nha may hien dai-->
<?php include("detail/nhamay.php"); ?>
<!--danhhieu-->
<?php include("detail/danhieu.php"); ?>
<!--hoa hau tin dung-->
<?php include("detail/hoahau.php"); ?>
<!--nghe si tin dung-->
<?php include("detail/nghesi.php"); ?>

<!--cam ket chinh hang-->
<?php include("detail/camket.php"); ?>
<!--danh gia, trai nghiem-->
<?php //include("detail/danhgia.php"); ?>
<!--quy trinh cham soc gia-->
<?php //include("detail/quytrinh.php"); ?>
<!--cua hang lien ket-->
<?php include("detail/cuahang.php"); ?>

<div id="callme-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"><span>TƯ VẤN CHO TÔI</span></h5>
            </div>
            <div class="modal-body">
                <form id="form-callme">
                    <div class="row">
                        <div class="col-xs-6 form-group">
                            <input placeholder="Họ tên" type="text" class="form-control" name="Name">
                        </div>
                        <div class="col-xs-6 form-group">
                            <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
                        </div>
                        <div class="col-xs-12 form-group">
                            <textarea placeholder="Nội dung cần chúng tôi tư vấn..." rows="3" name="Note" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row hidden" id="box-alert-callme" style="margin-top:10px;">
                        <div class="col-xs-12">
                            <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_callme(this,'form-callme','send_callme')">Gửi</button>
                <div class="logo">
                    <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="notice-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"><span>Thông báo</span></h5>
            </div>
            <div class="modal-body">
                <div class="notice-content"></div>
            </div>
            <div class="modal-footer text-center">
                <div class="logo">
                    <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bottom-function">
    <div id="scrollUp" class="scroll-up pull-right"><i class="snk snk-angle-up snk-2x"></i></div>
</div>
<a id="alo-phoneIcon" onclick="location.href='tel:19006033';" class="alo-phone alo-green alo-show">
    <div class="alo-ph-circle"></div>
    <div class="alo-ph-circle-fill"></div>
    <div class="alo-ph-img-circle"><span class="glyphicon glyphicon-earphone"></span></div>
    <span class="alo-ph-text">19006033</span>
    <div class="view-page">Có <span>556</span> người đang xem</div>
</a>
<div id="popmua" class="">
    <div class="popimg"><img src="public/site/images/vienuong.webp" alt="Viên uống Vi Tảo Lục" loading="lazy"></div>
    <div class="popmsg">Khách hàng ở Quảng Ngãi vừa đặt mua <span class="proname"> 3 Viên uống Sắc Ngọc Khang Vi Tảo Lục</span> <span class="poptime">18 phút trước</span>
    </div>
</div>
<input type="hidden" id="products_array" value='<?php $product = !empty($product) ? json_encode($product) : json_encode(array());
echo base64_encode($product); ?>'>

