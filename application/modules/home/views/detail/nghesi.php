<div id="nghesi" class="container m-t-3 m-b-3" style="position: relative">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 mg-20">
            <h3 class="text-uppercase font-weight-bold text-center"><span class="pri-text">Sản phẩm được hoa hậu – người nổi tiếng tin dùng</span></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-justify">
            <p class="font-weight-bold">Từ lúc ra đời, Sắc Ngọc Khang được tin tưởng và đồng hành cùng rất nhiều người đẹp: Á hậu quý bà thế
                giới 2009 – Hoàng Yến, Hoa hậu Việt Nam 2006 – Mai Phương Thúy, Hoa Hậu Việt Nam 2012 – Đặng Thu Thảo.
                Đây chính là minh chứng cho chất lượng và những giá trị mà Sắc Ngọc Khang đem lại cho các Hoa Hậu và cho
                phụ nữ Việt Nam</p>
            <p>Nhiều nghệ sĩ nổi tiếng như: Diễn viên Lan Phương, Trịnh Kim Chi, Khánh Huyền, người mẫu Thủy Hương…
                cũng rất ưa chuộng và thường xuyên sử dụng sản phẩm của Sắc
                Ngọc Khang. </p>
        </div>
    </div>
</div>
<div class="row justi-content m-t-2 m-b-2 center-text">
    <div class="col-xs-5 col-sm-6 col-md-3 transform-style-1 pd-mobile">
        <img src="public/site/images/dv-kimchi.jpg" alt="dv1" class="img img-fluid m-t-1" loading="lazy">
    </div>
    <div class="col-xs-5 col-sm-6 col-md-3 transform-style-1 pd-mobile">
        <img src="public/site/images/dv-thuyhuong.jpg" alt="dv2" class="img img-fluid m-t-1" loading="lazy">
    </div>
    <div class="col-xs-5 col-sm-6 col-md-3 transform-style-1 pd-mobile">
        <img src="public/site/images/dv-khanhhuyen.jpg" alt="dv3" class="img img-fluid m-t-1" loading="lazy">
    </div>
</div>