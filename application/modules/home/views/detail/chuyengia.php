<div id="chuyengia" class="text-center" style="position: relative">
    <div class="container p-a-3">
        <div class="row">
            <span><img src="public/site/images/rectange-1.png" alt="rectange-1" class="rectage-left"></span>
            <span><img src="public/site/images/rectangle-2.png" alt="rectange-2" class="rectage-right"></span>
            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="card text-center" id="bs1">
                    <img src="public/site/images/chuyen-gia-1.webp" alt="anh-chuyen-gia-1"
                         class="center-text img img-fluid transform-style-1 chuyengia-1" loading="lazy">
                    <div class="m-l-3" style="width: 200px;">
                        <p>Bác sỹ</p>
                        <h5 class="font-weight-bold">Huỳnh Huy Hoàng</h5>
                        <small>Trưởng khoa điều trị số 2 Bệnh viện Da Liễu TP.HCM</small>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 text-center">
                <div class="text-uppercase">
                    <h3 class="font-weight-bold">SẮC NGỌC KHANG KHẲNG ĐỊNH CHẤT LƯỢNG HÀNG ĐẦU</h3>
                </div>
                <h5>
                    Được cố vấn bởi đội ngũ chuyên gia, dược sĩ giàu kinh nghiệm
                </h5>
                <div class="border-dash padding-20">
                    Từ bước nghiên cứu công thức đến khâu sản xuất và kiểm soát chất lượng sản phẩm, Viên Uống Sắc Ngọc
                    Khang đều nhận được sự tư vấn chuyên sâu của Tiến sĩ Nguyễn Hoàng Tuấn cùng đội ngũ dược sĩ tâm huyết, giàu kinh
                    nghiệm. Sắc Ngọc Khang tự hào đem đến sản phẩm hiệu quả tốt, chất lượng cao, an toàn khi sử dụng.
                </div>
                <div class="row m-t-3 m-b-3">
                    <div class="col-md-6">
                        <div class="card text-center">
                            <img class="card-img-top h-150 transform-style-1" src="public/site/images/chuyen-gia-2.png"
                                 alt="anh-chuyen-gia-2" loading="lazy">
                            <div class="m-t-1">
                                <p>Tiến sĩ</p>
                                <p class="font-weight-bold">Nguyễn Hoàng Tuấn</p>
                                <small>Giảng viên Đại học Dược Hà Nội</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-center">
                            <img class="card-img-top h-150 transform-style-1" src="public/site/images/chuyen-gia-3.png"
                                 alt="anh-chuyen-gia-3" loading="lazy">
                            <div class="card-body m-t-1">
                                <p>Phó Giáo sư - Tiến sĩ</p>
                                <p class="font-weight-bold">Trương Văn Tuấn</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>