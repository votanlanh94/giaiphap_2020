<div id="thuonghieu"></div>
<div id="danhhieu" class="m-t-2 m-b-3">
    <div class="container-fluid">
        <div class="row m-b-2">
            <div class="col-md-12 m-t-1 ">
                <img src="public/site/images/hang-viet-nam.png" alt="img-thuong-hieu-viet-nam" class="img img-fluid img-hang-viet-nam m-b-3" loading="lazy">
                <h3 class="font-weight-bold text-center m-t-2 m-b-2 title-mobile-2">CÙNG NHIỀU DANH HIỆU, GIẢI THƯỞNG <br> KHẲNG ĐỊNH UY TÍN - CHẤT LƯỢNG.</h3>
            </div>
        </div>
        <div class="row box-shadow-3 justi-content center-text">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                <div class="card text-center">
                    <img class="card-img-top h-150 transform-style-1" src="public/site/images/danh-hieu-1.png" alt="Danh hieu" loading="lazy">
                    <div class="card-body m-t-1">
                        <p class="card-text txt-award">Giấy chứng nhận Hàng Việt Nam Chất Lượng Cao</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                <div class="card text-center">
                    <img class="card-img-top h-150 transform-style-1" src="public/site/images/danh-hieu-2.png" alt="Danh hieu" loading="lazy">
                    <div class="card-body m-t-1">
                        <p class="card-text txt-award">Sản phẩm uy tín, chất lượng vì sức khỏe cộng đồng</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                <div class="card text-center">
                    <img class="card-img-top h-150 transform-style-1" src="public/site/images/danh-hieu-3.png" alt="Danh hieu" loading="lazy">
                    <div class="card-body m-t-1">
                        <p class="card-text txt-award">Huy chương vàng vì sức khỏe cộng đồng</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                <div class="card text-center">
                    <img class="card-img-top h-150 transform-style-1" src="public/site/images/danh-hieu-4.png" alt="Danh hieu" loading="lazy">
                    <div class="card-body m-t-1">
                        <p class="card-text txt-award">Huy chương vàng vì sức khỏe cộng đồng</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                <div class="card text-center">
                    <img class="card-img-top h-150 transform-style-1" src="public/site/images/danh-hieu-5.png" alt="Danh hieu" loading="lazy">
                    <div class="card-body m-t-1">
                        <p class="card-text txt-award">Hàng Việt tốt 2014 do người tiêu dùng bình chọn</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
                <div class="card text-center" id="congdongviet">
                    <img class="card-img-top transform-style-1" src="public/site/images/danh-hieu-6.png" alt="Danh hieu" loading="lazy">
                    <div class="card-body m-t-1">
                        <p class="card-text txt-award">Thương hiệu vì cộng đồng người Việt</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
