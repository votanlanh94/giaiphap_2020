<section>
    <div id="thanhphan" class="content-style-2">
        <div class="container">
            <div class="row m-t-2">
                <div class="col-sm-12">
                    <h2 class="text-center">Nám, sạm, tàn nhang <br> Hiểu rõ nguyên nhân kiên trì chữa sẽ hết!</h2>
                   <div class="text-justify">
                       <p class="font-small">Nám, sạm, tàn nhang là biểu hiện của sự sa sút sức khỏe, suy giảm các yếu tố nội tại trong cơ thể. Muốn xóa được các vết nám, sạm, ngăn ngừa tàn nhang cần hiểu rõ các nguyên nhân gây nên.</p>
                       <p class="font-small">Theo các kết quả nghiên cứu của các nhà khoa học Việt Nam cũng như trên thế giới, <b>4 nguyên nhân chủ yếu gây nên nám sạm, tàn nhang, nếp nhăn </b>bao gồm:</p>
                   </div>

                </div>
            </div>
            <h3 class="font-weight-bold">1. Máu huyết</h3>
            <div class="row">
                <div class="col-sm-12 text-justify">
                    <p class="font-small">Ít ai biết rằng rất nhiều vấn đề về làn da (nám sạm, xanh xao) liên quan mật thiết đến tuần hoàn máu. Mất cân bằng khí huyết trong cơ thể
                        là một trong những
                        nguyên nhân chính gây ra vấn đề về da như: khô, nám sạm, tàn nhang…</p>
                    <p>
                        <img src="public/site/images/sac-ngoc-khang-mau-huyet.webp" alt="mauhuyet" class="img img-responsive" style="margin: 0 auto" loading="lazy">
                    </p>
                    <p class="font-small">Thực tế, máu duy trì sự sống vì máu mang chất dinh dưỡng và oxy đến nuôi dưỡng toàn bộ các tế bào trong cơ thể, trong đó có tế bào da. Nếu lưu thông máu kém hoặc máu xấu sẽ khiến hầu hết phụ nữ phải đối mặt với nhiều rắc rối trong cơ thể. Trong đó, nhan sắc sẽ bị ảnh hưởng trầm trọng và rõ nhất.</p>
                    <p class="font-small">Cụ thể, khi bị lưu thông máu kém hoặc máu xấu, ngoài bị mệt mỏi, đau đầu, hoa mắt, chóng mặt kéo dài không rõ nguyên nhân, tóc rụng, làn da trở nên xấu xí đi trông thấy. Khi ấy, da dễ trở nên nhạy cảm, nhợt nhạt, xanh xao, thiếu rạng rỡ, dễ bị nám sạm và nhiều tàn nhang hơn.</p>
                    <p class="font-small">Khi da bị “xuống cấp”, nếu không chăm sóc và chữa trị đúng cách, chúng sẽ ngày một tồi tệ và sẽ khiến chính bạn cảm thấy mất thẩm mỹ, dẫn đến tự ti trong giao tiếp hàng ngày.</p>
                </div>
            </div>
<!--            <h5 style="text-align: center;"><strong>HÃY XÓA SỔ NÁM SẠM, NẾP NHĂN BẰNG CÁCH TĂNG CƯỜNG MÁU HUYẾT</strong></h5>-->
<!--            <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">-->
<!--                <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>-->
<!--            </div>-->
        </div>
    </div>
</section>

<section>
    <div id="suygiamtietto" class="content-style-2">
        <div class="container">
            <h3 class="font-weight-bold">2. Suy giảm nội tiết tố</h3>
            <div class="row">
                <div class="col-sm-12 text-justify">
                    <p class="font-small">Estrogen là một loại hormone do buồng trứng tiết ra, nó được ví như dòng nhựa sống giúp phụ nữ duy trì được vóc dáng và làn da mịn màng. Đồng thời nó cũng là loại nội tiết giúp chị em duy trì ham muốn tình dục... Vì thế, sau thời kỳ xuân sắc, sự suy giảm estrogen cũng là một trong những nguyên nhân chính dẫn đến suy giảm về nhan sắc, sinh lý và sức khỏe.</p>
                    <p>
                        <img src="public/site/images/banner14.webp" alt="tiet to" class="img img-responsive w-100" style="margin: 0 auto" loading="lazy">
                    </p>
                    <p class="font-small">Các nhà khoa học chỉ ra rằng thiếu hụt nội tiết tố nữ Estrogen sẽ khiến nhan sắc, sức khỏe và tinh thần của người phụ nữ đều bị “tàn phá” nghiêm trọng</p>
                    <ul>
                        <li>Da khô hơn, xuất hiện nhiều vết nám sạm, tàn nhang, nếp nhăn, da thiếu đàn hồi.</li>
                        <li>Tóc khô xơ, dễ gãy rụng, vóc dáng kém thon thả, vòng 1 chảy sệ, tăng cân mất kiểm soát.</li>
                        <li>Bốc hỏa, hồi hộp, đổ mồ hôi trộm, mất ngủ, tính tình thất thường, hay cáu gắt...</li>
                        <li>Khô hạn, giảm ham muốn tình dục, khó đạt khoái cảm.</li>
                        <li>Rối loạn kinh nguyệt, viêm nhiễm phụ khoa, nguy cơ cao mắc các bệnh về tim mạch, loãng xương, ung thư vú...</li>
                    </ul>
                    <p class="font-small">Theo các chuyên gia đầu ngành, hiện nay có 2 cách tăng cường nội tiết tố nữ được áp dụng là bổ sung Estrogen thực vật hoặc cung cấp Estrogen hóa tổng hợp. Các loại Estrogen hóa tổng hợp tuy mang lại hiệu quả nhanh nhưng không được khuyến khích bởi nó tiềm ẩn nhiều tác dụng phụ nguy hiểm. Nếu sử dụng quá liều sẽ gây tình trạng thừa cân, tăng nguy cơ ung thư, xuất huyết tử cung,...</p>
                    <p class="font-small">Do đó hiện nay, chị em phụ nữ thường lựa chọn các phương pháp bổ sung, cân bằng nội tiết tố nữ một cách tự nhiên, an toàn, đó là sử dụng Estrogen thực vật. Đây cũng là giải pháp cân bằng nội tiết tố nữ đang được áp dụng rộng rãi tại các nước tiên tiến trên thế giới. Đặc biệt, phụ nữ Nhật Bản thường sử dụng Estrogen thực vật được chiết xuất từ mầm đậu nành (isoflavone).</p>
                </div>
            </div>

<!--            <h5 style="text-align: center;"><strong>HÃY XÓA SỔ NÁM SẠM, NẾP NHĂN BẰNG CÁCH CÂN BẰNG NỘI TIẾT TỐ</strong></h5>-->
<!--            <div class="row">-->
<!--                <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">-->
<!--                    <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</section>

<section>
    <div id="laohoa" class="content-style-2">
        <div class="container">
            <h3 class="font-weight-bold">3. Lão hóa</h3>
            <div class="row">
                <div class="col-sm-12 text-justify">
                    <p class="font-small">Tuổi tác khiến quá trình tái tạo tế bào và sự sản sinh lipid suy giảm. Quá trình chuyển hóa tự nhiên hoặc từ môi trường sinh ra các gốc tự do
                        trong cơ
                        thể. Quá trình này được gọi là sự oxy hóa, đây là nguồn gốc của sự lão hóa.</p>
                </div>
                <p>
                    <img src="public/site/images/banner12.webp" alt="lao hoa" class="img img-responsive" style="margin: 0 auto" loading="lazy">
                </p>
            </div>
            <h4>Với da</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                    <p class="font-small">Da dần trở nên thô ráp, xỉn màu, sạm nám, vết chân chim... Khi khả năng đàn hồi bị hạn chế, làn da có xu hướng dễ bị tổn thương, các mao mạch dễ bị phá vỡ. Vì thế, da nhạy cảm hơn với tia UV, khả năng nhiễm trùng cao, cơ chế tự lành diễn ra chậm,…</p>
                </div>
            </div>
            <h4>Với sức khỏe</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                    <p class="font-small">Giảm trí nhớ, thị lực kém, giảm cơ bắp, mất ngủ, mệt mỏi, suy giảm sức đề kháng, nguy cơ loãng xương cao, đau mỏi khớp,…</p>
                </div>
            </div>
<!--            <h5 style="text-align: center;"><strong>HÃY XÓA SỔ NÁM SẠM, NẾP NHĂN BẰNG CÁCH NGĂN NGỪA LÃO HÓA</strong></h5>-->
<!--            <div class="row">-->
<!--                <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">-->
<!--                    <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</section>

<section>
    <div id="yeutobenngoai" class="content-style-2">
        <div class="container">
            <h3 class="font-weight-bold">4. Yếu tố môi trường bên ngoài</h3>
            <div class="row">
                <div class="col-sm-12 text-justify">
                    <p class="font-small">Tia UV từ ánh nắng mặt trời tồn tại ở hai dạng là UVA và UVB. Trong đó, tia UVB là nguyên nhân gây sản sinh nhiều hắc tố da melanin kháng lại ánh nắng mặt
                        trời. Hắc tố này sẽ gây nên những vết thâm, nám, tàn nhang trên da.</p>
                    <p>
                        <img src="public/site/images/banner13.jpg" alt="moi truong" class="img img-responsive w-100 d-block" style="margin: 0 auto" loading="lazy">
                    </p>
                    <p class="font-small">Tia UV trong ánh nắng mặt trời còn phá hủy tế bào da, làm khô da thậm chí dẫn đến nguy cơ ung thư da. Mặt khác, tia UVA tồn tại ở bất kỳ thời tiết nào, kể cả mùa đông hay mùa hè, trời nhiều mây hay trời nắng. Loại tia này có thể xuyên qua kính, xuyên qua tường, quần áo làm tổn thương đến da.</p>
                    <p class="font-small">Vì vậy, dù làm việc môi trường trong nhà hay ngoài trời các chị em cũng cần lưu ý che chắn và sử dụng kem chống nắng để bảo vệ làn da.</p>
                </div>
            </div>

<!--            <h5 style="text-align: center;"><strong>HÃY XÓA SỐ NÁM SẠM, NẾP NHĂN BẰNG CÁCH TĂNG CƯỜNG MÁU HUYẾT</strong></h5>-->
<!--            <div class="row">-->
<!--                <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">-->
<!--                    <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>-->
<!--                </div>-->
<!--            </div>-->
        </div>
    </div>
</section>
<div id="astaxanthin"></div>
<div id="isoflavone"></div>
<section id="congthucmoi" class="p-a-1">
    <div id="nhom1" class="content-style-2">
        <div class="container-fluid m-b-2">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12">
                    <img src="public/site/images/banner5.webp" alt="img-content" class="img img-responsive d-block" style="margin: 0 auto" loading="lazy">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-justify">
                    <h2 class="text-center">Bí quyết làm đẹp toàn diện từ thiên nhiên <br>Giải quyết tận gốc nám, sạm, nếp nhăn</h2>
                    <p class="font-small">Hiểu rõ nguyên nhân gây nên nám, sạm, tàn nhang và nếp nhăn, Sắc Ngọc Khang ra sức tìm tòi, nghiên cứu, tổng hợp trọn vẹn những Bí quyết làm đẹp từ thiên nhiên để đưa vào trong sản phẩm, giúp bạn khỏe bên trong, đẹp bên ngoài. Những bí quyết này là chính kết quả của sự giao thoa giữa tinh hoa y học cổ truyền và tiến bộ của y học hiện đại, mang lại cho bạn vẻ đẹp toàn diện.</p>
                </div>
            </div>
            <h4 class="font-weight-bold">1. Bí quyết bổ huyết, hoạt huyết với bộ tứ dược liệu Việt Nam từ ngàn xưa</h4>
            <p class="m-t-2">
                <img src="public/site/images/banner15.webp" alt="mauhuyet" class="img img-responsive d-block" style="margin: 0 auto" loading="lazy">
            </p>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                    <p class="font-small"><b><i>Đương Quy, Ngưu Tất, Thục Địa, Ích Mẫu</i></b> là sự kết hợp tuyệt vời, tôn vinh nhan sắc phụ nữ. Dựa theo bài thuốc Tứ Vật Thang
                        bí truyền nổi tiếng của Việt Nam xưa, bộ tứ dược liệu này được coi là <b>Bí quyết ngàn đời gìn giữ nhan sắc phái đẹp Việt.</b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                    <p class="font-small">Với công dụng bổ huyết, hoạt huyết, điều kinh, tăng cường vận chuyển oxy và dưỡng chất, đào thải độc tố, bài thuốc này được xem như
                        <b>“thánh dược của phụ nữ”</b> giúp cơ thể căng tràn sức sống. Bên cạnh việc <b>khỏe từ bên trong</b>, bộ tứ dược liệu này có tác dụng <b>làm đẹp bên ngoài</b>,
                            giúp da hồng hào, khỏe mạnh, đẩy lùi các vết nám, sạm, tàn nhang và chống lão hóa vô cùng hiệu quả.</p>
                    <h5 style="text-align: center;"><strong>BÍ QUYẾT BỔ HUYẾT, HOẠT HUYẾT ĐÃ CÓ TRONG SẮC NGỌC KHANG</strong></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">
                    <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div id="nhom2" class="content-style-2">
        <div class="container">
            <acticle>
                <h4><strong>2. Bí quyết cân bằng nội tiết tố nữ, ức chế sản sinh Melanin từ hoàng cung Nhật Bản</strong></h4>
                <h5 class="font-weight-bold"><i>Isoflavone – chiết xuất mầm đậu nành: Bí quyết làn da không tuổi</i></h5>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                        <p class="font-small">Làn da trắng sáng, mịn màng không tuổi của những người phụ nữ Nhật Bản luôn là điều bí ẩn, khiến các chị em ao ước và ngưỡng mộ. Cách nào và tại sao phụ nữ Nhật Bản lại có làn da trắng hồng, không tì vết và luôn trẻ đẹp đến vậy? Có phải họ đang sở hữu 1 bí quyết làm đẹp quốc truyền nào đó không?</p>
                    </div>
                </div>
                <p>
                    <img src="public/site/images/banner6.webp" alt="mauhuyet" class="img img-responsive" style="margin: 0 auto" loading="lazy">
                </p>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                        <p class="font-small">Theo Viện Dinh Dưỡng Nhật Bản, người Nhật luôn bổ sung đậu nành trong những bữa ăn hằng ngày. Không chỉ là thực phẩm có giá trị dinh dưỡng cao, đậu nành còn là món quà mà tạo hóa ban tặng cho chị em để duy trì tuổi thanh xuân, ngăn ngừa các dấu hiệu lão hóa, và phòng ngừa nhiều bệnh nguy hiểm.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                        <p class="font-small">Lý do là trong đậu nành có chứa thành phần Isoflavone – một dạng Estrogen thực vật. Khi hàm lượng nội tiết tố nữ trong cơ thể thấp, Isoflavone bù đắp sự thiếu hụt estrogen, ngược lại khi nội tiết tố nữ trong cơ thể cao, Isoflavone ngăn cản estrogen hấp thu vào các thụ thể. Nhờ đó Isoflavone giúp cân bằng nội tiết tố nữ, đẩy lùi các triệu chứng tiền mãn kinh, cải thiện các vấn đề nám, sạm, tàn nhang,…</p>
                    </div>
                </div>
                <h5 class="font-weight-bold"><i>L-cystine – chiết xuất từ nhung hươu, nai: Bí quyết vẻ đẹp Hàn Quốc</i></h5>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                        <p class="font-small">Chất này đóng vai trò quan trọng trong cấu tạo protein, ức chế sản sinh Melanin, đẩy mạnh quá trình đào thải độc tố, tác động toàn
                            diện, mang lại vẻ đẹp hoàn hảo.
                        </p>
                    </div>
                </div>
                <ul>
                    <li>Đối với tóc: L-cystine giảm rụng tóc, làm vững chân tóc nhờ khả năng tăng cường sản sinh keratin.</li>
                    <li>Đối với da, L-cystine có tác dụng thúc đẩy quá trình tổng hợp collagen (protein nâng đỡ da).</li>
                    <li>Làm trắng da nhờ ức chế hình thành melanin.</li>
                    <li>Kích thích quá trình tái tạo da, giảm thâm sạm do sử dụng mỹ phẩm, hóa chất.</li>
                    <li>Điều hòa bài tiết chất nhờn, ngăn ngừa mụn.</li>
                </ul>
                <h5 style="text-align: center;"><strong>BÍ QUYẾT CÂN BẰNG NỘI TIẾT TỐ NỮ ĐÃ CÓ TRONG SẮC NGỌC KHANG</strong></h5>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">
                        <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>
                    </div>
                </div>
            </acticle>

            <acticle>
                <h4><strong>3. Bí quyết chống oxy hóa, bảo vệ da khỏi tia UV và gốc tự do, tuyệt chiêu ngăn chặn lão hóa từ bên trong</strong></h4>
                <h5 class="font-weight-bold"><i>Astaxanthin - chiết xuất Vi tảo lục Nhật Bản: Phép nhiệm màu của tự nhiên</i></h5>
                <p>
                    <img src="public/site/images/banner7.webp" alt="mauhuyet" class="img img-responsive" style="margin: 0 auto" loading="lazy">
                </p>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                        <p class="font-small">Nước Nhật luôn giữ kỷ lục về số lượng những người sống thọ nhất thế giới. Làng Ogimi (phía Bắc đảo Okinawa, Nhật Bản) còn được mệnh danh là “vùng đất người già” của thế giới bởi tuổi thọ kỷ lục (cứ 100 người thì có 11 người thọ trên 100 tuổi, 1/3 dân cư còn lại đang ở độ tuổi trên 65 tuổi). Có được tuổi thọ ấn tượng như vậy là vì thực phẩm mà người dân ở ngôi làng này sử dụng hàng ngày là tôm, rong biển, cá hồi và vi tảo lục. Những thực phẩm từ thiên nhiên này được các nhà khoa học chứng minh là có công dụng làm đẹp diệu kỳ với thành phần chính là hoạt chất Astaxanthin. Đặc biệt, trong vi tảo lục có hàm lượng Astaxanthin vượt trội hơn cả.
                        </p>
                        <p class="font-weight-bold font-small">Năng lực diệu kỳ của Astaxanthin</p>
                        <ul>
                            <li><i>Vua của các chất chống oxy hóa:</i> Khả năng chống lão hóa da gấp 110 lần so với vitamin E, gấp 6.000 lần so với vitamin C, gấp 800 lần so với CoQ10 toàn năng.
                            </li>
                            <li><i>Loại bỏ gốc tự do trong cơ thể:</i> Astaxanthin như một “bức tường chắn” vô hiệu hóa các gốc tự do, bảo vệ tế bào từ trong ra ngoài.</li>
                            <li><i>Chống tia UV:</i> Với khả năng vô hiệu hóa hoặc phá hủy các gốc tự do nên Astaxanthin có thể bảo vệ da khỏi tác hại của tia cực tím. Từ đó, hoạt chất này giúp
                                ngăn chặn sự hấp thu ánh sáng mặt trời vào làn da, đẩy lùi những tác hại như: cháy nắng, nám, sạm, tàn nhang.</li>
                            </li>
                            <li><i>Bảo vệ thị lực:</i> Astaxanthin là chất carotenoid duy nhất có thể hấp thụ vào đến võng mạc mắt và có khả năng cải thiện thị lực, ngăn ngừa, hỗ trợ điều trị hàng
                                loạt chứng bệnh về mắt: hoa mắt, quáng gà,…
                            </li>
                            <li><i>Phục hồi, tăng cường sức khỏe:</i> Với cấu trúc phân tử và cơ chế hoạt động độc đáo, Astaxanthin giúp tăng cường hoạt động của cơ bắp, đẩy mạnh quá trình trao đổi chất; tăng sức
                                chịu đựng và nâng cao sức đề kháng.
                            </li>
                        </ul>
                        <h5 style="text-align: center;"><strong>BÍ QUYẾT CHỐNG OXY HÓA ƯU VIỆT TỪ ASTAXANTHIN ĐÃ CÓ TRONG <br>SẮC NGỌC KHANG</strong></h5>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">
                                <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>
                            </div>
                        </div>
                        <p>
                            <img src="public/site/images/chungnhan.webp" alt="chung nhan" class="img img-responsive w-100" style="margin: 0 auto" loading="lazy">
                        </p>
                        <p class="font-small text-center" style="margin-top: -20px"><i>Chứng nhận Astaxanthin nhập khẩu từ Nhật Bản được nghiên cứu và phát triển bởi AstaReal</i></p>
                        <div class="info-company">
                            <p class="font-small text-justify"><b>AstaReal – Công ty hàng đầu thế giới nghiên cứu về lĩnh vực chống lão hóa</b></p>
                            <p class="font-small text-justify">AstaReal Co., Ltd. trực thuộc tập đoàn AstaReal Group, được sở hữu bởi Fuji Chemical Group của Nhật Bản – <b>Nhà sản xuất dược phẩm hàng đầu thế giới</b> thành
                                lập từ năm 1946. Đây là thương hiệu toàn cầu về các sản phẩm Astaxanthin có nguồn gốc tự nhiên và đã được FDA Hoa Kỳ chứng nhận GRAS (Chứng nhận <b>tuyệt đối an toàn</b>).
                            </p>
                            <p class="font-small text-justify">Năm 1994, <b>lần đầu tiên trên thế giới, Công ty AstaReal đã thành công trong việc sản xuất Astaxanthin từ Vi tảo lục</b>. Từ đó đến nay, AstaReal liên tục dẫn đầu trong những nghiên cứu khoa học lâm sàng về Astaxanthin,
                                mang lại công thức chống oxy hóa ngày càng vượt trội, lưu giữ nét thanh xuân cho phái đẹp trên toàn thế giới.
                            </p>
                        </div>
                    </div>
                </div>

<!--                <h5 class="font-weight-bold"><i>Dầu gấc:</i></h5>-->
<!--                <p class="m-t-2">-->
<!--                    <img src="public/site/images/quagat.jpg" alt="mauhuyet" class="img img-responsive w-100" style="margin: 0 auto">-->
<!--                </p>-->
<!--                <div class="row">-->
<!--                    <div class="col-xs-12 col-sm-12 col-md-12 text-justify">-->
<!--                        <p class="font-small">Lại quả này chứa Lycopen, Beta carotene – tiền chất Vitamin A, Vitamin E, acid béo omega 3,6,9 với những tác dụng: chữa nám, sạm, tàn-->
<!--                            nhang; giúp da căng mụn và ngăn ngừa lão hóa.-->
<!--                        </p>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">
                        <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>
                    </div>
                </div>
            </acticle>

        </div>
    </div>
</section>

<section>
    <div id="timeline">
        <div class="container-fluid">
            <img src="public/site/images/banner_final_cua_final.webp" alt="background" class="img img-fluid w-100" loading="lazy">
        </div>
        <div class="container m-t-2">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="text-uppercase text-center" style="color: rgb(215, 18, 73)!important;">VIÊN UỐNG SẮC NGỌC KHANG </h2>
                    <h5 class="text-uppercase text-center">TỔNG HỢP TINH HOA THIÊN NHIÊN, <br>GIẢI QUYẾT TẬN GỐC NÁM, SẠM, TÀN NHANG, TRẢ LẠI LÀN DA XUÂN SẮC </h5>
                    <div class="text-justify">
                        <p class="font-small">
                            Viên Uống Sắc Ngọc Khang là sản phẩm đầu tiên trên thị trường với công thức độc đáo, giải quyết tận gốc các vấn đề <b>suy giảm về sắc đẹp, sinh lý và sức khỏe</b>. Sản phẩm được hàng triệu phụ nữ
                            Việt Nam lựa chọn như “người bạn đồng hành” nuôi dưỡng làn da từ sâu bên trong, giúp họ tìm lại vẻ đẹp xuân thì. Với kết hợp nhiều loại thảo dược
                            thiên nhiên quý, Viên Uống Sắc Ngọc Khang chắc chắn sẽ là sự lựa chọn hoàn hảo giúp bạn <b>đánh thức làn da đẹp</b> đang “ẩn mình” cùng <b>thể trạng khỏe mạnh, tinh thần sảng khoái</b>.
                        </p>
                        <p>
                            <img src="public/site/images/quytrinh.webp" alt="lieutrinh" class="img img-responsive w-100" style="margin: 0 auto" loading="lazy">
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>