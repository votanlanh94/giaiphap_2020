<div id="cuahang" class="m-b-3 m-t-3">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="text-uppercase font-weight-bold text-center">Cửa hàng liên kết trực tuyến</h3>
            </div>
        </div>
        <div class="box-store">
            <div class="pd-store">
                <a href="https://bit.ly/2WeozgV" target="_blank"><img src="public/site/images/tiki.jpg" alt="tiki" class="img img-fluid m-t-1 hvr-float" loading="lazy"></a>
            </div>
            <div class="pd-store">
                <a href="https://bit.ly/2XdgAAQ" target="_blank"><img src="public/site/images/shopee.jpg" alt="shoppee" class="img img-fluid m-t-1 hvr-float" loading="lazy"></a>
            </div>
            <div class="pd-store">
                <a href="https://bit.ly/305TI8Q" target="_blank"><img src="public/site/images/sendo.jpg" alt="sendo" class="img img-fluid m-t-1 hvr-float" loading="lazy"></a>
            </div>
            <div class="pd-store">
                <a href="https://bit.ly/37ZaGaL" target="_blank"> <img src="public/site/images/lazada.jpg" alt="lazada" class="img img-fluid m-t-1 hvr-float" loading="lazy"></a>
            </div>
            <div class="pd-store">
                <img src="public/site/images/grab.jpg" alt="grab" class="img img-fluid m-t-1 hvr-float" loading="lazy">
            </div>
        </div>
    </div>
</div>