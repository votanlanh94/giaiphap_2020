<div id="nam" class="content-style-2 p-t-3" style="background-image: url(public/site/images/bg_header.png); background-repeat: no-repeat">
    <div class="container">
        <div class="row congthucchitiet">
            <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                <h2 class="text-center">Nám, sạm, tàn nhang <br> Nơi lưu dấu thiên chức của người phụ nữ</h2>
                <p class="font-small">Sứ mệnh thiên bẩm của người phụ nữ Việt Nam là cố gắng chu toàn mọi công việc: từ chăm sóc mái ấm hạnh phúc gia đình; đến nuôi dưỡng con cái và hoàn thành tốt những công việc khác. Tất cả những điều ấy được đánh đổi bằng cả thanh xuân của họ - những người phụ nữ tuyệt vời.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                <p class="font-small">Cho dù là công việc nội trợ, đồng áng, sản xuất, buôn bán hay văn phòng thì phụ nữ Việt đều có thể đảm đương. Vào những năm tháng trẻ trung, rực rỡ nhất, bất kể nắng gắt hay mưa phùn cũng không khiến bước chân của những người vợ, người mẹ nao núng. Nhưng khi bước qua tuổi 30, những đêm mất ngủ, những cơn đau bắt đầu kéo đến quấy rầy họ. Tóc rụng nhiều hơn, da trở nên khô hơn và rõ nét nhất là những vết nám sạm, nếp nhăn dần hiện rõ trên gương mặt khiến phụ nữ mới giật mình nhận ra mình sắp già!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                <p class="font-small">Mấy ai biết được “VẾT NÁM, SẠM, TÀN NHANG LÀ NƠI LƯU DẤU THIÊN CHỨC CỦA NGƯỜI PHỤ NỮ”, là minh chứng cao quý cho những năm tháng tảo tần vì gia đình! Nhưng cũng chính những vết dấu không đẹp đẽ này khiến những người vợ, người mẹ mất dần tự tin vì ngoại hình. Đây chính là dấu hiệu cảnh báo cho sự sa sút về sức khỏe từ bên trong của người phụ nữ.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-justify">
                <p class="font-small">Phụ nữ ơi! Đừng để đặc quyền này trở thành áp lực vô hình khiến chúng ta quên đi việc chăm sóc bản thân nhé. Không ai có thể thay thế được
                    vai trò thiêng liêng của người phụ nữ trong gia đình, vì thế, hãy chăm sóc cho sức khỏe và sắc đẹp của mình đúng cách để giữ cho ngôi nhà luôn ấm áp, vui tươi, phụ nữ nhé.</p>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <div class="col-xs-12 col-sm-12 col-md-12">-->
<!--                <img src="public/site/images/banner3.jpg" alt="img-content"-->
<!--                     class="img img-fluid hvr-bubble-float-bottom">-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-xs-12 col-sm-12 col-md-12 m-t-2 m-b-2">-->
<!--                <button data-toggle="modal" data-target="#callme-modal" class="btn btn-danger btn-advice d-block" style="margin: 0 auto">Tư Vấn Cho Tôi</button>-->
<!--            </div>-->
<!--        </div>-->

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-xs-12">
                <img src="public/site/images/banner4.webp" alt="img-content" class="img img-responsive w-100" loading="lazy">
            </div>
        </div>
    </div>
</div>

