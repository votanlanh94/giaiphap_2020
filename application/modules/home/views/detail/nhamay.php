<div id="nhamayhiendai">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
                <h5 class="text-uppercase font-weight-bold text-center">Sản xuất trên dây chuyền theo chuẩn quốc tế</h5>
                <p class="text-justify">Viên Uống Sắc Ngọc Khang được sản xuất tại nhà máy Hoa Sen với 20 năm kinh nghiệm trong việc nghiên
                    cứu và sản xuất dược phẩm. Nhà máy được xây dựng theo tiêu chuẩn quốc tế, trang thiết bị hiện đại và
                    đạt chuẩn GMP được cấp bởi Bộ Y Tế. Nhờ đó, chất lượng của Viên Uống Sắc Ngọc Khang đáp ứng đầy đủ
                    tiêu chuẩn khắt khe của Bộ Y Tế.</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                        <img src="public/site/images/nha-may-1.png" alt="nha may 1" class="img transform-style-1"
                             id="img-nha-may-1" loading="lazy">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                        <img src="public/site/images/nha-may-2.png" alt="nha may 2"
                             class="img img-fluid img-nha-may-2 transform-style-1" id="img-nha-may-2" loading="lazy">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 pos-relative p-a-0" style="display: grid">
                <div class="youtube" data-embed="4zUjptjYWtc">
                    <div class="play-button"></div>
                </div>
            </div>
        </div>
    </div>
</div>