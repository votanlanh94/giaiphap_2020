<div id="thaydoi" class="content-style-2 m-t-3 m-b-2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h1 class="text-uppercase font-weight-bold m-b-2 header-move text-center"><span>30 tuổi và những thay đổi </span>
                    <span class="pri-text txt-snk"><i class="fa fa-quote-left"></i> đáng sợ <i
                                class="fa fa-quote-right"></i></span>
                </h1>
            </div>
        </div>
        <div class="row padding-20 ">
            <div class="pri-border">
                <div class="col-md-12 text-center hvr-wobble-horizontal">
                    <p class="txt-dotpha padding-20 font-20 text-justify-mobile"><span class="pri-text txt-snk"></span><span
                                class="txt-snk">Ở độ tuổi 30, bên cạnh nám – sạm – tàn nhang, làn da và cơ thể người phụ nữ bắt đầu có những thay đổi đáng kể. Đây là mốc thời gian khởi đầu cho thời kỳ “mất dần thanh xuân” khi mà các </span>
                        <span class="pri-text txt-snk"> cơ quan có dấu hiệu xuống cấp và bắt đầu lão hóa </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row mg-20 daxau">
            <div class="col-xs-12 col-sm-8 col-md-9 p-a-0">
                <div class="row shadow">
                    <div class="col-xs-12 col-sm-6 col-md-3 p-a-0-5">
                        <img src="public/site/images/da_xau.jpg"
                             alt="Da xau" class="img img-fluid resize-img mb-left" style="border-radius: 50%">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8 align-mid">
                        <h3 class="pri-text">Da</h3>
                        <p>Nám, sạm, tàn nhang; nếp nhăn khô; vết chân chim; quầng thâm mắt; da sần sùi, thiếu sức sống.
                        </p>
                    </div>
                </div>
                <div class="row shadow">
                    <div class="col-xs-12 col-sm-6 col-md-3 p-a-0-5">
                        <img src="public/site/images/tam_ly.jpg"
                             alt="Tam ly tuoi 30" class="img img-fluid resize-img mb-left" style="border-radius: 50%; border: 1px solid #777; background:#b3b3b3">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8 align-mid">
                        <h3 class="pri-text">Tâm lý</h3>
                        <p>Nóng tính hay cáu bẳn, ít nói, trầm tính, căng thẳng, lo âu, mệt mỏi, phát hỏa trong người.
                        </p>
                    </div>
                </div>
                <div class="row shadow">
                    <div class="col-xs-12 col-sm-6 col-md-3 p-a-0-5">
                        <img src="public/site/images/sinh_ly.jpg"
                             alt="Sinh ly tuoi 30" class="img img-fluid resize-img mb-left" style="border-radius: 50%">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8 align-mid">
                        <h3 class="pri-text">Sinh lý</h3>
                        <p>Bốc hỏa, khó ngủ, đau đầu, giảm trí nhớ, khô âm đạo, giảm ham muốn, kinh nguyệt thất thường.
                        </p>
                    </div>
                </div>
                <div class="row shadow">
                    <div class="col-xs-12 col-sm-6 col-md-3 p-a-0-5">
                        <img src="public/site/images/the_chat.jpg"
                             alt="The chat" class="img img-fluid resize-img mb-left" style="border-radius: 50%">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8 align-mid">
                        <h3 class="pri-text">Thể chất</h3>
                        <p>Loãng xương, tích mỡ, dễ mắc bệnh tim mạch, huyết áp, tiểu đường, ung thư...
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3" style="margin: auto">
                <img src="public/site/images/img_vienuong.webp" alt="vienuong image"
                     class="img img-fluid m-l-2 transform-style-1" id="img-serum">
                <button onclick="add_to_cart_and_redirect(this,'add_cart',1,1, 'menu')" class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" style="display: block;
    margin: 0 auto;">đặt hàng ngay</button>
            </div>
        </div>
    </div>
    <div id="astaxanthin"></div>
    <div id="isoflavone"></div>
    <div id="dotpha"></div>
    <div class="container-fluid">
        <div class="section">
            <div class="row pri-background m-b-3">
                <div class="col-md-12 info-product" style="padding: 10px; text-align: justify">
                    <p class="txt-white text-justify">Là phụ nữ, ai cũng mong muốn bản thân xinh đẹp và có thể duy trì sự tươi trẻ.
                        Hiểu được điều đó, Sắc Ngọc Khang mang đến cho bạn giải pháp toàn diện về bí quyết dưỡng da và chăm sóc sức
                        khỏe tuổi 30 từ sâu bên trong để làm chậm quá trình lão hóa.
                    </p>
                    <p class="txt-white text-justify">Chúng ta có xu hướng tìm về cội nguồn thiên nhiên trong việc làm đẹp và chăm
                        sóc sức khỏe. Kết hợp với nhiều loại thảo dược thiên nhiên quý, Viên Uống Sắc Ngọc Khang chắc chắn sẽ là sự lựa chọn hoàn hảo giúp bạn đánh thức làn da đẹp đang “ẩn mình” cùng một thể
                        trạng khỏe mạnh, một tinh thần sảng khoái.
                    </p>
                </div>
            </div>
            <img src="public/site/images/bn-gioithieu.jpg" alt="background" class="img img-fluid w-100">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="offer offer-snk">
                        <div class="shape">
                            <div class="shape-text">
                                Nhóm 1
                            </div>
                        </div>
                        <div class="offer-content m-a-2">
                            <p class="pri-text">
                                Nhóm 1 giúp bổ huyết, hoạt huyết, điều kinh, tăng cường vận chuyển oxy và dưỡng chất giúp da hồng hào, khỏe mạnh
                            </p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapse3" role="button"
                               aria-expanded="false" aria-controls="collapse3">Xem chi tiết</a>
                            <div class="collapse" id="collapse3">
                                <div class="card card-body">
                                    <ul>
                                        <li>Thục địa vị ngọt tính ôn, vào kinh tâm, can, thận, tác dụng bổ thận, làm đen
                                            tóc, chữa huyết hư, kinh nguyệt không đều.
                                        </li>
                                        <li>Ngưu tất có tác dụng phá huyết, hành ứ, bổ can thận, mạnh gân cốt. Thường dùng
                                            chữa viêm khớp, kinh nguyệt khó khăn, đau bụng kinh
                                        </li>
                                        <li>Đương quy vào 3 kinh tâm, can, tỳ, tác dụng bổ huyết, hoạt huyết, điều huyết,
                                            thông kinh. Thuốc đầu vị trong chữa bệnh phụ nữ thường dùng trong Đông y
                                        </li>
                                        <li>Ích mẫu có khả năng trục ứ huyết, sinh huyết mới, bổ huyết, dùng trong bệnh về tuần hoàn cơ tim</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="m-a-2" style="display: flex; justify-content: center">
                            <img src="public/site/images/nhom1.jpg" alt="nhom1" style="border-radius: 50%; max-width: 50%">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="offer offer-snk">
                        <div class="shape">
                            <div class="shape-text">
                                Nhóm 2
                            </div>
                        </div>
                        <div class="offer-content m-a-2">
                            <p class="pri-text">
                                Nhóm 2 chứa chất chống oxy hóa, bảo vệ da khỏi tia UV và gốc tự do, ngăn chặn lão hóa da
                            </p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapse1" role="button"
                               aria-expanded="false" aria-controls="collapse1">Xem chi tiết</a>
                            <div class="collapse" id="collapse1">
                                <div class="card card-body">
                                    <p>Astaxanthin chiết xuất từ vi tảo lục Nhật bản:</p>
                                    <ul>
                                        <li>Chất chống oxy hóa mạnh từ thiên nhiên: Khả năng chống lão hóa da gấp 110 lần so
                                            với
                                            vitamin E, gấp 6.000 lần so với vitamin C, gấp 800 lần so với CoQ10 toàn năng.
                                        </li>
                                        <li>Loại bỏ gốc tự do trong cơ thể</li>
                                        <li>Chống nắng, chống nám, sạm và tàn nhang</li>
                                        <li>Bảo vệ thị lực</li>
                                        <li>Phục hồi, tăng cường sức khỏe</li>
                                        <li>Giảm nguy cơ bị ung thư da</li>
                                    </ul>
                                    <p>Dầu gấc chứa hàm lượng cao Beta carotene – tiền chất Vitamin A, Vitamin E, lycopen,
                                        acid béo omega 3,6,9.</p>
                                    <ul>
                                        <li>Lycopen là chất chống oxy hóa và chống lão hóa mạnh mà cơ thể không tự tổng hợp
                                            được, có tác dụng khử các gốc tự do, bảo vệ màng tế bào ngăn quá trình lão hóa
                                            da, sạm da, sần sùi
                                        </li>
                                        <li>Vitamin A tăng tái tạo da giúp da trắng, mịn màng.</li>
                                        <li>Các acid béo trong dầu gấc giúp nuôi dưỡng da khỏe đẹp</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="m-a-2" style="display: flex; justify-content: center">
                            <img src="public/site/images/nhom2.jpg" alt="nhom1" style="border-radius: 50%; max-width: 50%">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="offer offer-radius offer-snk">
                        <div class="shape">
                            <div class="shape-text">
                                Nhóm 3
                            </div>
                        </div>
                        <div class="offer-content m-a-2">
                            <p class="pri-text">
                                Nhóm 3 giúp cân bằng nội tiết tố nữ, ức chế sản sinh Melanin, bảo vệ da từ bên trong
                            </p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapse2" role="button"
                               aria-expanded="false" aria-controls="collapse2">Xem chi tiết</a>
                            <div class="collapse" id="collapse2">
                                <div class="card card-body">
                                    <p>Isoflavone – chiết xuất mầm đậu tương: cân bằng nội tiết tố nữ</p>
                                    <ul>
                                        <li>Khi hàm lượng nội tiết tố nữ trong cơ thể thấp, isoflavone bù đắp sự thiếu hụt
                                            estrogen. Ngược lại, khi nội tiết tố nữ trong cơ thể cao, isoflavone ngăn cản
                                            estrogen hấp thu vào các thụ thể. Nhờ đó Isoflavone giúp estrogen trong cơ thể
                                            luôn ở trạng thái cân bằng, cải thiện các triệu chứng tiền mãn kinh. Bổ sung nội
                                            tiết tố sớm giúp cải thiện các vấn đề nám da do thiếu hụt nội tiết.
                                        </li>
                                    </ul>
                                    <p>L-cystine – chiết xuất từ nhung hươu: Đóng vai trò quan trọng trong cấu tạo protein,
                                        ức chế sản sinh Melanin, tăng đào thải độc tố</p>
                                    <ul>
                                        <li>Đối với tóc: L-cystine giảm rụng tóc, làm vững chân tóc nhờ khả năng tăng tạo
                                            keratin.
                                        </li>
                                        <li>Đối với da, L-cystine có tác dụng tăng tổng hợp collagen (protein nâng đỡ da)
                                            <ul>
                                                <li>Làm trắng da nhờ ức chế hình thành melanin</li>
                                                <li>Đào thải các tế bào da bị tổn thương</li>
                                                <li>Tăng tái tạo da mới, giảm thâm sạm do sử dụng mỹ phẩm, hóa chất</li>
                                                <li>Điều hòa bài tiết chất nhờn, ngăn ngừa mụn</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="m-a-2" style="display: flex; justify-content: center">
                            <img src="public/site/images/nhom3.jpg" alt="nhom1" style="border-radius: 50%; max-width: 50%">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>