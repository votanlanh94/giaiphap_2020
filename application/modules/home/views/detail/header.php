<header class="header">
    <!-- End top-header-->
    <div class="main-head">
        <div class="container">
            <div class="row">
                <div class="col-xs-5 col-sm-4 col-md-2 text-center bg-logo" style="padding-left: 0px">
                    <a href="/">
                        <img src="public/site/images/logo-sacngockhang.png" style="max-width:150px;margin:10px auto" class="img-responsive"/>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-10" style="padding: 0px">
                    <label for="openmenu" class="hidden-md hidden-lg"><span class="ion-android-menu"></span></label>
                    <input type="checkbox" id="openmenu" class="hidden">
                    <ul id="main-menu">
<!--                        <li><a onclick="scrollto(this,'sieukhuyenmai')" id="nav-sieukhuyenmai" class="current">Ưu đãi <span>sốc</span></a></li>-->
                        <li><a onclick="scrollto(this,'nam')" id="nav-biquyet">Nguyên <span>nhân</span></a></li>
                        <li><a onclick="scrollto(this,'congthucmoi')" id="nav-congthucmoi">Công thức <span>đột phá</span></a></li>
                        <li><a onclick="scrollto(this,'nhamayhiendai')" id="nav-nhamay">Nhà máy <span> GMP</span></a></li>
                        <li><a onclick="scrollto(this,'danhhieu')" id="nav-danhhieu">Thương hiệu <span>danh giá</span></a></li>
                        <li><a onclick="scrollto(this,'hoahau')" id="nav-hoahau">Đại sứ <span>thương hiệu</span></a></li>
                        <li><a onclick="scrollto(this,'trainghiem')" id="nav-trainghiem">Câu hỏi <span>thường gặp</span></a></li>
                        <li><a onclick="scrollto(this,'khuyenmai-remain')" id="nav-khuyenmai">Khuyến mãi <span>Hôm nay</span></a></li>
                        <li>
                            <button onclick="add_to_cart_and_redirect(this,'add_cart',<?= $product_id_of_page ?>,1, 'menu')" class="btn btn-danger btn-menu">đặt ngay</button>
                            <button class="btn btn-secondary btn-menu btn-call-sm"><i class="glyphicon glyphicon-earphone" onclick="location.href='tel:19006033';"></i> 19006033
                            </button>
                        </li>
<!--                        <li>-->
<!--                            <button class="btn btn-secondary btn-menu btn-call-lg" onclick="location.href='tel:19006033';"><i class="glyphicon glyphicon-earphone"></i> 19006033-->
<!--                            </button>-->
<!--                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End main-head-->
</header>