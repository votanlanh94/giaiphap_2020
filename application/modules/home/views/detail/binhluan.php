<div id="binhluan" class="content-box content-style-8">
    <div class="container">
        <div class="content-box-heading">
            <h3 class="content-box-title">
                <b>Câu hỏi thường gặp về</b><span>Serum Sắc Ngọc Khang</span>
            </h3>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-xs-12 1">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-1">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                                   aria-controls="collapseOne">
                                                    1. Serum là gì?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-1">
                                            <div class="panel-body">
                                                Serum là sản phẩm chăm sóc da chuyên sâu. Các dưỡng chất trong Serum nhiều gấp 10 lần các loại kem dưỡng, với khả năng dẫn
                                                sâu,
                                                thấm
                                                nhanh vào 3 lớp biểu bì, thân bì và hạ bì để điều trị và nuôi dưỡng da từ gốc, mang lại tác dụng nhanh chóng và hiệu quả
                                                hơn.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-2">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true"
                                                   aria-controls="collapseTwo">
                                                    2. Loại Serum được sử dụng phổ biến nhất là gì?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-2">
                                            <div class="panel-body">
                                                Serum có nhiều loại để khắc phục từng nhược điểm khác nhau của da, và loại serum được sử dụng phổ biến nhất là serum chứa
                                                nhiều
                                                Vitamin C giúp làm sáng da, kích thích sản xuất collagen, cải thiện cấu trúc da, phục hồi làn da rạng rỡ và chống lại nếp
                                                nhăn.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-3">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true"
                                                   aria-controls="collapseThree">
                                                    3. Độ tuổi nào sử dụng Serum dưỡng da?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-3">
                                            <div class="panel-body">
                                                Serum dưỡng da có thể được dùng ở bất kỳ độ tuổi nào, tùy theo từng nhu cầu cần điều trị của da như: thâm sạm, lão hóa, mụn…
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-4">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true"
                                                   aria-controls="collapseFour">
                                                    4. Serum Sắc Ngọc Khang có tác dụng như thế nào?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-4">
                                            <div class="panel-body">
                                                Serum Sắc Ngọc Khang có tác dụng dưỡng da trắng khỏe, tươi sáng, đều màu; làm mờ những vùng da thâm sạm, xỉn màu, đồng thời,
                                                tăng độ
                                                săn chắc và đàn hồi cho da.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-5">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true"
                                                   aria-controls="collapseFive">
                                                    5. Serum Sắc Ngọc Khang có những thành phần gì?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-5">
                                            <div class="panel-body"> Serum Sắc Ngọc Khang có công thức đột phá với nguồn nguyên liệu nhập khẩu từ Nhật Bản và châu Âu:
                                                <ul>
                                                    <li><p>[Hoạt chất VC-IPTM]* ức chế melanin, kích hoạt sắc da trắng khỏe, đều màu, chống oxy hóa và phục hồi tổn thương
                                                            do
                                                            môi
                                                            trường.</p></li>
                                                    <li><p>[Squalane] dưỡng ẩm, cân bằng độ ẩm và dầu, ngăn chặn tác hại của tia UV</p></li>
                                                    <li><p>[Dầu Jojoba] kháng khuẩn, kháng viêm, khôi phục & tái tạo kết cấu da, giúp da mịn màng căng mọng.</p></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-xs-12 6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-6">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true"
                                                   aria-controls="collapseSix">
                                                    6. Điểm khác biệt của Serum dưỡng trắng da Sắc Ngọc Khang?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-6">
                                            <div class="panel-body">
                                                <ul>
                                                    <li><p>Sản phẩm của Công ty Cổ phần Đầu tư Dược phẩm HTP</p></li>
                                                    <li><p>Top thương hiệu Dược Mỹ Phẩm hàng đầu Việt Nam.</p></li>
                                                    <li><p>Được cố vấn bởi đội ngũ chuyên gia, dược sỹ nhiều kinh nghiệm.</p></li>
                                                    <li><p> Sản xuất trên dây chuyền đạt chuẩn quốc tế CGMP-ASEAN. </p></li>
                                                    <li><p>Thương hiệu dành được nhiều danh hiệu, giải thưởng uy tín.</p></li>
                                                    <li><p>Sản phẩm Serum Sắc Ngọc Khang thích hợp với mọi làn da.</p></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 7">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-7">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true"
                                                   aria-controls="collapseSeven">
                                                    7. Serum Sắc Ngọc Khang có đảm bảo độ an toàn không?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-7">
                                            <div class="panel-body">
                                                <p>Serum Sắc Ngọc Khang đảm bảo độ an toàn tối ưu:</p>
                                                <ul>
                                                    <li>
                                                        <p>Không gây kích ứng.</p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            Không chứa chất bảo quản.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            Không nhờn rít.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>
                                                            Phù hợp với mọi loại da, kể cả da nhạy cảm.
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 8">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-8">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true"
                                                   aria-controls="collapseEight">
                                                    8. Cách sử dụng Serum Sắc Ngọc Khang như thế nào?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-8">
                                            <div class="panel-body">
                                                <ul>
                                                    <li>
                                                        <p>
                                                            Làm sạch da, lấy lượng sản phẩm vừa đủ, thoa đều lên mặt và massage nhẹ nhàng.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>Thoa tinh chất mỗi sáng và tối trước khi sử dụng kem dưỡng da Sắc Ngọc Khang.</p>
                                                    </li>
                                                    <li>
                                                        <p>Ban ngày nên kết hợp sử dụng thêm kem chống nắng Sắc Ngọc Khang SPF 50 PA++++ để bảo vệ da.</p>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 9">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-9">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="true"
                                                   aria-controls="collapseNine">
                                                    9. Nên chăm sóc da thế nào khi sử dụng Serum Sắc Ngọc Khang?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-9">
                                            <div class="panel-body">
                                                Các chuyên gia của Sắc Ngọc Khang tư vấn cách sử dụng Serum theo quy trình sau:
                                                <ul>
                                                    <li>
                                                        <p>Bước 1: sử dụng Sữa rửa mặt Sắc Ngọc Khang làm sạch dịu nhẹ cho da.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>Bước 2: dùng Nước hoa hồng Sắc Ngọc Khang để làm sạch sâu, se khít lỗ chân lông, cân bằng độ ẩm cho da, giúp các
                                                            bước
                                                            chăm sóc da tiếp theo được hấp thụ tốt nhất để phát huy hiệu quả.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>Bước 3: sử dụng Serum Sắc Ngọc Khang để dưỡng chất dẫn sâu, thấm nhanh, phục hồi hoàn hảo cho làn da sáng khỏe,
                                                            đểu
                                                            màu
                                                            và rạng rỡ đến diệu kỳ.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>Bước 4: dùng Kem dưỡng da Sắc Ngọc Khang với tinh chất cô đặc dưỡng sáng đều màu, khóa ẩm, tái tạo da.
                                                        </p>
                                                    </li>
                                                    <li>
                                                        <p>Bước 5: ban ngày nên kết hợp thêm Kem chống nắng Sắc Ngọc Khang SPF 50 PA++++ để bảo vệ da.
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 10">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="number-10">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="true"
                                                   aria-controls="collapseTen">
                                                    10. Có nên dùng thêm kem chống nắng sau khi sử dụng Serum?
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="number-10">
                                            <div class="panel-body">
                                                Ban ngày, bạn nên kết hợp sử dụng thêm Kem chống nắng Sắc Ngọc Khang SPF 50 PA++++ để bảo vệ làn da khỏi tia cực tím và các
                                                tác
                                                nhân
                                                gây hại.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="text-xs-center"><a href="" data-toggle="modal" data-target="#comment-modal" data-dismiss="modal" title="Gửi bình luận của bạn"
                                               class="btn btn-primary btn-lg"><i class="snk snk-right-big"> </i><b>Gửi câu hỏi của bạn</b></a></div>
            </div>
        </div>
    </div>
</div>

<div id="order-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade"></div>
<div id="comment-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h5 class="modal-title"><span>Gửi bình luận</span></h5>
            </div>
            <div class="modal-body">
                <form id="form-comment">
                    <div class="row">
                        <div class="col-xs-6 form-group">
                            <input placeholder="Họ tên" type="text" class="form-control" name="Name">
                        </div>
                        <div class="col-xs-6 form-group">
                            <input placeholder="Số điện thoại" type="text" name="Phone" class="form-control">
                        </div>
                        <div class="col-xs-12 form-group">
                            <textarea placeholder="Nội dung bình luận..." rows="3" name="Comments" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row hidden" id="box-alert-comment" style="margin-top:10px;">
                        <div class="col-xs-12">
                            <div role="alert" class="alert alert-danger"><i class="snk snk-info-circled"></i><span>Vui lòng nhập đầy đủ thông tin.</span></div>
                        </div>
                    </div>
                    <input type="hidden" name="FunelID" value="1"/>
                </form>
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-primary btn-lg btn-block" onclick="send_comment(this,'form-comment','send_comment')">Gửi</button>
                <div class="logo">
                    <h2><a title="Sắc Ngọc Khang" href="#">Sắc Ngọc Khang</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>