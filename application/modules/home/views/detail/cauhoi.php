<section id="cauhoi">
    <div class="container">
        <div class="row pd-section" id="trainghiem">
            <div class="col-sm-12">
                <h1 class="text-uppercase text-center">Câu hỏi thường gặp</h1>
                <div class="text-justify">
                    <div class="question-item">
                        <p class="font-weight-bold">1. Tôi đã sử dụng được 2 hộp Sắc Ngọc Khang, tôi thấy da bớt khô, tóc bớt gãy rụng, ngủ ngon hơn, sinh lý thay đổi theo hướng tích cực
                            nhưng mấy vết
                            nám, sạm
                            chưa có thay đổi gì rõ rệt. Vậy tôi có nên tiếp tục sử dụng Sắc Ngọc Khang để giảm nám, sạm hay không?</p> <span>(KH Nguyễn Thị Lan, sinh năm 1964- Phường An Hòa, Sa Đéc, Đồng Tháp)</span>
                        <p>Chào chị Lan, nguyên nhân Chị chưa thấy có sự thay đổi rõ rệt từ các vết nám, sạm vì các sắc tố Melanin gây nám tập trung trên bề mặt da nhưng chân nám thì
                            nằm sâu tận lớp đáy của thượng bì (vui lòng xem hình mặt cắt lớp Thượng bì da mặt), viên uống Sắc Ngọc Khang tác động theo cơ chế đẩy dần chân nám lên bề mặt da và đào thải ra ngoài, nên trong thời gian đầu sử dụng Chị chưa thấy rõ kết quả hoặc một số trường hợp thấy nám xuất hiện nhiều hơn.</p>
                        <p>Vì vậy, cần phải sử dụng đều, đủ và kiên trì trong 3-4 tháng để việc đào thải chân gốc nám được diễn ra liên tục. Mặt khác, thành phần sản phẩm chủ yếu từ thiên nhiên nên hoàn toàn lành tính. Chị yên tâm sử dụng. Hãy cố gắng kiên trì để Khỏe bên trong, Đẹp bên ngoài chị nhé!</p>
                        <p>
                            <img src="public/site/images/mohinhtannhang.webp" alt="mohinhtannhang" class="img img-responsive w-100" style="margin: 0 auto" loading="lazy">
                        </p>
                    </div>
                    <div class="question-item">
                        <p class="font-weight-bold">2. Tôi dùng Sắc Ngọc Khang đã 2 tháng, tầm 6 hộp, thì thấy ăn uống ngon miệng, ngủ sâu giấc và không còn bị đau đầu, chóng mặt,
                            nhưng tôi sợ nếu uống Sắc Ngọc Khang lâu dài sẽ bị béo, tăng cân, vui lòng tư vấn giúp tôi!</p> <span>(KH Hoài Thu, 41 tuổi - xã Hải Đông huyện Hải Hậu tỉnh Nam Định)</span>
                        <p>Chào chị Hoài Thu, sản phẩm Sắc Ngọc Khang với thành phần công thức có nguồn gốc từ thiên nhiên nên chị cứ an tâm sử dụng mà không sợ bị béo hay tăng cân chị nhé! Một trong những nguyên nhân gậy tăng cân hay béo lên là do máu huyết ứ trệ, mất ngủ, thiếu ngủ… thì trong sản phẩm có chứa bộ tứ dược liệu Đương Quy, Ngưu Tất, Thục Địa, Ích Mẫu giúp bổ huyết, hoạt huyết, tăng cường lưu thông máu để nuôi dưỡng các tế bào não bộ, giúp ngủ ngon, ngủ sâu giấc, và không còn bị đau đầu, chóng mặt. Đồng thời, chị Hoài Thu cũng chú ý kiểm soát khẩu phần ăn, nên tăng cường ăn rau quả tươi xanh, kết hợp với tập luyện thể thao để cân bằng vóc dáng nhé!</p>
                    </div>
                    <div class="question-item">
                        <p class="font-weight-bold">3. Tôi là fan hâm mộ Sắc Ngọc Khang từ đầu, tới bây giờ ai cũng khen da tôi hồng hào, sáng mịn tự nhiên. Tôi định sẽ sử dụng Sắc Ngọc Khang cả đời có được không, có tác dụng phụ hay gì không? </p> <span>(Nguyễn Thị Bình - Đông Anh, Hà Nội)</span>
                        <p>Chào chị Bình, Sắc Ngọc Khang rất trân trọng và cảm ơn chị đã luôn đồng hành ủng hộ sản phẩm từ những ngày đầu! Với công thức hoàn toàn từ thảo dược thiên nhiên gồm Đương Quy, Ngưu Tất, Thục Địa, Ích Mẫu, tinh chất mầm đậu nành, Astaxanthin, dầu gấc,…rất an toàn, mang lại hiệu quả khỏe đẹp bền vững mà không gây tác dụng phụ. Chị Bình hãy yên tâm sử dụng Sắc Ngọc Khang hàng ngày như một thói quen để chăm sóc sức khỏe và nuôi dưỡng làn da sáng đẹp từ bên trong chị nhé!
                        </p>
                    </div>
                    <div class="question-item">
                        <p class="font-weight-bold">4. Trong thành phần của Sắc Ngọc Khang có Isofavone từ mầm đậu nành, tôi nghe nói dùng nhiều sẽ bị u nang buồng trứng, u xơ tử cung khiến tôi rất hoang mang, nhờ Sắc Ngọc Khang tư vấn rõ giúp tôi?</p> <span>(Nguyễn Mai Anh - Phường 13, Quận 11, TPHCM)</span>
                        <p>Chào chị Mai Anh, Sắc Ngọc Khang cũng đã nhận rất nhiều câu hỏi của khách hàng hoang mang về vấn đề này. Sắc Ngọc Khang sẽ giải đáp tại đây luôn ạ!
                        </p>
                        <p>Isoflavone thường bị hiểu nhầm là gây u nang buồng trứng, u xơ tử cung là do có những tác dụng tăng cường nội tiết tố Estrogen. Tuy nhiên, điểm đặc biệt
                            là Isoflavone đóng vai trò vừa là chất chủ vận vừa là chất đối kháng với Estrogen, cụ thể Isoflavone giúp bù đắp sự thiếu hụt estrogen, ngược lại khi nội tiết tố nữ trong cơ thể cao, Isoflavone ngăn cản hấp thu estrogen. Mặt khác, Isoflavon tác dụng chủ yếu trên da, xương, cơ tim , tác dụng trên nội mạc tử cung và mô vú rất yếu so với Estrogen tổng hợp và được đào thải ra ngoài qua nước tiểu khi dư thừa nên không gây u xơ tử cung, u nang buồng trứng. Một số nghiên cứu còn cho thấy Isoflavon giảm nguy cơ ung thư vú, ung thư nội mạc tử cung và mang lại nhiều lợi ích sức khỏe cho phụ nữ (theo bài báo “Soy and Health” của Cộng đồng bác sĩ, dược sĩ chịu trách nhiệm y học, “The role of soy isoflavones in menopausal health” của Hiệp hội mãn kinh Bắc Mỹ xuất bản năm 2011).</p>
                        <p>Thành phần Isoflavone trong Sắc Ngọc Khang là một Estrogen thực vật, được chiết xuất từ mầm đậu nành giúp bổ sung, cân bằng nội tiết tố nữ một cách tự nhiên, an toàn đang được áp dụng rộng rãi tại các nước tiên tiến trên thế giới đặc biệt là chị em phụ nữ Nhật Bản luôn bổ sung đậu nành trong những bữa ăn hằng ngày để duy trì tuổi thanh xuân, ngăn ngừa các dấu hiệu lão hóa, và phòng ngừa nhiều bệnh nguy hiểm. Do đó Nhật Bản nổi tiếng vì người dân Nhật sống khỏe mạnh và trường thọ hàng đầu thế giới khiến ai cũng ngưỡng mộ. Các chị em cứ an tâm và tiếp tục sử dụng Sắc Ngọc Khang để Khỏe trong, đẹp ngoài, giữ hoài thanh xuân nhé!</p>
                    </div>
                    <div class="question-item">
                        <p class="font-weight-bold">5. Tôi uống Sắc Ngọc Khang được 3 tháng thì thấy kinh nguyệt rất đều, tôi không còn bị đau bụng mỗi khi hành kinh, nhưng tôi cảm giác nóng trong người, môi hơi khô. Vậy tôi có nên tiếp tục uống Sắc Ngọc Khang hay không?</p> <span>(Nguyễn Song Nguyệt Ánh - xã Thanh Phước, huyện Gò Dầu, tỉnh Tây Ninh)</span>
                        <p>Chào chị Nguyệt Ánh, trong sản phẩm Sắc Ngọc Khang có BỘ TỨ DƯỢC LIỆU gồm Đương Quy, Ngưu Tất, Thục Địa, Ích Mẫu giúp bổ huyết, hoạt huyết, điều hòa kinh nguyệt. Tuy nhiên, với một số cơ địa sẽ có hiện tượng hơi nóng, môi khô. Vì vậy khi dùng Sắc Ngọc Khang chị nên uống nhiều nước, bổ sung thêm rau xanh, nước trái cây để giải nhiệt cho cơ thể.
                        </p>
                    </div>
                    <div class="question-item">
                        <p class="font-weight-bold">6. Tôi năm nay 48 tuổi, tôi bị mất kinh hơn nửa năm, nhưng khi uống Sắc Ngọc Khang được hơn 2 tháng thì tôi lại thấy có kinh trở lại, đêm ngủ thì cũng bớt ra mồ hôi và bớt nóng bừng mặt, cho tôi hỏi có vấn đề gì không, và vì sao?</p> <span>(Phạm Thị Mừng - huyện Gia Viễn tỉnh Ninh Bình)</span>
                        <p>Chào chị Mừng, hiện tượng mất kinh là do Estrogen trong cơ thể quá ít, không đạt được nồng độ ngưỡng trong máu để tác động vào tuyến yên tiết các hormon kích thích trứng rụng. Do Chị đang ở giai đoạn cuối của tiền mãn kinh, chưa chính thức bước vào thời kì mãn kinh, khi dùng Sắc Ngọc Khang cơ thể chị sẽ được bổ sung nội tiết tố nhờ thành phần Isoflavon, đồng thời máu huyết được bồi bổ và tuần hoàn tốt sẽ giúp duy trì hoạt động của buồng trứng, giúp ngăn cản và làm chậm quá trình mãn kinh. Từ đó giúp kéo dài thanh xuân cho các chị em.
                        </p>
                        <p>Viên uống Sắc Ngọc Khang với công thức độc đáo duy nhất chứa các thành phần từ thiên nhiên giải quyết tận gốc 4 nguyên nhân gây ảnh hưởng đến sức khỏe, sắc đẹp và sinh lý của các chị em phụ nữ:</p>
                        <ul>
                            <li>Nhóm giúp cân bằng nội tiết tố nữ, ức chế sản sinh Melanin bới Isoflavone bù đắp sự thiếu hụt estrogen, ngược lại khi nội tiết tố nữ trong cơ thể cao, Isoflavone ngăn cản estrogen hấp thu vào các thụ thể. Nhờ đó Isoflavone giúp cân bằng nội tiết tố nữ, đẩy lùi các triệu chứng tiền mãn kinh, cải thiện các vấn đề nám, sạm, tàn nhang,…</li>
                            <li>Nhóm bổ huyết, hoạt huyết bao gồm bộ tứ dược liệu Đương Quy, Ngưu Tất, Thục Địa, Ích Mẫu được xem như “thánh dược của phụ nữ”, có công dụng bổ
                                huyết, hoạt huyết, điều kinh, tăng cường vận chuyển oxy và dưỡng chất cung cấp cho da, đào thải độc tố ra khỏi cơ thể giúp da hồng hào, khỏe mạnh, đầy sức sống, đẩy lùi các vết nám, sạm, tàn nhang. Bên cạnh đó, bài thuốc còn giúp tăng cường sức khỏe, chống lão hóa.</li>
                            <li>Nhóm chất chống oxy hóa, bảo vệ da khỏi tia UV và gốc tự do, ngăn chặn lão hóa da, bảo vệ da từ bên trong: Astaxanthin được mệnh danh là Vua của các chất chống oxy hóa: Khả năng chống lão hóa da gấp 110 lần so với vitamin E, gấp 6.000 lần so với vitamin C, gấp 800 lần so với CoQ10 toàn năng. Astaxanthin như một “bức tường chắn” vô hiệu hóa các gốc tự do, bảo vệ tế bào từ trong ra ngoài.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>