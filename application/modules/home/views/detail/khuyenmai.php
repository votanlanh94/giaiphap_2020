<div id="tangtui"></div>
<section class="section" id="khuyenmai-remain">
    <div class="container-fluid mt-5">
        <div class="container-fluid">
            <p>
                <img src="public/site/images/banner1.webp" alt="mauhuyet" class="img img-responsive" style="margin: 0 auto" loading="lazy">
            </p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-justify">
                        <p class="font-small">
                            Sắc Ngọc Khang – giải pháp toàn diện cho sắc đẹp và sức khỏe của bạn. Sở hữu trọn bộ giải pháp chăm sóc da và sức khỏe: Nước tẩy trang, Sữa rửa mặt, Nước hoa hồng, Kem dưỡng da, Serum dưỡng trắng, Kem chống nắng, Viên Uống Sắc Ngọc Khang,… sẽ là lựa chọn hoàn hảo cho những người phụ nữ biết yêu thương và chăm sóc bản thân mình.
                        </p>
                        <p class="font-small">
                            Các sản phẩm của Sắc Ngọc Khang vừa kế thừa tinh hoa dược học cổ truyền dân tộc, đồng thời ứng dụng thành quả khoa học thế giới giúp loại bỏ từ gốc rễ những vấn đề về da và cải thiện sinh lý, sức khỏe.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pd-section">
    <div class="container-fluid">
        <div class="row justi-content">
            <div class="col-12 col-md-12 text-center m-b-2">
                <!--                <h4 class="text-uppercase font-weight-bold" style="color:rgb(215, 18, 73)">Chương trình khuyến mãi đặc biệt!!!</h4>-->
                <h2 class="font-weight-bold">
                    <span class="text-uppercase" style="color: rgb(215, 18, 73)"> HẾT NÁM, SÁNG DA, NHẬN QUÀ SÀNH ĐIỆU </span>
                </h2>
            </div>
            <div class="col-md-4 hv-scale" style="line-height: 2">
                <div class="pricing-card p-3 text-center py-5 mt-2 p-t-2 p-b-2">
                    <div class="images p-l-2 p-r-2"><img src="public/site/images/combo2.webp" width="80%" loading="lazy"></div>
                    <p class="p-t-2"><span class="d-block font-weight-bold mt-3">570.000đ <del>600.000đ</del></span></p>
                    <button class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" type="button"
                            onclick="add_to_cart_and_redirect(this,'add_cart', 106 ,1, 'menu')">
                        Mua Ngay
                    </button>
<!--                    <div class="mt-3"><span class="d-block"><i-->
<!--                                    class="fa fa-check"></i>&nbsp;Combo 3 hộp viên uống Sắc Ngọc Khang</span></div>-->
                </div>
            </div>

            <div class="col-md-4 hv-scale" style="line-height: 2">
                <div class="pricing-card p-3 text-center py-5 mt-2 p-t-2 p-b-2">
                    <div class="images p-l-2 p-r-2"><img src="public/site/images/combo1.webp" width="80%" loading="lazy"></div>
                    <p class="p-t-2"><span class="d-block font-weight-bold mt-3">1.080.000đ <del>1.200.000đ</del></span></p>
                    <button class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" type="button"
                            onclick="add_to_cart_and_redirect(this,'add_cart', 330 ,1, 'menu')">
                        Mua Ngay
                    </button>
<!--                    <div class="mt-3"><span class="d-block"><i-->
<!--                                    class="fa fa-check"></i>&nbsp;&nbsp;Combo 6 hộp viên uống Sắc Ngọc Khang</span>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="col-md-4 hv-scale" style="line-height: 2">
                <div class="pricing-card p-3 text-center py-5 p-t-2 p-b-2">
                    <div class="images p-l-2 p-r-2"><img src="public/site/images/combo3.webp" width="80%" loading="lazy"></div>
                    <p class="p-t-2"><span class="d-block font-weight-bold mt-3">699.000đ <del>1.111.000đ</del></span></p>
                    <button class="btn btn-primary shadow m-t-1 m-b-1 px-5 rounded-pill" type="button"
                            onclick="add_to_cart_and_redirect(this,'add_cart', 380 ,1, 'menu')">
                        Mua Ngay
                    </button>
                    <!--                    <div class="mt-3 "><span class="d-block"><i-->
                    <!--                                    class="fa fa-check"></i>&nbsp;1 Sữa Rửa Mặt Sắc Ngọc Khang</span>-->
                    <!--                        <span class="d-block "><i class="fa fa-check"></i>&nbsp;1 Serum Sắc Ngọc Khang</span>-->
                    <!--                        <span class="d-block"><i class="fa fa-check"></i>&nbsp;1 Kem Dưỡng Da Sắc Ngọc Khang</span>-->
                    <!--                        <span class="d-block"><i class="fa fa-check"></i>&nbsp;1 Nước Hoa Hồng Sắc Ngọc Khang</span>-->
                    <!--                        <span class="d-block"><i class="fa fa-check"></i>&nbsp;1 Nước Tẩy Trang Sắc Ngọc Khang</span>-->
                    <!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>
