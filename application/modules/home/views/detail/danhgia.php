<!--<div id="trainghiem" class="content-style-2 m-b-2 container-fluid p-a-0">-->
<!--    <h1 class="text-uppercase font-weight-bold text-center">Cảm nhận người dùng</h1>-->
<!--    <div class="slick">-->
<!--        <div class="slick-item hvr-float">-->
<!--            <div class="thumbnail thin-border pos-relative">-->
<!--                <img src="/public/site/images/nguoidung1.jpg"-->
<!--                     alt="người tiêu dùng đánh giá serum sắc ngọc khang"-->
<!--                     class="img img-responsive img-circle img-customer">-->
<!--                <div class="caption p-t-4">-->
<!--                    <h5 class="font-weight-bold">Chị Trương Huỳnh Đào </h5>-->
<!--                    <p class="position">Nhân viên công ty Brenn Tag (TP Hồ Chí Minh)</p>-->
<!--                    <p class="min-text">-->
<!--                        Mình biết đến sản phẩm qua lời giới thiệu của bạn bè. Sau 3 tháng sử dụng, mình thấy hài lòng và ưng ý. Trong quá trình tìm hiểu, mình biết thêm là sản phẩm-->
<!--                        chứa nhiều thành phần khác nhau. Đây đều là những loại thảo dược quý và những hoạt chất từ thiên nhiên tốt cho việc làm đẹp và cho sức khỏe của chị em phụ-->
<!--                        nữ.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="slick-item hvr-float">-->
<!--            <div class="thumbnail thin-border pos-relative">-->
<!--                <img src="/public/site/images/nguoidung2.jpg"-->
<!--                     class="img img-responsive img-circle img-customer"-->
<!--                     alt="đánh giá về serum sắc ngọc khang">-->
<!--                <div class="caption p-t-4">-->
<!--                    <h5 class="font-weight-bold">Chị Nguyễn Mai Anh</h5>-->
<!--                    <p class="position">Nội trợ (Lâm Đồng)</p>-->
<!--                    <p class="min-text">-->
<!--                        Đến độ tuổi U40, mình gặp phải nhiều vấn đề về da (da mình bị nhăn, khô và sạm nám) và đặc biệt là vấn đề về đời sống vợ chồng. Mình đi khám và biết được-->
<!--                        nguyên nhân do suy giảm nội tiết tố nữ. Vì thế, mình đã bổ sung Viên Uống Sắc Ngọc Khang Vi Tảo Lục trong vòng 6 tháng và cảm nhận được hiệu quả rõ rệt. Da-->
<!--                        mình đẹp lên, không còn nhiều nếp nhăn, giấc ngủ cũng được cải thiện đáng kể, “chuyện chăn gối” cũng dần trở nên ấm êm.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="slick-item hvr-float">-->
<!--            <div class="thumbnail thin-border pos-relative">-->
<!--                <img src="/public/site/images/nguoidung3.jpg"-->
<!--                     class="img img-responsive img-circle img-customer"-->
<!--                     alt="MC Tường Vân đánh giá về serum Sắc Ngọc Khang">-->
<!--                <div class="caption p-t-4">-->
<!--                    <h5 class="font-weight-bold">Chị Lê Thị Kim Ngân</h5>-->
<!--                    <p class="position">Kinh doanh online nông sản xanh tại nhà</p>-->
<!--                    <p class="min-text">Với bản thân tôi thì Viên Uống Sắc Ngọc Khang là một lựa chọn thích hợp trong việc chống lão hóa da và cải thiện lưu thông máu. Chính nhờ-->
<!--                        vậy mà da dẻ tôi hồng hào, sáng mịn hơn sau 1 năm sử dụng sản phẩm. Vì quá trình tuần hoàn máu diễn ra ổn định, giấc ngủ của tôi được cải thiện đáng kể, tôi-->
<!--                        không còn gặp phải những tình trạng như khó ngủ, ngủ chập chờn không sâu giấc trước đó.</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <!--comment facebook-->
<!--    <div id="carousel-simple" class="carousel multi-item-carousel slide" data-ride="carousel">-->
<!--        <div class="carousel-inner" role="listbox">-->
<!--            <div class="item active">-->
<!--                <div class="row">-->
<!--                    <div class="col-md-6">-->
<!--                        <img src="/public/site/images/comment-1.jpg" alt="Comment fb 1"-->
<!--                             class="img img-responsive center-text">-->
<!--                    </div>-->
<!--                    <div class="col-md-6">-->
<!--                        <img src="/public/site/images/comment-2.jpg" alt="Comment fb 2"-->
<!--                             class="img img-responsive center-text">-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="item">-->
<!--                <div class="item active">-->
<!--                    <div class="row">-->
<!--                        <div class="col-md-6">-->
<!--                            <img src="/public/site/images/comment-1.jpg" alt="Comment fb 1"-->
<!--                                 class="img img-responsive center-text">-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <img src="/public/site/images/comment-2.jpg" alt="Comment fb 2"-->
<!--                                 class="img img-responsive center-text">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <a class="left carousel-control" href="#carousel-simple" role="button" data-slide="prev">-->
<!--            <i class="fa fa-chevron-left" aria-hidden="true"></i>-->
<!--        </a>-->
<!--        <a class="right carousel-control" href="#carousel-simple" role="button" data-slide="next">-->
<!--            <i class="fa fa-chevron-right" aria-hidden="true"></i>-->
<!--        </a>-->
<!--    </div>-->
<!--</div>-->

<!--<script type="text/javascript">-->
<!--    $('.multi-item-carousel .item').each(function () {-->
<!--        var next = $(this).next();-->
<!--        if (!next.length) {-->
<!--            next = $(this).siblings(':first');-->
<!--        }-->
<!--        next.children(':first-child').clone().appendTo($(this));-->
<!---->
<!--        if (next.next().length > 0) {-->
<!--            next.next().children(':first-child').clone().appendTo($(this));-->
<!--        } else {-->
<!--            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));-->
<!--        }-->
<!--    });-->
<!--</script>-->