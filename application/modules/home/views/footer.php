    <footer>
      <div class="footer-content content">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
              <div class="fb-page" data-href="https://www.facebook.com/sacngockhang/" data-tabs="timeline" data-height="220" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/sacngockhang/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/sacngockhang/">Sắc Ngọc Khang</a></blockquote></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
                <div class="row logo">
                  <div class="col-xs-12 col-sm-6 col-md-2 logo-htp-footer">
                    <img src="public/site/clickfunel/images/logo-htp.png" class="img-responsive" />
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-4">
                    <img src="public/site/images/logo-sacngockhang.png" alt="logo serum footer" class="img-responsive" />
                  </div>
                  <div class="col-xs-4 col-sm-6 col-md-3 logo-bct-footer">
                    <!-- <img src="public/site/clickfunel/images/sacngockhang-thong-bao-bo-cong-thuong.png" alt="sắc ngọc khang thông báo bộ công thương" class="img-responsive" /> -->
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 info">
                    <hr>
                    <p>Địa chỉ trụ sở : Tầng 6, Tòa nhà Mê Linh Point Tower, số 2 Ngô Đức kế, Phường Bến Nghé, Quận 1, HCM.</p>
                    <p>Tổng đài tư vấn / đặt hàng : <b class="text-htp">19006033</b></p>
                    <p>MST/ĐKKD/QĐTL : 0315491880</p>
                  </div>
                </div>
            </div>
          </div>
        </div>        
      </div>
    </footer>
    <!-- End footer-->    
    <div class="fb-customerchat"
      page_id="141058836006826"
      theme_color="#fa3c4c"
      logged_in_greeting="Xin chào ! Anh/Chị cần hỗ trợ gì ạ ?"
      logged_out_greeting="Xin chào ! Anh/Chị cần hỗ trợ gì ạ ?"
    >
    </div>

    